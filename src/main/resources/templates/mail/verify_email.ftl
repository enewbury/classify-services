<#-- @ftlvariable name="" type="com.classify.model.data.model.TokenEmailModel" -->
<#include "component/email_header.ftl">
<div style="text-align:center; font-size: 18px;">
    <p style="font-size: 1.5em">Hi ${name},</p>

    <p>We're glad you decided to join us!  Before we get started, click the link to verify that you're real and not a sneaky robot!</p>
    <div style="width: 200px; max-width: 80%; margin:0 auto; padding: 50px 0;"><a style="<#include "component/button_styles.ftl">" href="${domain}/verify?token=${token}">Verify Email</a></div>

    <hr style="margin: 40px; border: 0; border-top: 1px solid #ecf0f1;" />
    <p>Or paste this link into your browser: ${domain}/verify?token=${token}</p>
</div>
<#include "component/email_footer.ftl">