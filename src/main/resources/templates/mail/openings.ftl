<#-- @ftlvariable name="" type="com.classify.model.data.model.SectionUpdatesDataModel" -->
<#include "component/email_header.ftl">
<div style="font-size: 18px;">
    <p style="font-size: 1.5em">Hi ${name},</p>

    <#if (openings?size > 0)>
        <p>Good news!  <#if (openings?size > 1)>Courses<#else>A course</#if> in your watchlist opened up!</p>

        <div>
            <#list openings as section>
                <div style="padding: 15px; background-color: #e3e3e3; margin-top: 15px; text-align: left">
                    <span style="float:right; text-align: center;">
                        ${section.seats - section.seatsFilled}<br />
                        seat<#if ((section.seats - section.seatsFilled) > 1)>s</#if> left
                    </span>
                    <strong>${section.course.subject} ${section.course.courseNumber} - ${section.sectionName}</strong><br />
                    <span>${section.course.name}</span>
                </div>
            </#list>

            <#--<div style="width: 200px; max-width: 80%; margin:0 auto; padding: 30px 0;"><a style="<#include "component/button_styles.ftl">" href="${domain}/register">Register</a></div>-->
        </div>
    </#if>

    <#if (openings?size > 0 && closings?size > 0)>
        <hr style="margin: 40px; border: 0; border-top: 1px solid #ecf0f1;" />
    </#if>

<#if (closings?size > 0)>
    <p>Unfortunately  <#if (closings?size > 1)>courses<#else>a course</#if> in your watchlist just filled up.</p>

    <div>
        <#list closings as section>
            <div style="padding: 15px; background-color: #e3e3e3; margin-top: 15px; text-align: left">
                <strong>${section.course.subject} ${section.course.courseNumber} - ${section.sectionName}</strong><br />
                <span>${section.course.name}</span>
            </div>
        </#list>
        <div style="width: 200px; max-width: 80%; margin:0 auto; padding: 30px 0;"><a style="<#include "component/button_styles.ftl">" href="${domain}/search">Search For Alternatives</a></div>
    </div>
</#if>

</div>
<#include "component/email_footer.ftl">