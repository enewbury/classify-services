<#-- @ftlvariable name="" type="com.classify.model.data.model.NewEmailDataModel" -->
<#include "component/email_header.ftl">
<div style="text-align:center; font-size: 18px;">

    <p>We just need to check this is a real email address before updated your account email.</p>
    <div style="width: 200px; max-width: 80%; margin:0 auto; padding: 50px 0;"><a style="<#include "component/button_styles.ftl">" href="${domain}/update-email?token=${token}">Verify Email</a></div>

    <hr style="margin: 40px; border: 0; border-top: 1px solid #ecf0f1;" />
    <p>Or paste this link into your browser: ${domain}/update-email?token=${token}</p>
</div>
<#include "component/email_footer.ftl">