<#-- @ftlvariable name="stackTrace" type="java.lang.String" -->
<#-- @ftlvariable name="location" type="java.lang.String" -->
<#-- @ftlvariable name="date" type="String" -->
<#include "component/email_header.ftl">

<p>Hey Eric!</p>
<p>Things are getting crazy down here on the ground.  I think we may have a broken integration.</p>

<p><b>DateTime:</b> ${date}</p>
<p><b>Error Location:</b> ${location}</p>
<br>
<b>Stack Trace</b>
<div style="color:#550000; background-color: #ffd2d2; border: solid 1px #b05e66; padding: 10px;">
    ${stackTrace}
</div>

<#include "component/email_footer.ftl">
