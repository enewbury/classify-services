<#-- @ftlvariable name="" type="com.classify.model.data.model.DonationDataModel" -->
<#include "component/email_header.ftl">
<div style="font-size: 18px; text-align: center;">
    <p>Thank you for your donation to Classify of:</p>
    <h1 style="font-size: 4em;" >$${amount}</h1>

</div>
<#include "component/email_footer.ftl">