<#-- @ftlvariable name="" type="com.classify.model.data.model.FeedbackModel" -->
<#include "component/email_header.ftl">
<div style="font-size: 16px;">
    <p>User ${name} (${email}) submitted feedback on classify.</p>
    <p>${feedback}</p>

</div>
<#include "component/email_footer.ftl">