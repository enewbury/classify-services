<#-- @ftlvariable name="" type="com.classify.model.data.model.TokenEmailModel" -->
<#include "component/email_header.ftl">
<div style="text-align:center; font-size: 18px;">
    <p style="font-size: 1.5em">Hi ${name},</p>

    <p>So you need a new password?  Let's get you a new one.</p>
    <div style="width: 200px; max-width: 80%; margin:0 auto; padding: 50px 0;"><a style="<#include "component/button_styles.ftl">" href="${domain}/reset?token=${token}">Reset Password</a></div>

    <p>Or paste this link into your browser: ${domain}/reset?token=${token}</p>

    <hr style="margin: 40px; border: 0; border-top: 1px solid #ecf0f1;" />
    <p>Didn't mean to reset your password?  No worries, you can safely ignore this email.</p>
</div>
<#include "component/email_footer.ftl">