<#-- @ftlvariable name="" type="com.classify.model.data.model.EmailModel" -->
<#--end content-->
    <p style="padding: 20px 0;">Best, <br /><b>Eric Newbury</b><br /><span style="color: #00b06c">The Classify Guy</span></p>
</div>

<#--footer-->
<div id="footer" style="font-size: 14px; margin:20px 0; text-align:center; color:#777; font-family:'Source Sans Pro','arial'; ">
    <a href="https://www.facebook.com/classifyRegistration/" style="color:#777;">like us on facebook</a> | <a href="${domain}" style="color:#777;">${domain?keep_after("https://")}</a>
</div>

<#--media queries for mobile clients-->
<style type="text/css">
    @media (max-width: 600px) {
        #header div{text-align:center;}
        #header span{display:none}
    }
</style>

</body>
</html>