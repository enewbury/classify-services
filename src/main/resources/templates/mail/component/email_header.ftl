<#-- @ftlvariable name="" type="com.classify.model.data.model.EmailModel" -->
<html>
<body style="padding:30px; max-width: 500px; margin:0 auto; background: #ddd; font-size: 16px; color: #444;">

<#--header-->
<div id="header" style="padding:15px; padding-bottom: 12px; background-color:#333; font-family:'Source Sans Pro','arial'; ">
    <div style="position:relative;">
        <a href="${domain}">
            <img style="height:30px; vertical-align:middle;" src="${domain}/logo.png" alt="classify">
        </a>
        <span style="padding: 0 5px; color:white; font-size:20px; font-weight:200;">Classify</span>
    </div>
</div>

<#--content-->
<div id="content" style="padding:15px; background-color:white; font-family:'Source Sans Pro','arial';">