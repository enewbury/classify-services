package com.classify.model.rest;

import com.classify.exceptions.ErrorType;
import com.classify.exceptions.ExceptionLevel;

/**
 * Created by Eric Newbury on 6/13/17.
 */
public class ErrorRestResponse {
    private ExceptionLevel level;
    private ErrorType error;
    private String message;
    private String devMessage;

    public ErrorRestResponse() {}

    public ErrorRestResponse(ExceptionLevel level, ErrorType error, String message, String devMessage) {
        this.level = level;
        this.error = error;
        this.message = message;
        this.devMessage = devMessage;
    }

    public ExceptionLevel getLevel() {
        return level;
    }

    public ErrorType getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getDevMessage() {
        return devMessage;
    }
}
