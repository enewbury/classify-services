package com.classify.model.rest;

/**
 * Created by Eric Newbury on 8/16/17.
 */
public class AuthenticationErrorRestResponse {
    private String message;
    private String exception;


    public AuthenticationErrorRestResponse(String message, String exception) {
        this.message = message;
        this.exception = exception;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
}
