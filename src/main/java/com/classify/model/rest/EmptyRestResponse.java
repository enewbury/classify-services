package com.classify.model.rest;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by Eric Newbury on 8/16/17.
 */
@JsonSerialize
public class EmptyRestResponse {}
