package com.classify.model.rest;

import com.classify.exceptions.ErrorType;
import com.classify.exceptions.ExceptionLevel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Eric Newbury on 8/10/17.
 */
public class ValidationErrorRestResponse extends ErrorRestResponse{

    private List<Violation> violations;

    public ValidationErrorRestResponse(String message, List<Violation> violations) {
        super(ExceptionLevel.WARN, ErrorType.VALIDATION_ERROR, message, message);
        this.violations = violations;
    }

    public List<Violation> getViolations() {
        return violations;
    }

    public void setViolations(List<Violation> violations) {
        this.violations = violations;
    }

    public static final class Violation {
        private final String field;
        private final String violation;

        public Violation(String field, String violation) {
            this.field = field;
            this.violation = violation;
        }

        public String getField() {
            return field;
        }

        public String getViolation() {
            return violation;
        }
    }
}
