package com.classify.model.rest.request;

/**
 * Created by Eric Newbury on 7/31/17.
 */
public class UpdateScheduleNameRequest {
    private String scheduleName;

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }
}
