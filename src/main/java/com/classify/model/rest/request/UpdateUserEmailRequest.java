package com.classify.model.rest.request;

import org.hibernate.validator.constraints.Email;

/**
 * Created by Eric Newbury on 9/5/17.
 */
public class UpdateUserEmailRequest {

    @Email(message = "Must be a valid email")
    private String email;

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
