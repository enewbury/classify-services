package com.classify.model.rest.request;

import com.classify.model.business.objects.enums.TokenType;

/**
 * Created by Eric Newbury on 8/17/17.
 */
public class PasswordTokenRequest extends TokenRequest {

    public PasswordTokenRequest() {
        this.setType(TokenType.PASSWORD);
    }

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
