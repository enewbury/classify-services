package com.classify.model.rest.request;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

/**
 * Created by Eric Newbury on 6/13/17.
 */
public class AccountCreationRequest {

    @Email(message = "Must be a valid email")
    private String email;

    @Length(min = 6, message = "Password must be at least 6 characters")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
