package com.classify.model.rest.request;

/**
 * Created by Eric Newbury on 7/25/17.
 */
public class AddSectionToScheduleRequest {
    private Integer termId;
    private Integer sectionId;
    private Integer parentId;

    public Integer getTermId() {
        return termId;
    }

    public void setTermId(Integer termId) {
        this.termId = termId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
}
