package com.classify.model.rest.request;

import com.classify.model.business.objects.enums.TokenType;

/**
 * Created by Eric Newbury on 8/17/17.
 */
public class VerificationTokenRequest extends TokenRequest {
    public VerificationTokenRequest() {
        this.setType(TokenType.VERIFICATION);
    }
}
