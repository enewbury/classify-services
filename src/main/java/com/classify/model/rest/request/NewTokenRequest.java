package com.classify.model.rest.request;

import com.classify.model.business.objects.enums.TokenType;

import javax.validation.constraints.NotNull;

/**
 * Created by Eric Newbury on 8/16/17.
 */
public class NewTokenRequest {
    @NotNull
    private String email;
    @NotNull
    private TokenType type;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TokenType getType() {
        return type;
    }

    public void setType(TokenType type) {
        this.type = type;
    }
}
