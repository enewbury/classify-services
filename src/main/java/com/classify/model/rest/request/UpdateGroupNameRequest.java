package com.classify.model.rest.request;

/**
 * Created by Eric Newbury on 7/31/17.
 */
public class UpdateGroupNameRequest {
    private String groupName;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
