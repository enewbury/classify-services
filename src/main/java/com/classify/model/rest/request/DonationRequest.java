package com.classify.model.rest.request;

/**
 * Created by Eric Newbury on 9/14/17.
 */
public class DonationRequest {
    private String email;
    private Integer amount;
    private String nonce;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }
}
