package com.classify.model.rest.request;

import com.classify.model.business.objects.enums.TokenType;

/**
 * Created by Eric Newbury on 9/15/17.
 */
public class NewEmailTokenRequest extends TokenRequest {
    public NewEmailTokenRequest() {
        this.setType(TokenType.UPDATE_EMAIL);
    }
}
