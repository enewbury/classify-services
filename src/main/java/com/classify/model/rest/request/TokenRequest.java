package com.classify.model.rest.request;

import com.classify.model.business.objects.enums.TokenType;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by Eric Newbury on 8/16/17.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = VerificationTokenRequest.class, name = "VERIFICATION"),
        @JsonSubTypes.Type(value = PasswordTokenRequest.class, name = "PASSWORD"),
        @JsonSubTypes.Type(value = NewEmailTokenRequest.class, name= "UPDATE_EMAIL")
})
public class TokenRequest {
    private String token;
    private TokenType type;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public TokenType getType() {
        return type;
    }

    public void setType(TokenType type) {
        this.type = type;
    }
}
