package com.classify.model.rest.request;

/**
 * Created by Eric Newbury on 8/21/17.
 */
public class SectionIdRequest {
    private Integer sectionId;

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }
}
