package com.classify.model.rest.request;

/**
 * Created by Eric Newbury on 7/25/17.
 */
public class NewScheduleRequest {
    private Integer termId;
    private String scheduleName;

    public Integer getTermId() {
        return termId;
    }

    public void setTermId(Integer termId) {
        this.termId = termId;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }
}
