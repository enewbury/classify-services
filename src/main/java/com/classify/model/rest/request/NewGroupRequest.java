package com.classify.model.rest.request;

/**
 * Created by Eric Newbury on 7/26/17.
 */
public class NewGroupRequest {
    private String folderName;
    private Integer termId;
    private Integer parentId;

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public Integer getTermId() {
        return termId;
    }

    public void setTermId(Integer termId) {
        this.termId = termId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
}
