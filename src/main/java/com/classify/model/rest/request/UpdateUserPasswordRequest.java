package com.classify.model.rest.request;

import org.hibernate.validator.constraints.Length;

/**
 * Created by Eric Newbury on 9/5/17.
 */
public class UpdateUserPasswordRequest {
    @Length(min = 6, message = "Password must be at least 6 characters")
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
