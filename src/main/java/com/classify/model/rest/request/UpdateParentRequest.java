package com.classify.model.rest.request;

/**
 * Created by Eric Newbury on 9/8/17.
 */
public class UpdateParentRequest {
    private Integer newParentId;

    public Integer getNewParentId() {
        return newParentId;
    }

    public void setNewParentId(Integer newParentId) {
        this.newParentId = newParentId;
    }
}
