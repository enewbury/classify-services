package com.classify.model.queue.message;

import com.classify.model.business.objects.enums.TokenType;

/**
 * Created by Eric Newbury on 8/11/17.
 */
public class GenerateTokenMessage {
    private TokenType type;
    private Integer userId;
    private String metaInformation;

    public GenerateTokenMessage() {
    }

    public GenerateTokenMessage(TokenType type, Integer userId, String metaInformation) {
        this.type = type;
        this.userId = userId;
        this.metaInformation = metaInformation;
    }

    public void setType(TokenType type) {
        this.type = type;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public TokenType getType() {
        return type;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getMetaInformation() {
        return metaInformation;
    }

    public void setMetaInformation(String metaInformation) {
        this.metaInformation = metaInformation;
    }
}
