package com.classify.model.queue.message;

/**
 * Created by Eric Newbury on 8/12/17.
 */
public class Email {
    private String type;
    private Integer userId;

    public Email() {
    }

    public Email(String type, Integer userId) {
        this.type = type;
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return String.format("Email{type=%s, userId=%s}", getType(), getUserId());
    }

}
