package com.classify.model;

import com.classify.model.business.objects.*;
import com.google.common.collect.Lists;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnrollmentChanges {

    private Term term;
    private BatchCrud<Course> courses = new BatchCrud<>();
    private BatchCrud<Section> sections = new BatchCrud<>();
    private BatchCrud<TimeSlot> timeSlots = new BatchCrud<>();
    private BatchCrud<Instructor> instructors = new BatchCrud<>();

    private Map<Integer, Section> previousSections = new HashMap<>();

    public EnrollmentChanges(Term term) {
        this.term = term;
    }

    public BatchCrud<Course> getCourses() { return courses; }

    public BatchCrud<Section> getSections() { return sections; }

    public BatchCrud<TimeSlot> getTimeSlots() { return timeSlots; }

    public BatchCrud<Instructor> getInstructors() { return instructors; }

    public Map<Integer, Section> getPreviousSections() {
        return previousSections;
    }


    public Term getTerm() {
        return term;
    }



    public static class BatchCrud<T>{
        private List<T> inserts = Lists.newArrayList();
        private List<T> updates = Lists.newArrayList();
        private List<T> deletes = Lists.newArrayList();

        public List<T> getInserts() { return inserts; }

        public List<T> getUpdates() { return updates; }

        public List<T> getDeletes() { return deletes; }
    }

}
