package com.classify.model;

public enum WeekDay {
    MON(1), TUE(2), WED(3), THU(4), FRI(5), SAT(6), SUN(7);

    private int value;

    WeekDay(int value) {
        this.value = value;
    }

    public int getId() {
        return value;
    }

    public static WeekDay getEnum(int value) throws IllegalArgumentException{
        switch(value){
            case 1:
                return MON;
            case 2:
                return TUE;
            case 3:
                return WED;
            case 4:
                return THU;
            case 5:
                return FRI;
            case 6:
                return SAT;
            case 7:
                return SUN;
            default:
                throw new IllegalArgumentException("Input '"+value+"' matches no weekday enum constant");
        }
    }

    public static WeekDay getEnum(String value) throws IllegalArgumentException{
        switch (value) {
            case "M":
            case "MON":
            case "MONDAY":
                return valueOf("MON");
            case "T":
            case "TUE":
            case "TUESDAY":
                return valueOf("TUE");
            case "W":
            case "WED":
            case "WEDNESDAY":
                return valueOf("WED");
            case "R":
            case "THU":
            case "THURSDAY":
                return valueOf("THU");
            case "F":
            case "FRI":
            case "FRIDAY":
                return valueOf("FRI");
            case "S":
            case "SAT":
            case "SATURDAY":
                return valueOf("SAT");
            case "U":
            case "SUN":
            case "SUNDAY":
                return valueOf("SUN");
            default:
                throw new IllegalArgumentException("Input '"+value+"' matches no weekday enum constant");
        }
    }

    public static String getFullName(int value) throws IllegalArgumentException{
        switch(value){
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
            default:
                throw new IllegalArgumentException("Input '"+value+"' matches no weekday id");
        }
    }
}