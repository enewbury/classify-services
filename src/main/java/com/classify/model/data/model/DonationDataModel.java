package com.classify.model.data.model;

/**
 * Created by Eric Newbury on 9/14/17.
 */
public class DonationDataModel extends EmailModel {
    private String email;
    private Integer amount;

    public DonationDataModel(String email, Integer amount) {
        this.email = email;
        this.amount = amount;
    }

    public String getEmail() {
        return email;
    }

    public Integer getAmount() {
        return amount;
    }
}
