package com.classify.model.data.model;

/**
 * Created by Eric Newbury on 9/15/17.
 */
public class NewEmailDataModel extends EmailModel{
    private String token;

    public NewEmailDataModel(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
