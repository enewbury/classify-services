package com.classify.model.data.model;

import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.User;

import java.util.List;

/**
 * Created by Eric Newbury on 8/21/17.
 */
public class SectionUpdatesDataModel extends EmailModel{
    private final String name;
    private final List<Section> openings;
    private final List<Section> closings;

    public SectionUpdatesDataModel(String name, List<Section> openings, List<Section> closings) {
        this.name = name;
        this.openings = openings;
        this.closings = closings;
    }

    public String getName() {
        return name;
    }

    public List<Section> getOpenings() {
        return openings;
    }

    public List<Section> getClosings() {
        return closings;
    }
}
