package com.classify.model.data.model;

/**
 * Created by Eric Newbury on 8/12/17.
 */
public class TokenEmailModel extends EmailModel {
    private final String name;
    private final String token;

    public TokenEmailModel(String name, String token) {
        this.name = name;
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public String getToken() {
        return token;
    }
}
