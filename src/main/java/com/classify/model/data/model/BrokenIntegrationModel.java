package com.classify.model.data.model;

/**
 * Created by Eric Newbury on 8/12/17.
 */
public class BrokenIntegrationModel extends EmailModel {
    private final String location;
    private final String stackTrace;
    private final String date;

    public BrokenIntegrationModel(String location, String stackTrace, String date) {
        this.location = location;
        this.stackTrace = stackTrace;
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public String getDate() {
        return date;
    }
}
