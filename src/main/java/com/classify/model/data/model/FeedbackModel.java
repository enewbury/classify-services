package com.classify.model.data.model;

/**
 * Created by Eric Newbury on 9/20/17.
 */
public class FeedbackModel extends EmailModel {
    private String name;
    private String email;
    private String feedback;

    public FeedbackModel(String name, String email, String feedback) {
        this.name = name;
        this.email = email;
        this.feedback = feedback;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getFeedback() {
        return feedback;
    }
}
