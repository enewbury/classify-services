package com.classify.model.data.model;

/**
 * Created by Eric Newbury on 8/12/17.
 */
public class EmailModel {
    private String domain;
    private String resourceDomain;

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public void setResourceDomain(String resourceDomain) {
        this.resourceDomain = resourceDomain;
    }

    public String getResourceDomain() {
        return resourceDomain;
    }

    public String getDomain() {
        return domain;
    }
}
