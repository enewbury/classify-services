package com.classify.model.uvm;

import com.classify.model.business.objects.enums.SectionType;

import java.sql.Time;
import java.time.LocalTime;


public class DenormalizedCourseData {
    public Integer courseId;
    public String courseName;
    public String subject;
    public String courseNumber;
    public Double creditsMin;
    public Double creditsMax;
    public Integer universityId;

    public Integer sectionId;
    public Integer universitySectionId;
    public String sectionName;
    public SectionType sectionType;
    public Integer seats;
    public Integer seatsFilled;

    public Integer instructorId;
    public String firstName;
    public String lastName;
    public String universityUsername;
    public String email;

    public Integer timeId;
    public LocalTime startTime;
    public LocalTime endTime;
    public String building;
    public String room;
    public String days;
    public int nonWeekly;

    @Override
    public String toString() {
        return "DenormalizedCourseData{\n" +
                "courseId=" + courseId +"\n"+
                ", courseName='" + courseName + '\'' + "\n"+
                ", subject='" + subject + '\'' +"\n"+
                ", courseNumber='" + courseNumber + '\'' +"\n"+
                ", creditsMin=" + creditsMin +"\n"+
                ", creditsMax=" + creditsMax +"\n"+
                ", sectionId=" + sectionId +"\n"+
                ", universitySectionId=" + universitySectionId + "\n"+
                ", sectionName='" + sectionName + '\'' +"\n"+
                ", sectionType=" + sectionType +"\n"+
                ", seats=" + seats +"\n"+
                ", seatsFilled=" + seatsFilled +"\n"+
                ", instructorId=" + instructorId +"\n"+
                ", firstName='" + firstName + '\'' +"\n"+
                ", lastName='" + lastName + '\'' +"\n"+
                ", universityUsername='" + universityUsername + '\'' +"\n"+
                ", email='" + email + '\'' +"\n"+
                ", timeId=" + timeId + "\'" + "\n" +
                ", startTime=" + startTime +"\n"+
                ", endTime=" + endTime +"\n"+
                ", building='" + building + '\'' +"\n"+
                ", room='" + room + '\'' +"\n"+
                ", days='" + days + '\'' +"\n"+
                ", nonWeekly='" + nonWeekly + '\'' +"\n"+
                '}';
    }
}
