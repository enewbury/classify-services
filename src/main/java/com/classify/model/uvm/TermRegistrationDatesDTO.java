package com.classify.model.uvm;

import com.classify.model.Semester;
import com.classify.model.business.objects.RegistrationTime;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TermRegistrationDatesDTO {
    private Semester semester;
    private Integer year;
    private List<RegistrationTime> registrationTimes;


    public TermRegistrationDatesDTO() {
        registrationTimes = new ArrayList<>();
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @NotNull
    public List<RegistrationTime> getRegistrationTimes() {
        return registrationTimes;
    }

    public void setRegistrationTimes(List<RegistrationTime> registrationTimes) {
        this.registrationTimes = registrationTimes;
    }
}
