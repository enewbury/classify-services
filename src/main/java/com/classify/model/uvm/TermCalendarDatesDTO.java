package com.classify.model.uvm;

import java.time.LocalDate;

public class TermCalendarDatesDTO {
    private LocalDate firstDay;
    private LocalDate lastDay;
    private LocalDate addDrop;
    private LocalDate withdraw;

    public LocalDate getFirstDay() {
        return firstDay;
    }

    public void setFirstDay(LocalDate firstDay) {
        this.firstDay = firstDay;
    }

    public LocalDate getLastDay() {
        return lastDay;
    }

    public void setLastDay(LocalDate lastDay) {
        this.lastDay = lastDay;
    }

    public LocalDate getAddDrop() {
        return addDrop;
    }

    public void setAddDrop(LocalDate addDrop) {
        this.addDrop = addDrop;
    }

    public LocalDate getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(LocalDate withdraw) {
        this.withdraw = withdraw;
    }
}
