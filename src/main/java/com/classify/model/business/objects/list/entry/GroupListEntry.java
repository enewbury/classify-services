package com.classify.model.business.objects.list.entry;

import com.classify.model.business.objects.enums.ListEntryType;

/**
 * Created by Eric Newbury on 6/15/17.
 */
public class GroupListEntry extends ContainerListEntry {

    public GroupListEntry() {
        this.setType(ListEntryType.GROUP);
    }

    private String name;
    private Integer requiredCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRequiredCount() {
        return requiredCount;
    }

    public void setRequiredCount(Integer requiredCount) {
        this.requiredCount = requiredCount;
    }
}
