package com.classify.model.business.objects.enums;

public enum Standing {
    GRADUATE,SENIOR,JUNIOR,SOPHOMORE,FRESHMAN,PART_TIME, ALL
}
