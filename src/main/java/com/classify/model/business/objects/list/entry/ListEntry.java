package com.classify.model.business.objects.list.entry;

import com.classify.model.business.objects.enums.ListEntryType;

/**
 * Created by Eric Newbury on 6/15/17.
 */
public class ListEntry {
    private Integer id;
    private Integer userId;
    private Integer termId;

    private Integer parentId;
    private ListEntryType type;
    private Integer priority;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTermId() {
        return termId;
    }

    public void setTermId(Integer termId) {
        this.termId = termId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public ListEntryType getType() {
        return type;
    }

    public void setType(ListEntryType type) {
        this.type = type;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
