package com.classify.model.business.objects.enums;

public enum SectionType { LECTURE, LAB, ONLINE, OTHER }
