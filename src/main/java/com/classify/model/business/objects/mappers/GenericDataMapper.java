package com.classify.model.business.objects.mappers;

import com.classify.model.WeekDay;
import com.classify.model.business.objects.*;
import com.classify.model.business.objects.enums.SectionType;
import com.classify.model.uvm.DenormalizedCourseData;
import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.stream.Collectors;

public class GenericDataMapper implements DataMapper<DenormalizedCourseData> {

    @Override
    public Course mapCourse(DenormalizedCourseData record) {
        if(record == null){
            throw new NullPointerException("Tried to map a null record to a course");
        }

        Course course = new Course();
        course.setId(record.courseId);
        course.setName(record.courseName);
        course.setSubject(record.subject);
        course.setCourseNumber(record.courseNumber);
        course.setCreditsMin(record.creditsMin);
        course.setCreditsMax(record.creditsMax);

        return course;
    }

    @Override
    public Section mapSection(DenormalizedCourseData record) {
        if(record == null){
            throw new NullPointerException("Tried to map a null record to a section");
        }

        Section section = new Section();
        section.setId(record.sectionId);
        section.setUniversitySectionId(record.universitySectionId);
        section.setCourseId(record.courseId);
        section.setSectionName(record.sectionName);
        section.setType(SectionType.valueOf(record.sectionType.toString()));
        section.setSeats(record.seats);
        section.setSeatsFilled(record.seatsFilled);
        section.setInstructorId(record.instructorId);

        Instructor instructor = new Instructor();
        instructor.setId(record.instructorId);
        instructor.setFirstName(record.firstName);
        instructor.setLastName(record.lastName);
        instructor.setUniversityUsername(record.universityUsername);
        instructor.setEmail(record.email);

        if(instructor.getUniversityUsername() != null){
            section.setInstructor(instructor);
        }

        return section;
    }

    @Override
    public TimeSlot mapTimeSlot(DenormalizedCourseData record) {
        if(record == null){
            throw new NullPointerException("Tried to map a null record to a time slot");
        }

        if(record.days == null || record.startTime == null || record.endTime == null){
            return null;
        }

        TimeSlot time = new TimeSlot();
        time.setId(record.timeId);
        time.setSectionId(record.sectionId);
        time.setStartTime(record.startTime);
        time.setEndTime(record.endTime);
        if(!"".equals(record.days)) {
            time.setDays(record.days);
        }

        if(record.days != null && !record.days.equals("")) {
            time.setDaysList(Arrays.stream(time.getDays().split(","))
                    .map(d -> WeekDay.getEnum(Integer.parseInt(d))).collect(Collectors.toList()));
        }
        else{
            time.setDaysList(Lists.newArrayList());
        }

        time.setBuilding(record.building);
        time.setRoom(record.room);

        time.setNonWeekly((record.nonWeekly > 0));

        return time;
    }
}
