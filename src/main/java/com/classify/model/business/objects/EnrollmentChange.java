package com.classify.model.business.objects;

import java.time.LocalDateTime;

/**
 * Created by Eric Newbury on 9/29/17.
 */
public class EnrollmentChange {
    private Integer id;
    private Integer sectionId;
    private Integer seats;
    private Integer seatsFilled;
    private LocalDateTime timeStamp;

    public EnrollmentChange() {}

    public EnrollmentChange(Integer sectionId, Integer seats, Integer seatsFilled, LocalDateTime timeStamp) {
        this.sectionId = sectionId;
        this.seats = seats;
        this.seatsFilled = seatsFilled;
        this.timeStamp = timeStamp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Integer getSeatsFilled() {
        return seatsFilled;
    }

    public void setSeatsFilled(Integer seatsFilled) {
        this.seatsFilled = seatsFilled;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }
}
