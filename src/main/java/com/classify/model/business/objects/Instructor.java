package com.classify.model.business.objects;

public class Instructor {
    private Integer id;
    private String firstName;
    private String lastName;
    private String universityUsername;
    private String email;
    private Integer syncedTerm;
    private Integer rmpId;
    private Double rmpRating;
    private String rmpLatestComment;
    private Integer universityId;
    private University university;
    

    public Instructor(){}

    public Instructor(String firstName, String lastName, String universityUsername, String email, Integer universityId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.universityUsername = universityUsername;
        this.email = email;
        this.universityId = universityId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUniversityUsername() {
        return universityUsername;
    }

    public void setUniversityUsername(String universityUsername) {
        this.universityUsername = universityUsername;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getRmpId() {
        return rmpId;
    }

    public void setRmpId(Integer rmpId) {
        this.rmpId = rmpId;
    }

    public Double getRmpRating() {
        return rmpRating;
    }

    public void setRmpRating(Double rmpRating) {
        this.rmpRating = rmpRating;
    }

    public String getRmpLatestComment() {
        return rmpLatestComment;
    }

    public void setRmpLatestComment(String rmpLatestComment) {
        this.rmpLatestComment = rmpLatestComment;
    }

    public Integer getUniversityId() {
        return universityId;
    }

    public void setUniversityId(Integer universityId) {
        this.universityId = universityId;
    }

    public Integer getSyncedTerm() {
        return syncedTerm;
    }

    public void setSyncedTerm(Integer syncedTerm) {
        this.syncedTerm = syncedTerm;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Instructor that = (Instructor) o;

        return !(universityUsername != null ? !universityUsername.equals(that.universityUsername) : that.universityUsername != null);

    }

    @Override
    public int hashCode() {
        return universityUsername != null ? universityUsername.hashCode() : 0;
    }
}
