package com.classify.model.business.objects.list.entry;

import com.classify.model.business.objects.enums.ListEntryType;

/**
 * Created by Eric Newbury on 6/15/17.
 */
public class SectionListEntry extends ListEntry {

    public SectionListEntry() {
        super();
        this.setType(ListEntryType.SECTION);
    }

    private Integer sectionId;

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }
}
