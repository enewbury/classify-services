package com.classify.model.business.objects;

/**
 * Created by Eric Newbury on 3/2/17.
 */
public class RegistrationWatchlistEntry {
    private Integer id;
    private Integer userId;
    private Integer sectionId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

}
