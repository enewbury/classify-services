package com.classify.model.business.objects;


import java.time.LocalDate;
import java.time.LocalDateTime;

public class Term {
    private Integer id;
    private Integer universityId;
    private University university;
    private String termCode;
    private String semester;
    private Integer year;
    private LocalDate firstDayOfClasses;
    private LocalDate lastDayOfClasses;
    private LocalDate withdrawDeadline;
    private LocalDateTime creationDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUniversityId() {
        return universityId;
    }

    public void setUniversityId(Integer universityId) {
        this.universityId = universityId;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    public String getTermCode() {
        return termCode;
    }

    public void setTermCode(String termCode) {
        this.termCode = termCode;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public LocalDate getFirstDayOfClasses() {
        return firstDayOfClasses;
    }

    public void setFirstDayOfClasses(LocalDate firstDayOfClasses) {
        this.firstDayOfClasses = firstDayOfClasses;
    }

    public LocalDate getLastDayOfClasses() {
        return lastDayOfClasses;
    }

    public void setLastDayOfClasses(LocalDate lastDayOfClasses) {
        this.lastDayOfClasses = lastDayOfClasses;
    }

    public LocalDate getWithdrawDeadline() {
        return withdrawDeadline;
    }

    public void setWithdrawDeadline(LocalDate withdrawDeadline) {
        this.withdrawDeadline = withdrawDeadline;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }


}
