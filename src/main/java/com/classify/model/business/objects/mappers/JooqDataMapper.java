package com.classify.model.business.objects.mappers;

import com.classify.model.WeekDay;
import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.Instructor;
import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.TimeSlot;
import com.classify.model.business.objects.enums.SectionType;
import com.google.common.collect.Lists;
import org.jooq.Record;
import org.jooq.tables.records.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.jooq.Tables.*;

public class JooqDataMapper implements DataMapper<Record> {

    //Business Objects to Jooq
    public JCourseRecord mapCoursePrimaryData(Course course, JCourseRecord r){
        r.setId(course.getId());
        r.setSubject(course.getSubject());
        r.setCourseNumber(course.getCourseNumber());
        r.setUniversityId(course.getUniversityId());
        return r;
    }

    public JCourseVersionRecord mapCourseVersionPrimaryData(Course course, JCourseVersionRecord r) {
        r.setId(course.getVersionId());
        r.setName(course.getName());
        r.setCreditsMin(course.getCreditsMin());
        r.setCreditsMax(course.getCreditsMax());
        r.setTermCode(course.getTermCode());
        return r;
    }

    public JInstructorRecord mapInstructorPrimaryData(Instructor inst, JInstructorRecord r){
        r.setId(inst.getId());
        r.setFirstName(inst.getFirstName());
        r.setLastName(inst.getLastName());
        r.setUniversityUsername(inst.getUniversityUsername());
        r.setEmail(inst.getEmail());
        r.setUniversityId(inst.getUniversityId());
        r.setSyncedTerm(inst.getSyncedTerm());
        return r;
    }

    public JSectionRecord mapSection(Section section, JSectionRecord r){
        r.setId(section.getId());
        r.setUniversitySectionId(section.getUniversitySectionId());
        r.setCourseId(section.getCourseId());
        r.setSectionName(section.getSectionName());
        r.setType(section.getType().toString());
        r.setSeats(section.getSeats());
        r.setSeatsFilled(section.getSeatsFilled());
        r.setTermId(section.getTermId());
        r.setInstructorId(section.getInstructorId());
        r.setCancelled(section.isCancelled());
        return r;
    }

    public JTimeSlotRecord mapTimeSlot(TimeSlot ts, JTimeSlotRecord r){
        r.setId(ts.getId());
        r.setNonWeekly(ts.getNonWeekly());
        r.setSectionId(ts.getSectionId());
        r.setStartTime(ts.getStartTime());
        r.setDays(ts.getDays());
        r.setEndTime(ts.getEndTime());
        r.setBuilding(ts.getBuilding());
        r.setRoom(ts.getRoom());
        return r;
    }

    //Jooq To Business Objects
    @Override
    public Course mapCourse(Record record) {
        Course course = new Course();

        course.setId((Integer) record.getValue(COURSE.getName()+"_"+COURSE.ID.getName()));
        course.setVersionId((Integer) record.getValue(COURSE_VERSION.getName()+"_"+COURSE_VERSION.ID.getName()));
        course.setName(record.getValue(COURSE_VERSION.NAME));
        course.setSubject(record.getValue(COURSE.SUBJECT));
        course.setCourseNumber(record.getValue(COURSE.COURSE_NUMBER));
        course.setCreditsMin(record.getValue(COURSE_VERSION.CREDITS_MIN));
        course.setCreditsMax(record.getValue(COURSE_VERSION.CREDITS_MAX));
        course.setSections(new ArrayList<>());
        course.setUniversityId(record.getValue(COURSE.UNIVERSITY_ID));
        if(record.fieldsRow().indexOf(COURSE_VERSION.DESCRIPTION) != -1){
            course.setDescription(record.get(COURSE_VERSION.DESCRIPTION));
        }
        course.setTermCode(record.getValue(COURSE_VERSION.TERM_CODE));

        return course;
    }

    @Override
    public Section mapSection(Record record){
        Section section = new Section();

        section.setId((Integer) record.getValue(SECTION.getName()+"_"+SECTION.ID.getName()));
        section.setSectionName(record.getValue(SECTION.SECTION_NAME));
        section.setUniversitySectionId(record.getValue(SECTION.UNIVERSITY_SECTION_ID));
        section.setType(SectionType.valueOf(record.get(SECTION.TYPE)));

        section.setSeats(record.getValue(SECTION.SEATS));
        section.setSeatsFilled(record.getValue(SECTION.SEATS_FILLED));
        section.setTermId(record.getValue(SECTION.TERM_ID));


        //set instructor if not null
        Instructor instructor = new Instructor();
        Integer instructorId = (Integer) record.getValue(INSTRUCTOR.getName()+"_"+INSTRUCTOR.ID.getName());

        if (instructorId != null) {
            instructor.setId(instructorId);
            instructor.setFirstName(record.getValue(INSTRUCTOR.FIRST_NAME));
            instructor.setLastName(record.getValue(INSTRUCTOR.LAST_NAME));
            instructor.setUniversityUsername(record.getValue(INSTRUCTOR.UNIVERSITY_USERNAME));
            instructor.setEmail(record.getValue(INSTRUCTOR.EMAIL));
            instructor.setUniversityId(record.getValue(INSTRUCTOR.UNIVERSITY_ID));
            instructor.setSyncedTerm(record.getValue(INSTRUCTOR.SYNCED_TERM));
            if(record.fieldsRow().indexOf(INSTRUCTOR.RMP_ID) != -1){
                instructor.setRmpId(record.get(INSTRUCTOR.RMP_ID));
                instructor.setRmpRating(record.get(INSTRUCTOR.RMP_RATING));
                instructor.setRmpLatestComment(record.get(INSTRUCTOR.RMP_LATEST_COMMENT));
            }

            section.setInstructorId(instructor.getId());
            section.setInstructor(instructor);
        }

        section.setTimeSlots(new ArrayList<>());

        return section;
    }

    @Override
    public TimeSlot mapTimeSlot(Record record){
        TimeSlot timeSlot = new TimeSlot();
        Integer timeId = (Integer) record.getValue(TIME_SLOT.getName()+"_"+TIME_SLOT.ID.getName());
        if (timeId == null) return null;

        timeSlot.setId(timeId);
        timeSlot.setStartTime(record.getValue(TIME_SLOT.START_TIME));
        timeSlot.setEndTime(record.getValue(TIME_SLOT.END_TIME));
        timeSlot.setDays(record.getValue(TIME_SLOT.DAYS));
        timeSlot.setBuilding(record.getValue(TIME_SLOT.BUILDING));
        timeSlot.setRoom(record.getValue(TIME_SLOT.ROOM));

        if (record.getValue(TIME_SLOT.NON_WEEKLY) != null) {
            timeSlot.setNonWeekly(record.getValue(TIME_SLOT.NON_WEEKLY));
        } else {
            timeSlot.setNonWeekly(false);
        }

        if (timeSlot.getDays() != null ) {
            timeSlot.setDaysList(Arrays.stream(timeSlot.getDays().split(","))
                    .map(d -> WeekDay.getEnum(Integer.parseInt(d))).collect(Collectors.toList()));
        } else {
            timeSlot.setDaysList(Lists.newArrayList());
        }

        return timeSlot;
    }

}
