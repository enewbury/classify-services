package com.classify.model.business.objects;

import com.classify.model.business.objects.enums.Standing;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Eric Newbury on 3/2/17.
 */
public class User {
    private Integer id;
    private Integer universityId;
    private String email;
    @JsonIgnore
    private String password;
    private String firstName;
    private String lastName;
    private Standing currentStanding;
    private String universityUsername;
    private boolean locked;
    private boolean verified;
    private boolean deleted;
    private boolean donated;

    public User(){}

    public User(User other) {
        this.id = other.id;
        this.universityId = other.universityId;
        this.email = other.email;
        this.password = other.password;
        this.firstName = other.firstName;
        this.lastName = other.lastName;
        this.currentStanding = other.currentStanding;
        this.universityUsername = other.universityUsername;
        this.locked = other.locked;
        this.verified = other.verified;
        this.deleted = other.deleted;
        this.donated = other.donated;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUniversityId() {
        return universityId;
    }

    public void setUniversityId(Integer universityId) {
        this.universityId = universityId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Standing getCurrentStanding() {
        return currentStanding;
    }

    public void setCurrentStanding(Standing currentStanding) {
        this.currentStanding = currentStanding;
    }

    public String getUniversityUsername() {
        return universityUsername;
    }

    public void setUniversityUsername(String universityUsername) {
        this.universityUsername = universityUsername;
    }

    public boolean isLocked() {
        return locked;
    }

    public User setLocked(boolean locked) {
        this.locked = locked;
        return this;
    }

    public boolean isVerified() {
        return verified;
    }

    public User setVerified(boolean verified) {
        this.verified = verified;
        return this;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public User setDeleted(boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public boolean isDonated() {
        return donated;
    }

    public void setDonated(boolean donated) {
        this.donated = donated;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", universityId=" + universityId +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", currentStanding=" + currentStanding +
                ", universityUsername='" + universityUsername + '\'' +
                ", locked=" + locked +
                ", verified=" + verified +
                ", deleted=" + deleted +
                ", donated=" + donated +
                '}';
    }
}
