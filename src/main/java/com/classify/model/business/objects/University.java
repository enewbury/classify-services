package com.classify.model.business.objects;

import com.classify.model.Semester;
import org.joda.time.DateTime;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.List;

public class University {
    public static final int UVM = 1;

    private Integer id;
    private String acronym;
    private String name;
    private String emailDomain;
    private String availableSemesters;
    private Integer rmpId;
    private LocalTime registrationOpens;
    private LocalTime registrationCloses;
    private Integer availableAccounts;

    private List<Semester> availableSemestersList;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailDomain() {
        return emailDomain;
    }

    public void setEmailDomain(String emailDomain) {
        this.emailDomain = emailDomain;
    }

    public String getAvailableSemesters() {
        return availableSemesters;
    }

    public void setAvailableSemesters(String availableSemesters) {
        this.availableSemesters = availableSemesters;
    }

    public Integer getRmpId() {
        return rmpId;
    }

    public void setRmpId(Integer rmpId) {
        this.rmpId = rmpId;
    }

    public List<Semester> getAvailableSemestersList() {
        return availableSemestersList;
    }

    public void setAvailableSemestersList(List<Semester> availableSemestersList) {
        this.availableSemestersList = availableSemestersList;
    }

    public LocalTime getRegistrationOpens() {
        return registrationOpens;
    }

    public void setRegistrationOpens(LocalTime registrationOpens) {
        this.registrationOpens = registrationOpens;
    }

    public LocalTime getRegistrationCloses() {
        return registrationCloses;
    }

    public void setRegistrationCloses(LocalTime registrationCloses) {
        this.registrationCloses = registrationCloses;
    }

    public Integer getAvailableAccounts() {
        return availableAccounts;
    }

    public void setAvailableAccounts(Integer availableAccounts) {
        this.availableAccounts = availableAccounts;
    }

    @Override
    public String toString() {
        return acronym;
    }
}
