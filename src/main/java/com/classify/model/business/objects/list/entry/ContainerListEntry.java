package com.classify.model.business.objects.list.entry;

import java.util.List;

/**
 * Created by Eric Newbury on 8/1/17.
 */
public abstract class ContainerListEntry extends ListEntry {
    private List<ListEntry> entries;

    public List<ListEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<ListEntry> entries) {
        this.entries = entries;
    }
}
