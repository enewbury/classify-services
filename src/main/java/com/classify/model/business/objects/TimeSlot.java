package com.classify.model.business.objects;

import com.classify.model.WeekDay;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Time;
import java.time.LocalTime;
import java.util.List;

public class TimeSlot implements Comparable<TimeSlot>{
    private Integer id;
    private Integer sectionId;
    @JsonIgnore
    private Section section;
    private LocalTime startTime;
    private LocalTime endTime;
    private String days;
    private List<WeekDay> daysList;
    private String building;
    private String room;
    private Boolean nonWeekly;

    public TimeSlot(){}

    public TimeSlot(LocalTime startTime, LocalTime endTime, String days, String building, String room, Boolean nonWeekly) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.days = days;
        this.building = building;
        this.room = room;
        this.nonWeekly = nonWeekly;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public List<WeekDay> getDaysList() {
        return daysList;
    }

    public void setDaysList(List<WeekDay> daysList) {
        this.daysList = daysList;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Boolean getNonWeekly() {
        return nonWeekly;
    }

    public void setNonWeekly(Boolean nonWeekly) {
        this.nonWeekly = nonWeekly;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TimeSlot timeSlot = (TimeSlot) o;

        if (startTime != null ? !startTime.equals(timeSlot.startTime) : timeSlot.startTime != null) return false;
        if (endTime != null ? !endTime.equals(timeSlot.endTime) : timeSlot.endTime != null) return false;
        return !(days != null ? !days.equals(timeSlot.days) : timeSlot.days != null);

    }

    @Override
    public int hashCode() {
        int result = startTime != null ? startTime.hashCode() : 0;
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (days != null ? days.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(TimeSlot o) {
        //throws null pointer exception if o is null.  Should never be null;
        if(o.startTime == null || o.endTime == null || o.days == null){
            throw new NullPointerException("startTime,endTime, and days must all be non-null.  Remove any rows in DB where this is the case, "+
                    "and check data loader to make sure they filter null time slots correctly");
        }
        if (this.startTime.equals(o.getStartTime())){
            return this.days.compareTo(o.getDays());
        }
        else{
            return this.startTime.compareTo(o.getStartTime());
        }
    }
}
