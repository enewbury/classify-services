package com.classify.model.business.objects.enums;

/**
 * Created by Eric Newbury on 8/1/17.
 */
public enum ListEntryType {SCHEDULE, GROUP, SECTION}
