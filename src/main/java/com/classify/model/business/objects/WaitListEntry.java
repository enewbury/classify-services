package com.classify.model.business.objects;

import java.time.ZonedDateTime;

/**
 * Created by Eric Newbury on 3/14/17.
 */
public class WaitListEntry {
    private Integer id;
    private Integer userId;
    private Integer sectionId;
    private ZonedDateTime lastNotified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public ZonedDateTime getLastNotified() {
        return lastNotified;
    }

    public void setLastNotified(ZonedDateTime lastNotified) {
        this.lastNotified = lastNotified;
    }
}
