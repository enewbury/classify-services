package com.classify.model.business.objects.list.entry;

import com.classify.model.business.objects.enums.ListEntryType;

/**
 * Created by Eric Newbury on 6/15/17.
 */
public class ScheduleListEntry extends ContainerListEntry {

    public ScheduleListEntry() {
        this.setType(ListEntryType.SCHEDULE);
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
