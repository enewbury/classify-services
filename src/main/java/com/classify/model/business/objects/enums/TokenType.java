package com.classify.model.business.objects.enums;

/**
 * Created by Eric Newbury on 8/14/17.
 */
public enum TokenType {VERIFICATION, UPDATE_EMAIL, PASSWORD}
