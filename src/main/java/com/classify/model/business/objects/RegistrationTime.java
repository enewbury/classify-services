package com.classify.model.business.objects;

import com.classify.model.business.objects.enums.Standing;

import java.time.LocalDateTime;

public class RegistrationTime {
    private Integer id;
    private Integer termId;
    private Term term;
    private Standing standing;
    private LocalDateTime start;
    private LocalDateTime end;
    private Integer processed;

    public RegistrationTime() {}

    public RegistrationTime(Standing standing, LocalDateTime start, LocalDateTime end) {
        this.standing = standing;
        this.start = start;
        this.end = end;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTermId() {
        return termId;
    }

    public void setTermId(Integer termId) {
        this.termId = termId;
    }

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }

    public Standing getStanding() {
        return standing;
    }

    public void setStanding(Standing standing) {
        this.standing = standing;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public Integer getProcessed() {
        return processed;
    }

    public void setProcessed(Integer processed) {
        if(processed > 1 || processed < 0) throw new RuntimeException("Processed must be 0 or 1");
        this.processed = processed;
    }
}
