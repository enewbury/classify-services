package com.classify.model.business.objects;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class Course implements Comparable<Course>{
    private Integer id;
    private Integer versionId;
    private String name;
    private String subject;
    private String courseNumber;
    private Double creditsMin;
    private Double creditsMax;
    private Integer universityId;
    private University university;
    private String termCode;

    private String description;

    private List<Section> sections = new ArrayList<>();

    public Course(){}

    public Course(String name, String subject, String courseNumber, Double creditsMin, Double creditsMax, Integer universityId) {
        this.name = name;
        this.subject = subject;
        this.courseNumber = courseNumber;
        this.creditsMin = creditsMin;
        this.creditsMax = creditsMax;
        this.universityId = universityId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer courseId) {
        this.id = courseId;
    }

    public Integer getVersionId() {
        return versionId;
    }

    public void setVersionId(Integer versionId) {
        this.versionId = versionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @NotNull
    public String getCourseNumber() {
        return courseNumber;
    }

    public void setCourseNumber(String courseNumber) {
        this.courseNumber = courseNumber;
    }

    public Double getCreditsMin() {
        return creditsMin;
    }

    public void setCreditsMin(Double creditsMin) {
        this.creditsMin = creditsMin;}

    public Double getCreditsMax() {
        return creditsMax;
    }

    public void setCreditsMax(Double creditsMax) {
        this.creditsMax = creditsMax;}

    public Integer getUniversityId() {
        return universityId;
    }

    public void setUniversityId(Integer universityId) {
        this.universityId = universityId;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    @NotNull
    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public String getTermCode() {
        return termCode;
    }

    public void setTermCode(String termCode) {
        this.termCode = termCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        return subject.equals(course.subject) && courseNumber.equals(course.courseNumber);

    }

    @Override
    public int hashCode() {
        int result = subject.hashCode();
        result = 31 * result + courseNumber.hashCode();
        return result;
    }

    @Override
    public int compareTo(Course o){
        //throws null pointer exception if o is null.  Should never be null;
        if(this.subject.equals(o.subject)){
            return this.courseNumber.compareTo(o.courseNumber);
        } else {
            return this.subject.compareTo(o.subject);
        }
    }
}
