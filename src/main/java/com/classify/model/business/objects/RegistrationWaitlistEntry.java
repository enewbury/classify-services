package com.classify.model.business.objects;

import com.classify.dispatcher.RegistrationStatus;

import java.time.LocalDateTime;

/**
 * Created by Eric Newbury on 3/2/17.
 */
public class RegistrationWaitlistEntry {
    private Integer id;
    private Integer userId;
    private Integer sectionId;
    private LocalDateTime queueTime;
    private LocalDateTime attemptTime;
    private RegistrationStatus status;
    private String error;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public void setQueueTime(LocalDateTime queueTime) {
        this.queueTime = queueTime;
    }

    public LocalDateTime getAttemptTime() {
        return attemptTime;
    }

    public void setAttemptTime(LocalDateTime attemptTime) {
        this.attemptTime = attemptTime;
    }

    public RegistrationStatus getStatus() {
        return status;
    }

    public void setStatus(RegistrationStatus status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
