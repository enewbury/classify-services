package com.classify.model.business.objects;

import com.classify.dispatcher.RegistrationStatus;
import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by Eric Newbury on 3/14/17.
 */
public class RegistrationAttempt implements Serializable {
    private Integer id;
    private Integer userId;
    private Integer sectionId;
    private DateTime attemptTime;
    private RegistrationStatus status;
    private String error;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public DateTime getAttemptTime() {
        return attemptTime;
    }

    public void setAttemptTime(DateTime attemptTime) {
        this.attemptTime = attemptTime;
    }

    public RegistrationStatus getStatus() {
        return status;
    }

    public void setStatus(RegistrationStatus status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
