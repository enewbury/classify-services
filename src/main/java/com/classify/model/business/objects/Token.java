package com.classify.model.business.objects;

import com.classify.model.business.objects.enums.TokenType;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.LocalDateTime;

/**
 * Created by Eric Newbury on 8/11/17.
 */
public class Token {
    private Integer id;
    private Integer userId;
    private TokenType type;
    private String token;
    private LocalDateTime expires;
    private String metaInformation;

    public Token() {}

    public Token(Integer userId, TokenType type, LocalDateTime expires, String metaInformation) {
        this.userId = userId;
        this.type = type;
        this.expires = expires;

        SecureRandom random = new SecureRandom();
        this.token = new BigInteger(130, random).toString(32);
        this.metaInformation = metaInformation;
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public TokenType getType() {
        return type;
    }

    public String getToken() {
        return token;
    }

    public LocalDateTime getExpires() {
        return expires;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Token setUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setExpires(LocalDateTime expires) {
        this.expires = expires;
    }

    public String getMetaInformation() {
        return metaInformation;
    }

    public void setMetaInformation(String metaInformation) {
        this.metaInformation = metaInformation;
    }
}
