package com.classify.model.business.objects.mappers;

import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.TimeSlot;

public interface DataMapper<R> {
    Course mapCourse(R record);
    Section mapSection(R record);
    TimeSlot mapTimeSlot(R record);
}
