package com.classify.model.business.objects;

import com.classify.model.business.objects.enums.SectionType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class Section implements Comparable<Section>{
    private Integer id;
    private Integer universitySectionId;
    private Integer courseId;
    @JsonIgnore
    private Course course;
    private String sectionName;
    private SectionType type;
    private Integer seats;
    private Integer seatsFilled;
    private Integer termId;
    private Integer instructorId;
    private Instructor instructor;
    private boolean cancelled = false;

    private List<TimeSlot> timeSlots = new ArrayList<>();

    public Section(){}

    public Section(Section other) {
        this.id = other.id;
        this.universitySectionId = other.universitySectionId;
        this.courseId = other.courseId;
        this.course = other.course;
        this.sectionName = other.sectionName;
        this.type = other.type;
        this.seats = other.seats;
        this.seatsFilled = other.seatsFilled;
        this.termId = other.termId;
        this.instructorId = other.instructorId;
        this.instructor = other.instructor;
        this.cancelled = other.cancelled;
        this.timeSlots = other.timeSlots;
    }

    public Section(Integer universitySectionId, String sectionName, SectionType type, Integer seats, Integer seatsFilled) {
        this.universitySectionId = universitySectionId;
        this.sectionName = sectionName;
        this.type = type;
        this.seats = seats;
        this.seatsFilled = seatsFilled;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @NotNull
    public Integer getUniversitySectionId() {
        return universitySectionId;
    }

    public void setUniversitySectionId(Integer universitySectionId) {
        this.universitySectionId = universitySectionId;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public SectionType getType() {
        return type;
    }

    public void setType(SectionType type) {
        this.type = type;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Integer getSeatsFilled() {
        return seatsFilled;
    }

    public void setSeatsFilled(Integer seatsFilled) {
        this.seatsFilled = seatsFilled;
    }

    public Integer getTermId() {
        return termId;
    }

    public void setTermId(Integer termId) {
        this.termId = termId;
    }

    public Integer getInstructorId() {
        return instructorId;
    }

    public void setInstructorId(Integer instructorId) {
        this.instructorId = instructorId;
    }

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    @NotNull
    public List<TimeSlot> getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(List<TimeSlot> timeSlots) {
        this.timeSlots = timeSlots;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Section section = (Section) o;

        return universitySectionId.equals(section.universitySectionId);

    }

    @Override
    public int hashCode() {
        return universitySectionId.hashCode();
    }

    @Override
    public int compareTo(Section o){
        return this.sectionName.compareTo(o.getSectionName());
    }
}
