package com.classify.model;

import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.Term;

import java.util.List;
import java.util.Map;

/**
 * Created by Eric Newbury on 8/21/17.
 */
public class SectionUpdates {
    private Term term;
    private List<Section> updatedSections;
    private Map<Integer, Section> previousValues;

    public SectionUpdates(EnrollmentChanges changeSet) {
        this.term = changeSet.getTerm();
        this.updatedSections = changeSet.getSections().getUpdates();
        this.previousValues = changeSet.getPreviousSections();
    }

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }

    public List<Section> getUpdatedSections() {
        return updatedSections;
    }

    public void setUpdatedSections(List<Section> updatedSections) {
        this.updatedSections = updatedSections;
    }

    public Map<Integer, Section> getPreviousValues() {
        return previousValues;
    }

    public void setPreviousValues(Map<Integer, Section> previousValues) {
        this.previousValues = previousValues;
    }
}
