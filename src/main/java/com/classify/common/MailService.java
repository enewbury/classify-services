package com.classify.common;

import com.classify.config.ClassifyProperties;
import com.classify.exceptions.BrokenIntegrationException;
import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.User;
import com.classify.model.data.model.*;
import freemarker.template.Configuration;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class MailService {
    private Log log = LogFactory.getLog(MailService.class);

    private final EmailSender mailSender;
    private final Configuration freemarkerConfig;

    private static final String TEMPLATE_PATH = "mail/";
    private static final String SENDING_ADDRESS = "noreply@classifyregistration.com";
    private static final String SENDING_NAME = "Classify";

    @Value("${com.classify.domain}")
    private String domain;

    @Value("${com.classify.mock-email}")
    private Boolean mockEmail;

    @Autowired
    public MailService(Configuration freemarkerConfig, EmailSender mailSender) {
        this.freemarkerConfig = freemarkerConfig;
        this.mailSender = mailSender;

    }

    private void composeEmail(String toName, String toEmail, String subject, String templateFile, EmailModel dataModel){
        if(mockEmail){
            toEmail = "enewbury94@icloud.com";
        }

        try {
            dataModel.setDomain(domain);

            String body = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerConfig.getTemplate(TEMPLATE_PATH + templateFile), dataModel);

            mailSender.sendEmail(new InternetAddress(toEmail, toName), new InternetAddress(SENDING_ADDRESS, SENDING_NAME), subject, body, Jsoup.parse(body).text());

            log.info("Sent email with subject "+ subject +" to " + toEmail);

        } catch (Exception ex) {
            log.error("Unable to send message with subject: " + subject, ex);
        }
    }

    private String getName(User user){
        return (user.getFirstName() != null) ? user.getFirstName() : user.getEmail();
    }

    public void sendBrokenIntegrationNotification(BrokenIntegrationException e) {
            BrokenIntegrationModel dataModel = new BrokenIntegrationModel(
                    e.getLocation(),
                    ExceptionUtils.getFullStackTrace(e),
                    LocalDateTime.now().toString()
            );
            composeEmail("Eric Newbury", "enewbury94@icloud.com", "Broken Integration Exception", "broken_integration_email.ftl", dataModel);
    }


    public void sendAccountVerificationEmail(User user, String token) {
        TokenEmailModel dataModel = new TokenEmailModel(getName(user), token);
        composeEmail(user.getFirstName() + " " + user.getLastName(), user.getEmail(), "Verify Your Account", "verify_email.ftl", dataModel);
    }

    public void sendPasswordResetEmail(User user, String token) {
        TokenEmailModel dataModel = new TokenEmailModel(getName(user), token);
        composeEmail(user.getFirstName() + " " + user.getLastName(), user.getEmail(), "Reset Your Password", "reset_password.ftl", dataModel);

    }

    public void sendEnrollmentChangeNotification(User user, List<Section> openings, List<Section> closings) {
        SectionUpdatesDataModel dataModel = new SectionUpdatesDataModel(getName(user), openings, closings);
        composeEmail(
                user.getFirstName() + " " + user.getLastName(),
                user.getEmail(),
                (openings.size() > 0) ? "Watchlist Opening!" : "Class filled",
                "openings.ftl",
                dataModel
            );
    }

    public void sendDonationConfirmationEmail(String email, Integer amount) {
        DonationDataModel dataModel = new DonationDataModel(email, amount);
        composeEmail(null, email, "Thank you for your donation!", "donation.ftl", dataModel);
    }

    public void sendNewEmailVerificationEmail(User user, String token, String newEmail) {
        NewEmailDataModel dataModel = new NewEmailDataModel(token);
        composeEmail(user.getFirstName() + " " + user.getLastName(), newEmail, "Verify This Email", "verify_new_email.ftl", dataModel);
    }

    public void sendFeedback(User user, String feedback) {
        FeedbackModel dataModel = new FeedbackModel(getName(user), user.getEmail(), feedback);
        composeEmail("Eric Newbury", "enewbury94@icloud.com", "Feedback", "feedback.ftl", dataModel);
    }
}
