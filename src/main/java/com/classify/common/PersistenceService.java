package com.classify.common;

import com.classify.crawler.helper.TermConverter;
import com.classify.model.Semester;
import com.classify.model.business.objects.RegistrationTime;
import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.Term;
import com.classify.model.business.objects.University;
import com.classify.model.business.objects.enums.Standing;
import com.google.common.base.Splitter;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jooq.DSLContext;
import org.jooq.tables.records.JAccountWaitlistRecord;
import org.jooq.tables.records.JInstructorRecord;
import org.jooq.tables.records.JTermRecord;
import org.jooq.tables.records.JUniversityRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.jooq.Tables.*;

@Service
public class PersistenceService {
    private Log log = LogFactory.getLog(PersistenceService.class);

    protected final DSLContext db;

    @Autowired
    public PersistenceService(@SuppressWarnings("SpringJavaAutowiringInspection") DSLContext db) {
        this.db = db;
    }

    public University getUniversityById(int id){
        University university = db.selectFrom(UNIVERSITY).where(UNIVERSITY.ID.eq(id)).fetchOneInto(University.class);
        university.setAvailableSemestersList(new ArrayList<>());
        List<String> seasons = Splitter.on(",").splitToList(university.getAvailableSemesters());
        seasons.forEach(s -> university.getAvailableSemestersList().add(Semester.valueOf(s.toUpperCase())));

        return university;
    }

    public University getUniversityFromDomain(String domain){
        return db.selectFrom(UNIVERSITY).where(UNIVERSITY.EMAIL_DOMAIN.eq(domain)).fetchOneInto(University.class);
    }

    public List<University> getAllUniversities() {
        List<University> universities = db.selectFrom(UNIVERSITY).fetchInto(University.class);

        //convert string of seasons to list.
        universities.forEach(u -> {
            u.setAvailableSemestersList(new ArrayList<>());
            List<String> seasons = Splitter.on(",").splitToList(u.getAvailableSemesters());
            seasons.forEach(s -> u.getAvailableSemestersList().add(Semester.valueOf(s.toUpperCase())));
        });

        return universities;
    }

    public void updateUniversity(University university){
        JUniversityRecord record = db.newRecord(UNIVERSITY, university);
        record.changed(UNIVERSITY.ID, false);
        record.update();
    }

    public void addToAccountWaitlist(String email){
        JAccountWaitlistRecord record = db.newRecord(ACCOUNT_WAITLIST);
        record.setEmail(email);
        record.setDate(LocalDateTime.now());
        record.insert();
    }

    public Term getTermById(int termId){
        return db.selectFrom(TERM).where(TERM.ID.eq(termId)).fetchOneInto(Term.class);
    }

    public Term getTerm(Integer universityId, Semester semester, int year){
        return db.selectFrom(TERM)
                .where(
                        TERM.SEMESTER.eq(semester.toString().toLowerCase()).and(
                                TERM.YEAR.eq(year)).and(
                                TERM.UNIVERSITY_ID.eq(universityId))
                )
                .fetchOneInto(Term.class);
    }

    public Term insertTerm(Integer universityId, Semester semester, int year){
        log.info("Adding new term");
        //Save term to database.
        String termCode = new TermConverter().getTermCode(semester, Integer.toString(year));
        String seasonString = WordUtils.capitalizeFully(semester.toString());

        JTermRecord term = db.newRecord(TERM);
        term.setUniversityId(universityId).setTermCode(termCode).setSemester(seasonString).setYear(year).setCreationDate(LocalDateTime.now());
        term.insert();

        return term.into(Term.class);
    }

    public List<JInstructorRecord> getInstructorsForUniversity(int id) {
        return db.selectFrom(INSTRUCTOR).where(INSTRUCTOR.UNIVERSITY_ID.eq(id)).fetch();
    }

    public void updateRMPIdForUniversity(int id, int rmpSchoolId) {
        db.update(UNIVERSITY).set(UNIVERSITY.RMP_ID, rmpSchoolId).where(UNIVERSITY.ID.eq(id)).execute();
    }

    public void updateInstructorRMPData(JInstructorRecord instructor) {
        log.info("updating instructor RMP data for: "+instructor.getFirstName()+" "+instructor.getLastName());
        instructor.update();
    }

    public Section getSectionById(Integer sectionId) {
        return db.selectFrom(SECTION).where(SECTION.ID.eq(sectionId)).fetchOneInto(Section.class);
    }

}
