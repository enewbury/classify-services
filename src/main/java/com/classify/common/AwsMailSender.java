package com.classify.common;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;

/**
 * Created by Eric Newbury on 9/19/17.
 */
@Profile("aws")
@Service
public class AwsMailSender implements EmailSender{

    private final AmazonSimpleEmailService awsSimpleEmailService;

    @Autowired
    public AwsMailSender(AmazonSimpleEmailService awsSimpleEmailService) {
        this.awsSimpleEmailService = awsSimpleEmailService;
    }

    public void sendEmail(InternetAddress to, InternetAddress from, String subject, String htmlBody, String textBody) throws Exception{

            SendEmailRequest request = new SendEmailRequest()
                    .withDestination(
                            new Destination().withToAddresses(to.toString()))
                    .withMessage(new Message()
                            .withBody(new Body()
                                    .withHtml(new Content()
                                            .withCharset("UTF-8").withData(htmlBody))
                                    .withText(new Content()
                                            .withCharset("UTF-8").withData(textBody)))
                            .withSubject(new Content()
                                    .withCharset("UTF-8").withData(subject)))
                    .withSource(from.toString());
            awsSimpleEmailService.sendEmail(request);
    }
}
