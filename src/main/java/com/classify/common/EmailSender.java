package com.classify.common;

import javax.mail.internet.InternetAddress;

/**
 * Created by Eric Newbury on 9/19/17.
 */
public interface EmailSender {
    void sendEmail(InternetAddress to, InternetAddress from, String subejct, String htmlBody, String textBody) throws Exception;
}
