package com.classify;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * Created by Eric Newbury on 5/2/17.
 */
@RestController
public class HealthCheckRestController {

    @RequestMapping("/_ah/health")
    public String healthy(HttpServletRequest request) {
        return "Still surviving.";
    }
}
