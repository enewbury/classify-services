package com.classify.api.rest;

import com.classify.api.service.ListsService;
import com.classify.api.service.persistence.ListsPersistenceService;
import com.classify.config.auth.CurrentUser;
import com.classify.exceptions.ClassifyClientErrorException;
import com.classify.exceptions.ErrorType;
import com.classify.model.business.objects.User;
import com.classify.model.business.objects.list.entry.GroupListEntry;
import com.classify.model.business.objects.list.entry.ListEntry;
import com.classify.model.business.objects.list.entry.ScheduleListEntry;
import com.classify.model.business.objects.list.entry.SectionListEntry;
import com.classify.model.rest.EmptyRestResponse;
import com.classify.model.rest.request.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Eric Newbury on 7/31/17.
 */
@RestController
@RequestMapping(value="api/v1/lists", consumes = "application/json", produces="application/json")
@SuppressWarnings("unused")
public class ListsRestService {
    private static final Log log = LogFactory.getLog(ListsRestService.class);

    private final ListsService listsService;
    private final ListsPersistenceService listsPersistenceService;

    @Autowired
    public ListsRestService(ListsService listsService, ListsPersistenceService listsPersistenceService) {
        this.listsService = listsService;
        this.listsPersistenceService = listsPersistenceService;
    }

    @GetMapping("")
    public ResponseEntity<List<ListEntry>> getUserLists(@RequestParam("termId") Integer termId, @CurrentUser User user){
        List<ListEntry> lists = listsService.loadListsForUserAndTerm(user.getId(), termId);

        return ResponseEntity.ok(lists);
    }

    @PostMapping("/schedules")
    public ResponseEntity<Map<String,Integer>> addSchedule(@RequestBody NewScheduleRequest request, @CurrentUser User user){
        ScheduleListEntry scheduleListEntry = listsService.addSchedule(request.getScheduleName(), user.getId(), request.getTermId());
        return ResponseEntity.ok(Collections.singletonMap("id",scheduleListEntry.getId()));
    }

    @PostMapping("/groups")
    public ResponseEntity<Map<String, Integer>> addGroup(@RequestBody NewGroupRequest request, @CurrentUser User user) throws ClassifyClientErrorException {

        if(request.getParentId() == null){
            log.error("Tried to add a folder with no parent id");
            throw new ClassifyClientErrorException(ErrorType.ENTRY_NEEDS_PARENT, HttpStatus.SC_BAD_REQUEST, "Tried to add a folder with no parent id.");
        }

        GroupListEntry groupScheduleEntry = listsService.newFolder(user.getId(), request.getTermId(), request.getParentId(), request.getFolderName());
        return ResponseEntity.ok(Collections.singletonMap("id", groupScheduleEntry.getId()));
    }

    @PostMapping("/sections")
    public ResponseEntity<Map<String,Integer>> addSectionToList(@RequestBody AddSectionToScheduleRequest request, @CurrentUser User user) throws ClassifyClientErrorException {

        if(request.getParentId() == null) {
            log.error("Tried to section  with no parent id");
            throw new ClassifyClientErrorException(ErrorType.ENTRY_NEEDS_PARENT, HttpStatus.SC_BAD_REQUEST, "Tried to add a section with no parent id.");
        }

        SectionListEntry sectionScheduleEntry = listsService.addSectionToList(user.getId(), request.getTermId(), request.getParentId(), request.getSectionId());
        return ResponseEntity.ok(Collections.singletonMap("id", sectionScheduleEntry.getId()));
    }

    @PreAuthorize("@authService.checkOwnsEntry(principal, #entryId) && @authService.checkOwnsEntry(principal, #request.getNewParentId())")
    @PatchMapping("/{entryId}")
    public ResponseEntity<EmptyRestResponse> changeEntryParent(@PathVariable("entryId") Integer entryId,
                                                               @RequestBody UpdateParentRequest request){
        listsPersistenceService.updateParentId(entryId, request.getNewParentId());
        return ResponseEntity.ok(new EmptyRestResponse());
    }

    @PreAuthorize("@authService.checkOwnsEntry(principal, #entryId)")
    @PutMapping("/schedules/{entryId}")
    public ResponseEntity<EmptyRestResponse> updateScheduleName(@PathVariable("entryId") Integer entryId,
                                                                @RequestBody UpdateScheduleNameRequest request){

        listsPersistenceService.updateName(entryId, request.getScheduleName());
        return ResponseEntity.ok().body(new EmptyRestResponse());
    }

    @PreAuthorize("@authService.checkOwnsEntry(principal, #entryId)")
    @PutMapping("/groups/{entryId}")
    public ResponseEntity<EmptyRestResponse> updateGroupName(@PathVariable("entryId") Integer entryId,
                                                             @RequestBody UpdateGroupNameRequest request){

        listsPersistenceService.updateGroupName(entryId, request.getGroupName());
        return ResponseEntity.ok().body(new EmptyRestResponse());
    }

    @PreAuthorize("@authService.checkOwnsEntry(principal, #entryId)")
    @DeleteMapping("/schedules/{entryId}")
    public ResponseEntity<EmptyRestResponse> deleteSchedule(@PathVariable("entryId")Integer entryId){
        listsPersistenceService.deleteSchedule(entryId);
        return ResponseEntity.ok().body(new EmptyRestResponse());
    }

    @PreAuthorize("@authService.checkOwnsEntry(principal, #entryId)")
    @DeleteMapping("/groups/{entryId}")
    public ResponseEntity<EmptyRestResponse> deleteGroup(@PathVariable("entryId")Integer entryId){
        listsPersistenceService.deleteGroup(entryId);

        return ResponseEntity.ok().body(new EmptyRestResponse());
    }

    @PreAuthorize("@authService.checkOwnsEntry(principal, #entryId)")
    @DeleteMapping("/sections/{entryId}")
    public ResponseEntity<EmptyRestResponse> deleteSectionFromList(@PathVariable("entryId") Integer entryId) {
        listsPersistenceService.removeSectionFromList(entryId);

        return ResponseEntity.ok().body(new EmptyRestResponse());
    }
}
