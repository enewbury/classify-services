package com.classify.api.rest;

import com.classify.api.service.CourseDataService;
import com.classify.config.auth.CurrentUser;
import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.Term;
import com.classify.model.business.objects.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Eric Newbury on 6/27/17.
 */
@RestController
@RequestMapping("api/v1/course-data")
@SuppressWarnings("unused")
public class CourseDataRestService {

    private final CourseDataService courseDataService;

    @Autowired
    public CourseDataRestService(CourseDataService courseDataService) {
        this.courseDataService = courseDataService;
    }

    @GetMapping("/terms")
    public ResponseEntity<List<Term>> getLatest4Terms(@CurrentUser User user){
        List<Term> terms = courseDataService.getLast4Terms(user.getUniversityId());

        return ResponseEntity.ok(terms);
    }

    @PreAuthorize("@authService.termIsFromCorrectUniversity(principal, #termId)")
    @GetMapping("/term/{termId}")
    public ResponseEntity<List<Course>> getCourseDataForTerm(@PathVariable("termId") Integer termId){
        return ResponseEntity.ok(courseDataService.getCourseDataForTerm(termId));
    }

    @PreAuthorize("@authService.sectionIsFromCorrectUniversity(principal, #sectionId)")
    @GetMapping("/section/{sectionId}/changes")
    public ResponseEntity<Map<String, List>> getSectionMetrics(@PathVariable("sectionId") Integer sectionId){
        return ResponseEntity.ok(Collections.singletonMap("changes", courseDataService.getSectionMetrics(sectionId)));
    }

}
