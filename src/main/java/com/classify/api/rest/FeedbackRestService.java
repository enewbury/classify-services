package com.classify.api.rest;

import com.classify.common.MailService;
import com.classify.config.auth.CurrentUser;
import com.classify.model.business.objects.User;
import com.classify.model.rest.EmptyRestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by Eric Newbury on 9/20/17.
 */
@RestController
@RequestMapping(value="api/v1/feedback", consumes = "application/json", produces="application/json")
public class FeedbackRestService {

    private final MailService mailService;

    @Autowired
    public FeedbackRestService(MailService mailService) {
        this.mailService = mailService;
    }

    @PostMapping
    public ResponseEntity<EmptyRestResponse> sendFeedback(@CurrentUser User user, @RequestBody Map<String, String> request){
        mailService.sendFeedback(user, request.get("feedback"));
        return ResponseEntity.ok(new EmptyRestResponse());
    }
}
