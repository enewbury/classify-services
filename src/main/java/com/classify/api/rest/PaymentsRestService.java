package com.classify.api.rest;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import com.classify.api.service.persistence.PaymentsPersistenceService;
import com.classify.api.service.persistence.UserPersistenceService;
import com.classify.common.MailService;
import com.classify.config.auth.CurrentUser;
import com.classify.exceptions.ClassifyClientErrorException;
import com.classify.exceptions.ErrorType;
import com.classify.model.business.objects.User;
import com.classify.model.rest.request.DonationRequest;
import org.apache.http.HttpStatus;
import org.jooq.tools.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

/**
 * Created by Eric Newbury on 9/14/17.
 */
@RestController
@RequestMapping(value="api/v1/payments", consumes = "application/json", produces="application/json")
public class PaymentsRestService {

    private final BraintreeGateway braintreeGateway;
    private final PaymentsPersistenceService paymentsPersistenceService;
    private final UserPersistenceService userPersistenceService;
    private final MailService mailService;

    @Autowired
    public PaymentsRestService(BraintreeGateway braintreeGateway, PaymentsPersistenceService paymentsPersistenceService, MailService mailService, UserPersistenceService userPersistenceService) {
        this.braintreeGateway = braintreeGateway;
        this.paymentsPersistenceService = paymentsPersistenceService;
        this.mailService = mailService;
        this.userPersistenceService = userPersistenceService;
    }

    @GetMapping("/braintreeToken")
    public Map<String, String> getBraintreeToken(){
        return Collections.singletonMap("clientToken", braintreeGateway.clientToken().generate());
    }

    @PostMapping("/donation")
    public void processDonation(@CurrentUser User user, @RequestBody DonationRequest restRequest) throws ClassifyClientErrorException {
        if(restRequest.getAmount() != null && restRequest.getAmount() > 0) {
            TransactionRequest request = new TransactionRequest()
                    .amount(new BigDecimal(restRequest.getAmount()))
                    .paymentMethodNonce(restRequest.getNonce())
                    .options()
                    .submitForSettlement(true)
                    .done();

            braintreeGateway.transaction().sale(request);

            if(!StringUtils.isBlank(restRequest.getEmail())){
                paymentsPersistenceService.saveDonation(restRequest.getEmail(), restRequest.getAmount());
                mailService.sendDonationConfirmationEmail(restRequest.getEmail(), restRequest.getAmount());
            }

            if(user != null){
                userPersistenceService.saveAsDonar(user);
            }

        } else {
            throw new ClassifyClientErrorException(ErrorType.NO_AMOUNT_SET, HttpStatus.SC_BAD_REQUEST, "You must select an amount you would like to donate.");
        }
    }
}
