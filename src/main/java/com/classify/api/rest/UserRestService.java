package com.classify.api.rest;

import com.classify.api.service.UserService;
import com.classify.api.service.persistence.UserPersistenceService;
import com.classify.config.auth.CurrentUser;
import com.classify.exceptions.ClassifyClientErrorException;
import com.classify.exceptions.ClassifyInternalErrorException;
import com.classify.exceptions.ErrorType;
import com.classify.exceptions.ExceptionLevel;
import com.classify.model.business.objects.Token;
import com.classify.model.business.objects.User;
import com.classify.model.rest.EmptyRestResponse;
import com.classify.model.rest.request.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;

/**
 * Created by Eric Newbury on 6/13/17.
 */
@RestController
@RequestMapping(value = "api/v1/user", consumes = "application/json", produces = "application/json")
@SuppressWarnings("unused")
public class UserRestService {
    private static final Log log = LogFactory.getLog(UserRestService.class);
    private final UserService userService;
    private final UserPersistenceService userPersistenceService;

    @Autowired
    public UserRestService(UserService userService, UserPersistenceService userPersistenceService) {
        this.userService = userService;
        this.userPersistenceService = userPersistenceService;
    }

    @GetMapping("/me")
    public ResponseEntity<User> getCurrentUser(@CurrentUser User user, HttpServletRequest request) throws ClassifyClientErrorException {
        if(user == null){
            throw new ClassifyClientErrorException(ErrorType.NOT_LOGGED_IN, HttpServletResponse.SC_UNAUTHORIZED, "No logged in user");
        }

        return ResponseEntity.ok().body(new User(user));
    }

    @PatchMapping(value="/me", params="type=NAME")
    public ResponseEntity<EmptyRestResponse> updateUserAttributes(@CurrentUser User user,
                                                                  @RequestBody UpdateUserNameRequest request) {
        userPersistenceService.updateName(user.getId(), request.getFirstName(), request.getLastName());
        return ResponseEntity.ok(new EmptyRestResponse());
    }

    @PatchMapping(value = "/me", params = "type=EMAIL")
    public ResponseEntity<EmptyRestResponse> updateUserAttributes(@CurrentUser User user,
                                                                  @RequestBody @Valid UpdateUserEmailRequest request) throws ClassifyClientErrorException {
        userService.changeEmail(user, request.getEmail(), request.getPassword());
        return ResponseEntity.ok(new EmptyRestResponse());
    }

    @PatchMapping(value = "/me", params = "type=PASS")
    public ResponseEntity<EmptyRestResponse> updateUserAttributes(@CurrentUser User user,
                                                                  @RequestBody @Valid UpdateUserPasswordRequest request) {
        userService.updatePassword(user.getId(), request.getPassword());
        return ResponseEntity.ok(new EmptyRestResponse());
    }

    @PostMapping
    public ResponseEntity<EmptyRestResponse> createUser(@Valid @RequestBody AccountCreationRequest request) throws ClassifyClientErrorException {

        if(userPersistenceService.findByEmail(request.getEmail()) != null){
            throw new ClassifyClientErrorException(ErrorType.EMAIL_TAKEN, HttpStatus.SC_BAD_REQUEST, "Email not available");
        }

        User user = userService.createUser(request.getEmail(), request.getPassword());
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(user.getId()).toUri();

        return ResponseEntity.created(location).body(new EmptyRestResponse());
    }

    @PostMapping("/token")
    public ResponseEntity<EmptyRestResponse> newVerificationTokenAndEmail(@RequestBody NewTokenRequest request){
        userService.createAndSendToken(userPersistenceService.findByEmail(request.getEmail()), request.getType());

        return ResponseEntity.ok(new EmptyRestResponse());
    }

    @PatchMapping("/token")
    public ResponseEntity<Object> verifyToken(@RequestBody TokenRequest request) throws ClassifyClientErrorException {
        Token token = userService.verifyToken(request.getToken(), request.getType());
        Object response = new EmptyRestResponse();
        switch (request.getType()){
            case VERIFICATION:
                userPersistenceService.verifyUser(token.getUserId());
                break;
            case PASSWORD:
                userService.updatePassword(token.getUserId(), ((PasswordTokenRequest) request).getPassword());
                break;
            case UPDATE_EMAIL:
                userService.updateEmail(token.getUserId(), token.getMetaInformation());
                response = Collections.singletonMap("email", token.getMetaInformation());
                break;
        }

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/me")
    public ResponseEntity<EmptyRestResponse> deactivateUser(@CurrentUser User user, HttpServletRequest request) throws ClassifyInternalErrorException {
        userService.deactivateUser(user);
        try {
            request.logout();
        } catch (ServletException e) {
            log.error("Could not log out",e);
            throw new ClassifyInternalErrorException(ErrorType.LOGOUT, ExceptionLevel.ERROR, "Error logging out", e);
        }
        return ResponseEntity.accepted().body(new EmptyRestResponse());
    }
}
