package com.classify.api.rest;

import com.classify.api.service.persistence.WatchListPersistenceService;
import com.classify.config.auth.CurrentUser;
import com.classify.model.business.objects.User;
import com.classify.model.business.objects.WatchlistEntry;
import com.classify.model.rest.EmptyRestResponse;
import com.classify.model.rest.request.SectionIdRequest;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

/**
 * Created by Eric Newbury on 8/21/17.
 */
@RestController
@RequestMapping(value="api/v1/watchList", consumes = "application/json", produces = "application/json")
@SuppressWarnings("unused")
public class WatchListRestService {

    private final WatchListPersistenceService watchListPersistenceService;

    @Autowired
    public WatchListRestService(WatchListPersistenceService watchListPersistenceService) {
        this.watchListPersistenceService = watchListPersistenceService;
    }

    @GetMapping
    public ResponseEntity<List<WatchlistEntry>> getWatchListEntries(@CurrentUser User user,
                                                                    @RequestParam("termId") Integer termId){
        List<WatchlistEntry> watchListEntries = watchListPersistenceService.getWatchListEntries(user.getId(), termId);
        return ResponseEntity.ok(watchListEntries);
    }

    @PostMapping
    public ResponseEntity<WatchlistEntry> addWatchListEntry(@CurrentUser User user,
                                                            @RequestParam("termId") Integer termId,
                                                            @RequestBody SectionIdRequest request){
        Preconditions.checkNotNull(request.getSectionId());
        WatchlistEntry watchlistEntry = watchListPersistenceService.addWatchlistEntry(user.getId(), termId, request.getSectionId());

        return ResponseEntity.ok(watchlistEntry);
    }

    @DeleteMapping
    public ResponseEntity<EmptyRestResponse> removeWatchListEntryBySectionId(@CurrentUser User user,
                                                                  @RequestParam("termId") Integer termId,
                                                                  @RequestParam("sectionId") Integer sectionId){
        watchListPersistenceService.removeBySectionId(user.getId(), termId, sectionId);
        return ResponseEntity.ok(new EmptyRestResponse());
    }
}
