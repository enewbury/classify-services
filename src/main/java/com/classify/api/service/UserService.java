package com.classify.api.service;

import com.classify.api.service.persistence.PaymentsPersistenceService;
import com.classify.api.service.persistence.UserPersistenceService;
import com.classify.common.PersistenceService;
import com.classify.crawler.service.CrawlService;
import com.classify.crawler.service.CrawlServiceFactory;
import com.classify.exceptions.ClassifyClientErrorException;
import com.classify.exceptions.ErrorType;
import com.classify.model.business.objects.Token;
import com.classify.model.business.objects.University;
import com.classify.model.business.objects.User;
import com.classify.model.business.objects.enums.TokenType;
import com.google.common.base.Preconditions;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by Eric Newbury on 8/10/17.
 */
@Service
public class UserService {

    private final UserPersistenceService userPersistenceService;
    private final PaymentsPersistenceService paymentsPersistenceService;
    private final CrawlServiceFactory crawlServiceFactory;
    private final PersistenceService persistenceService;
    private final PasswordEncoder passwordEncoder;
    private final TokenService tokenService;

    @Autowired
    public UserService(UserPersistenceService userPersistenceService, CrawlServiceFactory crawlServiceFactory, PersistenceService persistenceService, PasswordEncoder passwordEncoder, TokenService tokenService, PaymentsPersistenceService paymentsPersistenceService) {
        this.userPersistenceService = userPersistenceService;
        this.crawlServiceFactory = crawlServiceFactory;
        this.persistenceService = persistenceService;
        this.passwordEncoder = passwordEncoder;
        this.tokenService = tokenService;
        this.paymentsPersistenceService = paymentsPersistenceService;
    }

    public User createUser(String email, String clearPassword) throws ClassifyClientErrorException {

        University university = persistenceService.getUniversityFromDomain(email.split("@")[1]);

        if (university == null){
            throw new ClassifyClientErrorException(ErrorType.UNSUPORTED_UNIVERSITY, HttpStatus.SC_BAD_REQUEST, "Classify does not support your university yet.");
        } else if (university.getAvailableAccounts() != null && university.getAvailableAccounts() <= 0){
            persistenceService.addToAccountWaitlist(email);
            throw new ClassifyClientErrorException(ErrorType.NO_AVAILABLE_ACCOUNTS, HttpStatus.SC_FORBIDDEN, "Classify is currently in beta and is not taking new users at this time.  We've added you to our waitlist and will notify you when we open Classify up to new users.");
        }

        CrawlService crawlService = crawlServiceFactory.getCrawlServiceForUniversity(university);
        User user = new User();
        user.setUniversityId(university.getId());
        user = crawlService.crawlUserDetails(user, email.split("@")[0]);
        user.setEmail(email);
        user.setPassword(passwordEncoder.encode(clearPassword));

        //check if donor
        if(paymentsPersistenceService.madeDonation(email)){
            user.setDonated(true);
        }

        Integer userId = userPersistenceService.createUser(user);
        user.setId(userId);

        //send verification token
        tokenService.createAndSendToken(userId, TokenType.VERIFICATION, null);

        //update available accounts
        if(university.getAvailableAccounts() != null){
            university.setAvailableAccounts(university.getAvailableAccounts() - 1);
            persistenceService.updateUniversity(university);
        }

        return user;
    }

    public void deactivateUser(User user) {
        userPersistenceService.deactivateUser(user);
    }

    public void createAndSendToken(User user, TokenType type) {
        createAndSendToken(user, type, null);
    }

    private void createAndSendToken(User user, TokenType type, String metaInformation){
        if(user != null) {
            tokenService.createAndSendToken(user.getId(), type, metaInformation);
        }
    }

    public Token verifyToken(String tokenString, TokenType type) throws ClassifyClientErrorException {
        Preconditions.checkNotNull(type);
        Token token = tokenService.verifyToken(tokenString, type);
        if(token == null){
            throw new ClassifyClientErrorException(ErrorType.BAD_TOKEN, HttpStatus.SC_FORBIDDEN, "Invalid token.  Tokens expire after 24 hours");
        }
        return token;
    }

    public void updateEmail(Integer userId, String email){
        userPersistenceService.updateEmail(userId, email);
    }

    public void updatePassword(Integer userId, String clearPassword) {
        String encodedPassword = passwordEncoder.encode(clearPassword);
        //TODO: send notification email
        userPersistenceService.updatePassword(userId, encodedPassword);
    }

    public void changeEmail(User user, String email, String clearPassword) throws ClassifyClientErrorException {
        if(!passwordEncoder.matches(clearPassword, user.getPassword())){
            throw new ClassifyClientErrorException(ErrorType.INCORRECT_PASSWORD, HttpStatus.SC_FORBIDDEN, "The password you entered is incorrect");
        }

        createAndSendToken(user, TokenType.UPDATE_EMAIL, email);
    }
}
