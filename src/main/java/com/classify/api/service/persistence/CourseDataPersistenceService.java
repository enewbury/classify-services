package com.classify.api.service.persistence;

import com.classify.model.business.objects.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jooq.*;
import org.jooq.tables.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;

import static org.jooq.Tables.*;

/**
 * Created by Eric Newbury on 6/15/17.
 */
@Component
public class CourseDataPersistenceService {
    private final static Log log = LogFactory.getLog(CourseDataPersistenceService.class);

    private final DSLContext db;

    @Autowired
    public CourseDataPersistenceService(@SuppressWarnings("SpringJavaAutowiringInspection") DSLContext db) {
        this.db = db;
    }


    public Result<Record> getCourseData(Term term){
        JCourse c = COURSE.as("c");
        JCourseVersion v = COURSE_VERSION.as("v");
        JSection s = SECTION.as("s");
        JTimeSlot t = TIME_SLOT.as("t");
        JInstructor i = INSTRUCTOR.as("i");

        return db.select(c.ID.as(COURSE.getName()+ "_"+COURSE.ID.getName()), c.SUBJECT, c.COURSE_NUMBER, c.UNIVERSITY_ID,
                v.ID.as(COURSE_VERSION.getName()+ "_"+COURSE_VERSION.ID.getName()), v.NAME, v.CREDITS_MIN, v.CREDITS_MAX, v.DESCRIPTION, v.TERM_CODE,
                s.ID.as(SECTION.getName()+ "_"+SECTION.ID.getName()), s.SECTION_NAME, s.UNIVERSITY_SECTION_ID,
                s.TYPE, s.SEATS, s.SEATS_FILLED, s.TERM_ID,
                i.ID.as(INSTRUCTOR.getName()+ "_"+INSTRUCTOR.ID.getName()), i.FIRST_NAME, i.LAST_NAME, i.UNIVERSITY_USERNAME, i.EMAIL, i.SYNCED_TERM, i.RMP_ID, i.RMP_RATING, i.RMP_LATEST_COMMENT,
                t.ID.as(TIME_SLOT.getName()+ "_"+TIME_SLOT.ID.getName()), t.START_TIME, t.END_TIME, t.BUILDING, t.ROOM, t.DAYS, t.NON_WEEKLY
        )
        .from(s)
        .join(c).on(s.COURSE_ID.eq(c.ID))
        .join(v).on(c.ID.eq(v.COURSE_ID))
        .leftOuterJoin(t).on(t.SECTION_ID.eq(s.ID))
        .leftOuterJoin(i).on(i.ID.eq(s.INSTRUCTOR_ID))
        .where(
            s.CANCELLED.eq(false).and(
            s.TERM_ID.eq(term.getId()).and(
            c.UNIVERSITY_ID.eq(term.getUniversityId()).and(
            v.TERM_CODE.eq(term.getTermCode()))))
        ).orderBy(c.SUBJECT, c.COURSE_NUMBER, s.SECTION_NAME, t.START_TIME).fetch();
    }

    public List<Term> getLatestTerms(int limit, Integer universityId) {
        return db.selectFrom(TERM).where(TERM.UNIVERSITY_ID.eq(universityId)).orderBy(TERM.TERM_CODE.desc()).limit(limit).fetchInto(Term.class);
    }

    public List<EnrollmentChange> getLastWeekOfSectionMetrics(Integer sectionId) {
        return db.selectFrom(SECTION_METRICS)
                .where(SECTION_METRICS.SECTION_ID.eq(sectionId))
                .and(SECTION_METRICS.TIME_STAMP.greaterOrEqual(LocalDateTime.now().minusDays(7)))
                .orderBy(SECTION_METRICS.TIME_STAMP.asc())
                .fetchInto(EnrollmentChange.class);
    }

}
