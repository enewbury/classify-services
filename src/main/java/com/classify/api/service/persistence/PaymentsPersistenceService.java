package com.classify.api.service.persistence;

import org.jooq.DSLContext;
import org.jooq.tables.records.JDonationRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static org.jooq.Tables.DONATION;

/**
 * Created by Eric Newbury on 9/14/17.
 */
@Service
public class PaymentsPersistenceService {

    private final DSLContext db;

    @Autowired
    public PaymentsPersistenceService(DSLContext db) {
        this.db = db;
    }

    public void saveDonation(String email, Integer amount){
        JDonationRecord record = db.newRecord(DONATION);
        record.setEmail(email);
        record.setAmount(amount);
        record.setDate(LocalDateTime.now());
        record.insert();
    }

    public boolean madeDonation(String email) {
        return db.selectFrom(DONATION).where(DONATION.EMAIL.eq(email)).fetch().size() > 0;
    }
}
