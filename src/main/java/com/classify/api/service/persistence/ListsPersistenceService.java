package com.classify.api.service.persistence;

import com.classify.model.business.objects.enums.ListEntryType;
import com.classify.model.business.objects.list.entry.GroupListEntry;
import com.classify.model.business.objects.list.entry.ListEntry;
import com.classify.model.business.objects.list.entry.ScheduleListEntry;
import com.classify.model.business.objects.list.entry.SectionListEntry;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jooq.DSLContext;
import org.jooq.Record10;
import org.jooq.Result;
import org.jooq.tables.records.JGroupListEntryRecord;
import org.jooq.tables.records.JListEntryRecord;
import org.jooq.tables.records.JScheduleListEntryRecord;
import org.jooq.tables.records.JSectionListEntryRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.jooq.Tables.*;


/**
 * Created by Eric Newbury on 7/31/17.
 */
@Component
public class ListsPersistenceService {
    private static final Log log = LogFactory.getLog(ListsPersistenceService.class);

    private final DSLContext db;

    @Autowired
    public ListsPersistenceService(@SuppressWarnings("SpringJavaAutowiringInspection") DSLContext db) {
        this.db = db;
    }

    public Result<Record10<Integer, Integer, Integer, Integer, String, Integer, String, String, Integer, Integer>> getListItemsForUserAndTerm(int userId, int termId){

        return db.select(
                LIST_ENTRY.ID, LIST_ENTRY.USER_ID, LIST_ENTRY.TERM_ID, LIST_ENTRY.PARENT_ID, LIST_ENTRY.TYPE, LIST_ENTRY.PRIORITY,
                SCHEDULE_LIST_ENTRY.NAME.as(SCHEDULE_LIST_ENTRY.getName()+SCHEDULE_LIST_ENTRY.NAME.getName()),
                GROUP_LIST_ENTRY.NAME.as(GROUP_LIST_ENTRY.getName()+GROUP_LIST_ENTRY.NAME.getName()), GROUP_LIST_ENTRY.REQUIRED_COUNT,
                SECTION_LIST_ENTRY.SECTION_ID
        )
                .from(LIST_ENTRY)
                .leftJoin(SCHEDULE_LIST_ENTRY).on(SCHEDULE_LIST_ENTRY.LIST_ENTRY_ID.eq(LIST_ENTRY.ID))
                .leftJoin(GROUP_LIST_ENTRY).on(GROUP_LIST_ENTRY.LIST_ENTRY_ID.eq(LIST_ENTRY.ID))
                .leftJoin(SECTION_LIST_ENTRY).on(SECTION_LIST_ENTRY.LIST_ENTRY_ID.eq(LIST_ENTRY.ID))
                .where(LIST_ENTRY.USER_ID.eq(userId)).and(LIST_ENTRY.TERM_ID.eq(termId))
                .fetch();
    }

    public SectionListEntry saveSectionEntry(SectionListEntry sectionListEntry) {
        JListEntryRecord entryRecord = db.newRecord(LIST_ENTRY, sectionListEntry);
        entryRecord.insert();
        JSectionListEntryRecord sectionListEntryRecord = db.newRecord(SECTION_LIST_ENTRY, sectionListEntry);
        sectionListEntryRecord.setListEntryId(entryRecord.getId());
        sectionListEntryRecord.insert();

        sectionListEntry.setId(entryRecord.getId());
        return sectionListEntry;
    }

    public ScheduleListEntry addSchedule(ScheduleListEntry scheduleListEntry) {
        JListEntryRecord entryRecord = db.newRecord(LIST_ENTRY, scheduleListEntry);
        entryRecord.insert();
        JScheduleListEntryRecord scheduleRecord = db.newRecord(SCHEDULE_LIST_ENTRY, scheduleListEntry);
        scheduleRecord.setListEntryId(entryRecord.getId());
        scheduleRecord.insert();

        scheduleListEntry.setId(entryRecord.getId());
        return scheduleListEntry;
    }

    public GroupListEntry saveGroupEntry(GroupListEntry groupListEntry) {
        JListEntryRecord entryRecord = db.newRecord(LIST_ENTRY, groupListEntry);
        entryRecord.insert();
        JGroupListEntryRecord groupListEntryRecord = db.newRecord(GROUP_LIST_ENTRY, groupListEntry);
        groupListEntryRecord.setListEntryId(entryRecord.getId());
        groupListEntryRecord.insert();

        groupListEntry.setId(entryRecord.getId());
        return groupListEntry;
    }

    public void updateName(Integer entityId, String scheduleName) {
        db.update(SCHEDULE_LIST_ENTRY).set(SCHEDULE_LIST_ENTRY.NAME, scheduleName).where(SCHEDULE_LIST_ENTRY.LIST_ENTRY_ID.eq(entityId)).execute();
    }

    public void updateGroupName(Integer entryId, String groupName) {
        db.update(GROUP_LIST_ENTRY).set(GROUP_LIST_ENTRY.NAME, groupName).where(GROUP_LIST_ENTRY.LIST_ENTRY_ID.eq(entryId)).execute();
    }

    public void deleteSchedule(Integer entryId) {
        deleteChildren(entryId);
        db.deleteFrom(SCHEDULE_LIST_ENTRY).where(SCHEDULE_LIST_ENTRY.LIST_ENTRY_ID.eq(entryId)).execute();
        db.deleteFrom(LIST_ENTRY).where(LIST_ENTRY.ID.eq(entryId)).execute();
    }

    private void deleteChildren(Integer entryId){
        Result<JListEntryRecord> children = db.selectFrom(LIST_ENTRY).where(LIST_ENTRY.PARENT_ID.eq(entryId)).fetch();

        for(JListEntryRecord child: children){
            //delete the specific type
            switch (ListEntryType.valueOf(child.get(LIST_ENTRY.TYPE))){
                case SCHEDULE:
                    deleteChildren(child.getId());
                    db.deleteFrom(SCHEDULE_LIST_ENTRY).where(SCHEDULE_LIST_ENTRY.LIST_ENTRY_ID.eq(child.getId())).execute();
                    break;
                case GROUP:
                    deleteChildren(child.getId());
                    db.deleteFrom(GROUP_LIST_ENTRY).where(GROUP_LIST_ENTRY.LIST_ENTRY_ID.eq(child.getId())).execute();
                    break;
                case SECTION:
                    db.deleteFrom(SECTION_LIST_ENTRY).where(SECTION_LIST_ENTRY.LIST_ENTRY_ID.eq(child.getId())).execute();

                    break;
                default:
                    log.warn("Skipped deleting an entry with an unsupported type: "+child.get(LIST_ENTRY.TYPE));
                    continue;
            }

            //delete the generic entry
            db.deleteFrom(LIST_ENTRY).where(LIST_ENTRY.ID.eq(child.getId())).execute();
        }
    }

    public void deleteGroup(Integer entryId) {
        deleteChildren(entryId);
        db.deleteFrom(GROUP_LIST_ENTRY).where(GROUP_LIST_ENTRY.LIST_ENTRY_ID.eq(entryId)).execute();
        db.deleteFrom(LIST_ENTRY).where(LIST_ENTRY.ID.eq(entryId)).execute();
    }

    public void removeSectionFromList(Integer entryId) {
        db.deleteFrom(SECTION_LIST_ENTRY).where(SECTION_LIST_ENTRY.LIST_ENTRY_ID.eq(entryId)).execute();
        db.deleteFrom(LIST_ENTRY).where(LIST_ENTRY.ID.eq(entryId)).execute();
    }

    public ListEntry getEntry(Integer entryId) {
        return db.selectFrom(LIST_ENTRY).where(LIST_ENTRY.ID.eq(entryId)).fetchOneInto(ListEntry.class);
    }

    public void updateParentId(Integer entryId, Integer newParentId) {
        db.update(LIST_ENTRY).set(LIST_ENTRY.PARENT_ID, newParentId).where(LIST_ENTRY.ID.eq(entryId)).execute();
    }
}
