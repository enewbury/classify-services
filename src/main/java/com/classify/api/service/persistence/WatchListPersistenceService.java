package com.classify.api.service.persistence;

import com.classify.model.business.objects.WatchlistEntry;
import org.jooq.DSLContext;
import org.jooq.tables.records.JWatchlistEntryRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.jooq.Tables.WATCHLIST_ENTRY;

/**
 * Created by Eric Newbury on 8/21/17.
 */
@Component
public class WatchListPersistenceService {

    private final DSLContext db;

    @Autowired
    public WatchListPersistenceService(DSLContext db) {
        this.db = db;
    }

    public List<WatchlistEntry> getWatchListEntries(Integer userId, Integer termId) {
        return db.selectFrom(WATCHLIST_ENTRY).where(WATCHLIST_ENTRY.USER_ID.eq(userId)).and(WATCHLIST_ENTRY.TERM_ID.eq(termId)).fetchInto(WatchlistEntry.class);
    }

    public WatchlistEntry addWatchlistEntry(Integer userId, Integer termId, Integer sectionId) {
        JWatchlistEntryRecord record = db.newRecord(WATCHLIST_ENTRY);
        record.setUserId(userId);
        record.setTermId(termId);
        record.setSectionId(sectionId);
        record.insert();
        return record.into(WatchlistEntry.class);
    }

    public void removeBySectionId(Integer userId, Integer termId, Integer sectionId) {
        db.deleteFrom(WATCHLIST_ENTRY)
                .where(WATCHLIST_ENTRY.USER_ID.eq(userId))
                .and(WATCHLIST_ENTRY.TERM_ID.eq(termId))
                .and(WATCHLIST_ENTRY.SECTION_ID.eq(sectionId))
                .execute();
    }
}
