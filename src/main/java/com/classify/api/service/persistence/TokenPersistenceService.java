package com.classify.api.service.persistence;

import com.classify.model.business.objects.Token;
import com.classify.model.business.objects.enums.TokenType;
import org.jooq.DSLContext;
import org.jooq.tables.records.JTokenRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static org.jooq.tables.JToken.TOKEN;

/**
 * Created by Eric Newbury on 8/11/17.
 */
@Component
public class TokenPersistenceService {

    private final DSLContext db;


    @Autowired
    public TokenPersistenceService(@SuppressWarnings("SpringJavaAutowiringInspection") DSLContext db) {
        this.db = db;
    }

    public void saveToken(Token token){
        JTokenRecord tokenRecord = db.newRecord(TOKEN, token);
        tokenRecord.insert();
    }

    public Token lookupValidToken(String token, TokenType type){
        return db.selectFrom(TOKEN).where(TOKEN.TOKEN_.eq(token)).and(TOKEN.TYPE.eq(type.toString())).and(TOKEN.EXPIRES.greaterThan(LocalDateTime.now())).fetchOneInto(Token.class);
    }

    public void deleteTokens(Integer userId, TokenType type) {
        db.deleteFrom(TOKEN).where(TOKEN.USER_ID.eq(userId)).and(TOKEN.TYPE.eq(type.toString())).execute();
    }
}
