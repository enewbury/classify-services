package com.classify.api.service.persistence;

import com.classify.model.business.objects.Token;
import com.classify.model.business.objects.User;
import org.jooq.DSLContext;
import org.jooq.tables.records.JTokenRecord;
import org.jooq.tables.records.JUserRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static org.jooq.tables.JToken.TOKEN;
import static org.jooq.tables.JUser.USER;

/**
 * Created by Eric Newbury on 6/13/17.
 */
@Component
public class UserPersistenceService {

    private final DSLContext db;

    @Autowired
    public UserPersistenceService(@SuppressWarnings("SpringJavaAutowiringInspection") DSLContext db) {
        this.db = db;
    }

    public Integer createUser(User user){
        JUserRecord existingDeleted = db.selectFrom(USER).where(USER.EMAIL.eq(user.getEmail())).and(USER.DELETED.eq(true)).fetchOne();
        JUserRecord record = db.newRecord(USER, user);
        if(existingDeleted == null){
            record.insert();
        } else {
            record.setId(existingDeleted.getId());
            record.update();
        }

        return record.getId();
    }

    public User getUser(Integer userId){
        return db.selectFrom(USER).where(USER.ID.eq(userId)).fetchOneInto(User.class);
    }

    public User findByEmail(String email){
        return db.selectFrom(USER).where(USER.EMAIL.eq(email)).and(USER.DELETED.eq(false)).fetchOneInto(User.class);
    }

    public void deactivateUser(User user) {
        db.update(USER).set(USER.DELETED, true).where(USER.ID.eq(user.getId())).execute();
    }

    public void verifyUser(Integer userId) {
        db.update(USER).set(USER.VERIFIED, true).where(USER.ID.eq(userId)).execute();
    }

    public void updatePassword(Integer userId, String password) {
        db.update(USER).set(USER.PASSWORD, password).where(USER.ID.eq(userId)).execute();
    }

    public void updateEmail(Integer userId, String email) {
        db.update(USER).set(USER.EMAIL, email).where(USER.ID.eq(userId)).execute();
    }

    public void updateName(Integer userId, String firstName, String lastName) {
        db.update(USER).set(USER.FIRST_NAME, firstName).set(USER.LAST_NAME, lastName).where(USER.ID.eq(userId)).execute();
    }

    public void saveAsDonar(User user) {
        db.update(USER).set(USER.DONATED, true).where(USER.ID.eq(user.getId())).execute();
    }
}
