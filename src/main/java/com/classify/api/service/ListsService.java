package com.classify.api.service;

import com.classify.api.service.persistence.ListsPersistenceService;
import com.classify.model.business.objects.enums.ListEntryType;
import com.classify.model.business.objects.list.entry.*;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jooq.Record;
import org.jooq.Record10;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.jooq.Tables.*;

/**
 * Created by Eric Newbury on 7/31/17.
 */
@Component
public class ListsService {
    private static Log log = LogFactory.getLog(ListsService.class);

    private final ListsPersistenceService listsPersistenceService;

    @Autowired
    public ListsService(ListsPersistenceService listsPersistenceService) {
        this.listsPersistenceService = listsPersistenceService;
    }

    public List<ListEntry> loadListsForUserAndTerm(int userId, int termId){

        //load list of all list entries items
        Result<Record10<Integer, Integer, Integer, Integer, String, Integer, String, String, Integer, Integer>> records = listsPersistenceService.getListItemsForUserAndTerm(userId, termId);

        //map children to their parent group
        ListMultimap<Integer, ListEntry> groupsMultimap = ArrayListMultimap.create();
        Map<Integer, ListEntry> entryMap = Maps.newHashMap();

        for (Record record : records) {
            ListEntry entry;
            switch (ListEntryType.valueOf(record.get(LIST_ENTRY.TYPE))){
                case SCHEDULE:
                    entry = loadScheduleEntry(record);
                    break;
                case GROUP:
                    entry = loadGroupEntry(record);
                    break;
                case SECTION:
                    entry = loadSectionEntry(record);
                    break;
                default:
                    log.warn("Skipped an entry with an unsupported type: "+record.get(LIST_ENTRY.TYPE));
                    continue;
            }
            groupsMultimap.put(entry.getParentId(), entry);
            entryMap.put(entry.getId(), entry);
        }

        //assign all the children to each group
        for (Integer groupEntryId : groupsMultimap.keys()) {
            if (groupEntryId != null) {
                ListEntry parent = entryMap.get(groupEntryId);
                List<ListEntry> groupChildren = groupsMultimap.get(groupEntryId);

                if (parent instanceof ContainerListEntry) {
                    ((ContainerListEntry) parent).setEntries(groupChildren);
                } else {
                    log.warn("Entries:" + groupChildren.stream().map(ListEntry::getId).collect(Collectors.toList()) + " has entity " + groupEntryId + " as parent, but it is not a container");
                }
            }
        }

        //return top level entries
        return groupsMultimap.get(null);

    }

    private void mapListEntry(ListEntry entry, Record record){
        entry.setId(record.get(LIST_ENTRY.ID));
        entry.setUserId(record.get(LIST_ENTRY.USER_ID));
        entry.setTermId(record.get(LIST_ENTRY.TERM_ID));
        entry.setParentId(record.get(LIST_ENTRY.PARENT_ID));
        entry.setType(ListEntryType.valueOf(record.get(LIST_ENTRY.TYPE)));
        entry.setPriority(record.get(LIST_ENTRY.PRIORITY));
    }

    private ScheduleListEntry loadScheduleEntry(Record record){
        ScheduleListEntry scheduleListEntry = new ScheduleListEntry();
        mapListEntry(scheduleListEntry, record);
        scheduleListEntry.setName((String) record.get(SCHEDULE_LIST_ENTRY.getName()+SCHEDULE_LIST_ENTRY.NAME.getName()));
        scheduleListEntry.setEntries(new ArrayList<>(0));
        return scheduleListEntry;
    }

    private GroupListEntry loadGroupEntry(Record record){
        GroupListEntry groupScheduleEntry = new GroupListEntry();
        mapListEntry(groupScheduleEntry, record);
        groupScheduleEntry.setName((String) record.get(GROUP_LIST_ENTRY.getName()+GROUP_LIST_ENTRY.NAME.getName()));
        groupScheduleEntry.setRequiredCount(record.get(GROUP_LIST_ENTRY.REQUIRED_COUNT));
        groupScheduleEntry.setEntries(new ArrayList<>(0));
        return groupScheduleEntry;
    }

    private SectionListEntry loadSectionEntry(Record record){
        SectionListEntry sectionScheduleEntry = new SectionListEntry();
        mapListEntry(sectionScheduleEntry, record);
        sectionScheduleEntry.setSectionId(record.get(SECTION_LIST_ENTRY.SECTION_ID));
        return sectionScheduleEntry;
    }

    public SectionListEntry addSectionToList(Integer userId, Integer termId, Integer parentId, Integer sectionId) {
        SectionListEntry sectionScheduleEntry = new SectionListEntry();
        sectionScheduleEntry.setUserId(userId);
        sectionScheduleEntry.setTermId(termId);
        sectionScheduleEntry.setParentId(parentId);
        sectionScheduleEntry.setPriority(5);
        sectionScheduleEntry.setSectionId(sectionId);
        sectionScheduleEntry = listsPersistenceService.saveSectionEntry(sectionScheduleEntry);

        return sectionScheduleEntry;
    }

    public ScheduleListEntry addSchedule(String name, Integer userId, Integer termId) {
        ScheduleListEntry scheduleListEntry = new ScheduleListEntry();
        scheduleListEntry.setTermId(termId);
        scheduleListEntry.setUserId(userId);
        scheduleListEntry.setName(name);
        scheduleListEntry = listsPersistenceService.addSchedule(scheduleListEntry);

        return scheduleListEntry;
    }

    public GroupListEntry newFolder(Integer userId, Integer termId, Integer parentId, String folderName) {
        GroupListEntry groupScheduleEntry = new GroupListEntry();
        groupScheduleEntry.setUserId(userId);
        groupScheduleEntry.setTermId(termId);
        groupScheduleEntry.setParentId(parentId);
        groupScheduleEntry.setName(folderName);
        groupScheduleEntry.setPriority(5);
        groupScheduleEntry = listsPersistenceService.saveGroupEntry(groupScheduleEntry);

        return groupScheduleEntry;
    }

    public void updateName(Integer entryId, String scheduleName){
        listsPersistenceService.updateName(entryId, scheduleName);
    }

    public void updateGroupName(Integer entryId, String groupName) {
        listsPersistenceService.updateGroupName(entryId, groupName);
    }

    public void deleteSchedule(Integer scheduleId) {
        listsPersistenceService.deleteSchedule(scheduleId);
    }
}
