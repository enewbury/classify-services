package com.classify.api.service;

import com.classify.api.service.persistence.CourseDataPersistenceService;
import com.classify.common.PersistenceService;
import com.classify.crawler.helper.JooqDataNormalizer;
import com.classify.model.business.objects.*;
import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Eric Newbury on 6/16/17.
 */
@Component
public class CourseDataService {
    private static final Log log = LogFactory.getLog(CourseDataService.class);

    private final PersistenceService persistenceService;
    private final CourseDataPersistenceService dataPersistenceService;

    @Autowired
    public CourseDataService(CourseDataPersistenceService dataPersistenceService, PersistenceService persistenceService) {
        this.dataPersistenceService = dataPersistenceService;
        this.persistenceService = persistenceService;
    }

    public List<Term> getLast4Terms(Integer universityId){
        return dataPersistenceService.getLatestTerms(4, universityId);
    }

    public List<Course> getCourseDataForTerm(Integer termId) {
        Term term = persistenceService.getTermById(termId);
        JooqDataNormalizer dataNormalizer = new JooqDataNormalizer();
        return dataNormalizer.normalizeResultsToCourse(
                dataPersistenceService.getCourseData(term), term
        );

    }


    public List<EnrollmentChange> getSectionMetrics(Integer sectionId) {
        return dataPersistenceService.getLastWeekOfSectionMetrics(sectionId);
    }
}
