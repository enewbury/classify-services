package com.classify.api.service;

import com.classify.api.service.persistence.TokenPersistenceService;
import com.classify.api.service.persistence.UserPersistenceService;
import com.classify.common.MailService;
import com.classify.config.Queues;
import com.classify.model.business.objects.Token;
import com.classify.model.business.objects.User;
import com.classify.model.business.objects.enums.TokenType;
import com.classify.model.queue.message.GenerateTokenMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Created by Eric Newbury on 8/11/17.
 */
@Service
public class TokenService {

    private final MailService mailService;
    private final TokenPersistenceService tokenPersistenceService;
    private final UserPersistenceService userPersistenceService;
    private final JmsTemplate jmsTemplate;

    @Autowired
    public TokenService(MailService mailService, TokenPersistenceService tokenPersistenceService, UserPersistenceService userPersistenceService, JmsTemplate jmsTemplate) {
        this.mailService = mailService;
        this.tokenPersistenceService = tokenPersistenceService;
        this.userPersistenceService = userPersistenceService;
        this.jmsTemplate = jmsTemplate;
    }


    @JmsListener(destination = Queues.GENERATE_TOKEN, containerFactory = "jmsListenerContainerFactory")
    public void generateTokenListener(GenerateTokenMessage message) throws IOException {
        Token token = new Token(message.getUserId(), message.getType(), LocalDateTime.now().plusDays(1), message.getMetaInformation());
        while(tokenPersistenceService.lookupValidToken(token.getToken(), message.getType()) != null){
            token = new Token(message.getUserId(), token.getType(), LocalDateTime.now().plusDays(1), message.getMetaInformation());
        }

        tokenPersistenceService.saveToken(token);

        User user = userPersistenceService.getUser(message.getUserId());
        if(message.getType().equals(TokenType.VERIFICATION)){
            mailService.sendAccountVerificationEmail(user, token.getToken());
        } else if(message.getType().equals(TokenType.PASSWORD)){
            mailService.sendPasswordResetEmail(user, token.getToken());
        } else if (message.getType().equals(TokenType.UPDATE_EMAIL)){
            mailService.sendNewEmailVerificationEmail(user, token.getToken(), token.getMetaInformation());
        }
    }

    public void createAndSendToken(Integer userId, TokenType type, String metaInformation) {
        jmsTemplate.convertAndSend(Queues.GENERATE_TOKEN, new GenerateTokenMessage(type, userId, metaInformation));
    }

    public Token verifyToken(String tokenString, TokenType type) {
        Token token = tokenPersistenceService.lookupValidToken(tokenString, type);
        if(token != null){
            tokenPersistenceService.deleteTokens(token.getUserId(), token.getType());
        }
        return token;
    }
}
