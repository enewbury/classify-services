package com.classify.api.service;

import com.classify.model.business.objects.User;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static org.jooq.Tables.LIST_ENTRY;
import static org.jooq.Tables.SECTION;
import static org.jooq.Tables.TERM;

/**
 * Created by Eric Newbury on 8/18/17.
 */
@Component("authService")
@SuppressWarnings("unused")
public class AuthService {

    private final DSLContext db;

    @Autowired
    public AuthService(DSLContext db) {
        this.db = db;
    }


    public boolean checkOwnsEntry(User user, int entryId){
        return db.selectFrom(LIST_ENTRY).where(LIST_ENTRY.ID.eq(entryId)).and(LIST_ENTRY.USER_ID.eq(user.getId())).fetchOne() != null;
    }

    public boolean termIsFromCorrectUniversity(User user, int termId){
        Integer termUniversityId = db.select(TERM.UNIVERSITY_ID).from(TERM).where(TERM.ID.eq(termId)).fetchOneInto(Integer.class);
        return (termUniversityId != null && Objects.equals(termUniversityId, user.getUniversityId()));
    }

    public boolean sectionIsFromCorrectUniversity(User user, int sectionId){
        Integer sectionUniversityId = db.select(TERM.UNIVERSITY_ID).from(SECTION).join(TERM).on(TERM.ID.eq(SECTION.TERM_ID)).where(SECTION.ID.eq(sectionId)).fetchOneInto(Integer.class);
        return (sectionUniversityId != null && Objects.equals(sectionUniversityId, user.getUniversityId()));
    }
}
