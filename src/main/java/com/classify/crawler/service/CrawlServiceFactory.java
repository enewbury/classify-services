package com.classify.crawler.service;

import com.classify.crawler.service.uvm.UvmCrawlService;
import com.classify.model.business.objects.University;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrawlServiceFactory {

    private final UvmCrawlService uvmCrawlService;

    @Autowired
    public CrawlServiceFactory(UvmCrawlService uvmCrawlService) {
        this.uvmCrawlService = uvmCrawlService;
    }

    public CrawlService getCrawlServiceForUniversity(University university){
        switch (university.getId()){
            case University.UVM:
                return uvmCrawlService;
            default:
                throw new RuntimeException("Asked factory for non existent crawl service '"+university+"'");
        }
    }
}
