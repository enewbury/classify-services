package com.classify.crawler.service.uvm;

import com.classify.crawler.service.persistence.CrawlPersistenceService;
import com.classify.crawler.helper.TermConverter;
import com.classify.model.business.objects.Term;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.tables.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.jooq.Tables.*;

@Service
public class UvmCrawlPersistenceService extends CrawlPersistenceService {
    private Log log = LogFactory.getLog(UvmCrawlPersistenceService.class);

    @Autowired
    public UvmCrawlPersistenceService(@SuppressWarnings("SpringJavaAutowiringInspection") DSLContext db) {
        super(db);
    }


    public Result<Record> loadDenormalizedCourseData(Term term){

        JCourse c = COURSE.as("c");
        JCourseVersion v = COURSE_VERSION.as("v");
        JSection s = SECTION.as("s");
        JTimeSlot t = TIME_SLOT.as("t");
        JInstructor i = INSTRUCTOR.as("i");

        return db
                .select(
                        c.ID.as(COURSE.getName()+ "_"+COURSE.ID.getName()), c.SUBJECT, c.COURSE_NUMBER, c.UNIVERSITY_ID,
                        v.ID.as(COURSE_VERSION.getName()+ "_"+COURSE_VERSION.ID.getName()), v.NAME, v.CREDITS_MIN, v.CREDITS_MAX, v.TERM_CODE,
                        s.ID.as(SECTION.getName()+ "_"+SECTION.ID.getName()), s.SECTION_NAME, s.UNIVERSITY_SECTION_ID,
                        s.TYPE, s.SEATS, s.SEATS_FILLED, s.TERM_ID,
                        i.ID.as(INSTRUCTOR.getName()+ "_"+INSTRUCTOR.ID.getName()), i.FIRST_NAME, i.LAST_NAME, i.UNIVERSITY_USERNAME, i.EMAIL, i.SYNCED_TERM,
                        t.ID.as(TIME_SLOT.getName()+ "_"+TIME_SLOT.ID.getName()), t.START_TIME, t.END_TIME, t.BUILDING, t.ROOM, t.DAYS, t.NON_WEEKLY
                )
                .from(s)
                .join(c).on(s.COURSE_ID.eq(c.ID))
                .join(v).on(c.ID.eq(v.COURSE_ID))
                .leftOuterJoin(t).on(t.SECTION_ID.eq(s.ID))
                .leftOuterJoin(i).on(i.ID.eq(s.INSTRUCTOR_ID))
                .where(
                        s.TERM_ID.eq(term.getId())).and(
                        s.CANCELLED.eq(false).and(
                        c.UNIVERSITY_ID.eq(term.getUniversityId()).and(
                        v.TERM_CODE.eq(term.getTermCode())))
                )
                .orderBy(c.SUBJECT, c.COURSE_NUMBER, s.SECTION_NAME, t.START_TIME, (t.DAYS.bitOr("")))
                .fetch();
    }


}
