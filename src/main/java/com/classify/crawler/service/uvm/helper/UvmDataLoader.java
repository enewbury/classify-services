package com.classify.crawler.service.uvm.helper;

import com.classify.common.PersistenceService;
import com.classify.crawler.service.CacheService;
import com.classify.crawler.helper.GenericDataNormalizer;
import com.classify.crawler.helper.JooqDataNormalizer;
import com.classify.crawler.helper.JsoupHelper;
import com.classify.crawler.service.uvm.UvmCrawlPersistenceService;
import com.classify.exceptions.BrokenIntegrationException;
import com.classify.model.Semester;
import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.RegistrationTime;
import com.classify.model.business.objects.Term;
import com.classify.model.business.objects.University;
import com.classify.model.business.objects.enums.Standing;
import com.classify.model.uvm.DenormalizedCourseData;
import com.classify.model.uvm.TermCalendarDatesDTO;
import com.classify.model.uvm.TermRegistrationDatesDTO;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.EnumUtils;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.tools.csv.CSVReader;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class UvmDataLoader {
    private Log log = LogFactory.getLog(UvmDataLoader.class);

    private final CacheService cacheService;

    private final UvmCrawlPersistenceService crawlPersistenceService;

    private final PersistenceService persistenceService;

    private UvmDataUrlFactory urlFactory = new UvmDataUrlFactory();

    @Autowired
    public UvmDataLoader(UvmCrawlPersistenceService crawlPersistenceService, CacheService cacheService, PersistenceService persistenceService) {
        this.crawlPersistenceService = crawlPersistenceService;
        this.cacheService = cacheService;
        this.persistenceService = persistenceService;
    }

    //Gets response code for csv and sets lastUpdated.
    public int getCsvResponseCode(Semester semester) throws MalformedURLException, BrokenIntegrationException {
        return getCsvResponseCode(semester, 0);
    }

    private int getCsvResponseCode(Semester semester, int retryCount) throws BrokenIntegrationException, MalformedURLException {

        String csvUrl = urlFactory.getCourseDataUrlForSeason(semester);

        HttpURLConnection connection = null;
        try {
            URL url = new URL(csvUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(1000);
            connection.setRequestMethod("HEAD");
            if (cacheService.getUVMEnrollmentUpdated(semester) != null) {
                connection.setIfModifiedSince(cacheService.getUVMEnrollmentUpdated(semester));
            }
            connection.connect();

            if (connection.getResponseCode() == HttpsURLConnection.HTTP_OK) {
                cacheService.setUVMEnrollmentUpdated(semester, connection.getLastModified());
            }

            return connection.getResponseCode();

        } catch (MalformedURLException e) {
            log.error("Could not parse url for course data csv", e);
            throw e;
        } catch (IOException e) {
            //ran out of retries
            if(retryCount >= 2) {
                log.error("Couldn't connect to csv url after 2 tries");
                throw new BrokenIntegrationException("UVM "+ semester.name()+" CSV", "Couldn't connect to UVM csv after 2 tries. Url may have changed.",e);
            }
            //wait a bit
            try {
                Thread.sleep(500);
            } catch (InterruptedException e1) {
                log.warn("Thread sleep interrupted while waiting to make a retry on to get response code for UVM data");
            }
            //retry
            return getCsvResponseCode(semester, retryCount+1);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public int getYearForSeason(Semester semester) throws IOException, BrokenIntegrationException {
        return getYearForSeason(semester, 0);
    }
    private int getYearForSeason(Semester semester, int retryCount) throws IOException, BrokenIntegrationException {

        String htmlScrapeUrl = urlFactory.getUrlForYearScrape(semester);
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(htmlScrapeUrl).get();
            String[] titleArray = doc.title().trim().split("\\s+");
            String docSeason = titleArray[0];
            String docYear = titleArray[1];


            //if for some reason the url we went to links to a different semester/term
            if(!docSeason.toLowerCase().equals(semester.toString().toLowerCase())){
                log.error("Requested page for '" + semester + "' and got page for '" + docSeason + "'.");
                throw new BrokenIntegrationException("UVM "+ semester.name()+ " HTML", "Requested page for '" + semester + "' and got page for '" + docSeason + "'.");
            }

            return Integer.parseInt(docYear);

        } catch (IOException e) {
            //no more retries
            if(retryCount >=2 ){
                log.error("Could not connect to html doc to get year info",e);
                throw new BrokenIntegrationException("UVM "+ semester.name()+" HTML", "Could not connect to html doc to get year info for UVM data. Check that url is still good.", e);
            }

            //wait before retrying
            try {
                Thread.sleep(500);
            } catch (InterruptedException e1) {
                log.warn("Thread sleep interrupted while waiting to make a retry on getting the year for UVM data");
            }

            //retry
            return getYearForSeason(semester, retryCount+1);
        }
    }

    //get reader for csv
    private Reader getCsvReader(Semester semester) throws IOException {

        String url = urlFactory.getCourseDataUrlForSeason(semester);
        try {
            URL stockURL = new URL(url);
            return new BufferedReader(new InputStreamReader(stockURL.openStream()));

        } catch (MalformedURLException e) {
            log.error("Could not parse url for course data csv", e);
            throw e;
        } catch (IOException e) {
            log.error("Could not make connection to csv url", e);
            throw e;
        }

    }
    public List<Course> loadAndMapCsv(Term term) throws Exception {

        List<DenormalizedCourseData> denormalizedRecords = new ArrayList<>();

        CSVReader reader = new CSVReader(getCsvReader(Semester.valueOf(term.getSemester().toUpperCase())));

        //get map of indexes
        UvmCsvTranslator dataMapper = new UvmCsvTranslator();
        Map<String, Integer> indexes = dataMapper.getIndexMap(reader.readNext());

        //load csv into denormalized data object
        String [] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            //map to comparison record
            DenormalizedCourseData record;
            try {
                record = dataMapper.mapCsvToDenormalizedDataRecord(nextLine, indexes);
            } catch (IllegalAccessException e) {
                log.error("tried to set a restricted (or non existent :) )field during reflection", e);
                throw e;
            }
            denormalizedRecords.add(record);
        }

        //use GenericDataNormalizer
        GenericDataNormalizer dataNormalizer = new GenericDataNormalizer();
        List<Course> normalizedRecords = dataNormalizer.normalizeResultsToCourse(denormalizedRecords, term);

        log.info("Loaded Csv Records");
        return normalizedRecords;
    }

    public List<Course> loadAndMapDbRecords(Term term) throws IllegalAccessException {
        //get denormalized data
        Result<Record> dbRecords = crawlPersistenceService.loadDenormalizedCourseData(term);

        //get a database data denormalizer
        JooqDataNormalizer dataNormalizer = new JooqDataNormalizer();
        List<Course> courseData = dataNormalizer.normalizeResultsToCourse(dbRecords, term);

        log.info("Loaded Database Records");
        return courseData;
    }

    public Map<String, Map<String, String>> loadCourseDescriptionMap(List<String> subjects, Term term) throws BrokenIntegrationException {
        Map<String, Map<String, String>> doubleMap = new HashMap<>(140);

        for(String subject: subjects) {
            try {
                Map<String, String> subjectMap = loadDescriptionsForSubject(subject, term);
                doubleMap.put(subject, subjectMap);
            } catch(HttpStatusException e){
                //status was not ok, maybe they changed url
                log.error("Did not get a 200 response code when trying to get course descriptions for term. Sending email");
                throw new BrokenIntegrationException(term.getTermCode()+" Descriptions", "Did not get a 200 response code when trying to get course descriptions for term");
            } catch (IOException e) {
                //something else went wrong
                log.warn("Could not get descriptions for "+subject+" courses", e);
            }
        }

        return doubleMap;
    }

    private Map<String,String> loadDescriptionsForSubject(String subject, Term term) throws IOException, BrokenIntegrationException {
        log.info("Loading descriptions for "+subject);
        //returns courseNum: description
        Map<String, String> numToDescription = new HashMap<>(40);

        Document doc = Jsoup.connect(urlFactory.getUrlForDescriptions(term.getTermCode(), subject))
                .userAgent(JsoupHelper.getRandomUserAgent()).timeout(20000).get();

        //select all the course nodes
        Elements courseNodes = doc.select(".course");
        for(Element courseNode: courseNodes){
            //get description
            String description = courseNode.select(".description").text();

            //get course num
            String title = courseNode.select(".title").text().trim();
            Matcher courseNumMatcher = Pattern.compile("[A-Z]+ ([0-9]+) - .*").matcher(title);
            if(courseNumMatcher.find() && !description.equals("")){
                String courseNum = courseNumMatcher.group(1);
                numToDescription.put(courseNum, description);
            }
        }
        return numToDescription;
    }

    public List<TermRegistrationDatesDTO> loadTermRegistrationDates() throws IOException, BrokenIntegrationException {
        University university = persistenceService.getUniversityById(University.UVM);

        Document doc = Jsoup.connect(urlFactory.getRegistrationDatesUrls())
                .userAgent(JsoupHelper.getRandomUserAgent()).get();

        List<TermRegistrationDatesDTO> termDatesList = Lists.newArrayList();

        //GET TERM
        Elements tables = doc.select("table.uvmtable");
        if (tables.size() == 0){
            throw new BrokenIntegrationException("UVM Registration Dates Table", "Found no table with class .uvmtable");
        }

        for(Element table: tables) {
            TermRegistrationDatesDTO termDates = new TermRegistrationDatesDTO();
            String termLine = table.select("tr:first-child strong").text();
            String[] termLineSegments = termLine.split(" ");

            //check that line is still formatted correctly.
            if (termLineSegments.length != 4 ||
                    !termLineSegments[1].matches("\\d{4}") ||
                    !EnumUtils.isValidEnum(Semester.class, termLineSegments[0].toUpperCase())) {
                throw new BrokenIntegrationException("UVM Registration Dates Term", "Term line should have 4 words, or 2nd word is not a year or 1st word is not a semester.");
            }

            termDates.setSemester(Semester.valueOf(termLineSegments[0].toUpperCase()));
            termDates.setYear(Integer.parseInt(termLineSegments[1]));
            final List<RegistrationTime> registrationTimes = new ArrayList<>();

            //GET REG DATES
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d, yyyy HH:mm:ss");
            String startTime = university.getRegistrationOpens().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
            String endTime = university.getRegistrationCloses().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
            Integer regYear = (termDates.getSemester().equals(Semester.SPRING)) ? termDates.getYear() - 1 : termDates.getYear();

            List<TableStandingSection> tableStandingSections = Lists.newArrayList(
                    new TableStandingSection(Standing.GRADUATE, "Graduate"),
                    new TableStandingSection(Standing.SENIOR, "Seniors"),
                    new TableStandingSection(Standing.JUNIOR, "Juniors"),
                    new TableStandingSection(Standing.SOPHOMORE, "Sophomores"),
                    new TableStandingSection(Standing.FRESHMAN, "First Year"),
                    new TableStandingSection(Standing.PART_TIME, "Continuing"),
                    new TableStandingSection(Standing.ALL, "All Students")
            );

            for (TableStandingSection section : tableStandingSections) {
                RegistrationTime registrationTime = addRegistrationDate(table, section.matcher, section.standing, regYear, startTime, endTime, formatter);
                if (registrationTime != null) {
                    registrationTimes.add(registrationTime);
                }
            }
            termDates.setRegistrationTimes(registrationTimes);
            termDatesList.add(termDates);
        }

        return termDatesList;
    }

    private RegistrationTime addRegistrationDate(final Element table, final String matcher,
                                                 final Standing standing, final Integer regYear, String startTime,
                                                 final String endTime, final DateTimeFormatter formatter) throws BrokenIntegrationException {

        for(Element tr: table.select("tr")){
            if (tr.select("td:first-child").text().contains(matcher)) {
                String dateString = tr.select("td:last-child").text() + ", " + regYear + " ";
                if (dateString.matches(".*(?:AM|PM).*")) {
                    String[] parts = dateString.split("\\(");
                    Matcher match = Pattern.compile("(\\d{1,2}:\\d{2}(?:AM|PM))").matcher(parts[1]);
                    if (match.find()) {
                        LocalTime startTimeObj = LocalTime.parse(match.group(1), DateTimeFormatter.ofPattern("h:mma"));
                        startTime = startTimeObj.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
                    }
                    dateString = parts[0].trim() + ", " + regYear + " ";
                }
                LocalDateTime endDateTime = (Standing.ALL.equals(standing)) ? null : LocalDateTime.parse(dateString + endTime, formatter);
                return new RegistrationTime(standing, LocalDateTime.parse(dateString + startTime, formatter), endDateTime);
            }
        }
        if(Standing.ALL.equals(standing)){
            throw new BrokenIntegrationException("UVM Registration Dates " + standing, "Rows do not contain a td that includes \"" + matcher + "\"");
        }
        return null;
    }

    public TermCalendarDatesDTO loadTermCalendarDates(Term term) throws IOException, BrokenIntegrationException {
        TermCalendarDatesDTO termCalendarDates = new TermCalendarDatesDTO();

        Document doc = Jsoup.connect(urlFactory.getTermCalendarDetails(term))
                .userAgent(JsoupHelper.getRandomUserAgent()).get();

        //get correct table section
        Elements tables = doc.select("table.uvmtable");
        Element semesterTable = null;
        for(Element table: tables){
            if(table.select("th").text().toLowerCase().contains(term.getSemester().toLowerCase() + " " + term.getYear())){
                semesterTable = table;
                break;
            }
        }

        //no table for that semester found
        if(semesterTable == null){
            throw new BrokenIntegrationException("UVM Calendar Dates "+term.getYear(), "Found no table section for semester"+term.getSemester());
        }

        for(Element row: semesterTable.select("tr")){
            if(row.select("td:first-child").text().contains("First Day of Classes")){
                LocalDate firstDay = LocalDate.parse(row.select("td:nth-child(2)").text() +", "+term.getYear(), DateTimeFormatter.ofPattern("MMM d, yyyy"));
                termCalendarDates.setFirstDay(firstDay);

            } else if (row.select("td:first-child").text().contains("Last Day of Classes")){
                LocalDate firstDay = LocalDate.parse(row.select("td:nth-child(2)").text() +", "+term.getYear(), DateTimeFormatter.ofPattern("MMM d, yyyy"));
                termCalendarDates.setLastDay(firstDay);

            } else if (row.select("td:first-child").text().contains("Add/Drop")){
                LocalDate firstDay = LocalDate.parse(row.select("td:nth-child(2)").text() +", "+term.getYear(), DateTimeFormatter.ofPattern("MMM d, yyyy"));
                termCalendarDates.setAddDrop(firstDay);

            } else if (row.select("td:first-child").text().contains("Last Day to Withdraw")){
                LocalDate firstDay = LocalDate.parse(row.select("td:nth-child(2)").text() +", "+term.getYear(), DateTimeFormatter.ofPattern("MMM d, yyyy"));
                termCalendarDates.setWithdraw(firstDay);

            }
        }

        if(termCalendarDates.getFirstDay() == null){
            throw new BrokenIntegrationException("UVM Calendar Dates "+term.getYear(), "Couldn't find the first day of classes in the list");
        }

        return termCalendarDates;
    }

    public JsonNode loadUserDirectoryDetails(String universityUsername) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
       return objectMapper.readTree(new URL(urlFactory.getDirectoryResultsUrl(universityUsername)));
    }

    private static class TableStandingSection {
        Standing standing;
        String matcher;

        TableStandingSection(Standing standing, String matcher) {
            this.standing = standing;
            this.matcher = matcher;
        }
    }
}
