package com.classify.crawler.service.uvm.helper;

import com.classify.crawler.helper.UrlFactory;
import com.classify.model.Semester;
import com.classify.model.business.objects.Term;

public class UvmDataUrlFactory extends UrlFactory{

    public String getCourseDataUrlForSeason(Semester semester){
        if (semester == null){ throw new RuntimeException("tried to get url for null semester");}

        String seasonString = semester.toString().toLowerCase();

        return "https://giraffe.uvm.edu/~rgweb/batch/curr_enroll_" + seasonString + ".txt";
    }

    public String getUrlForYearScrape(Semester semester){

        if (semester == null){ throw new RuntimeException("tried to get url for null semester");}

        String seasonString = semester.toString().toLowerCase();
        return "https://giraffe.uvm.edu/~rgweb/batch/curr_enroll_" + seasonString + ".html";
    }

    public String getUrlForDescriptions(String termCode, String subject){
        return "https://www.uvm.edu/academics/courses/?term="+termCode+"&subject="+subject;
    }

    public String getRegistrationDatesUrls(){
        return "http://www.uvm.edu/~rgweb/registration/r_registrationmain.html";
    }

    public String getTermCalendarDetails(Term term){
        if (term.getSemester().toUpperCase().equals(Semester.FALL.toString())){
            String nextYear = String.valueOf(term.getYear()+1).substring(2);
            String years = term.getYear().toString().substring(2)+nextYear;
            return "http://www.uvm.edu/~rgweb/importantdates/i_ac"+years+".html";
        } else {
            String years = String.valueOf(term.getYear()-1).substring(2) + String.valueOf(term.getYear().intValue()).substring(2);
            return "http://www.uvm.edu/~rgweb/importantdates/i_ac"+years+".html";
        }
    }

    public String getExamTimesUrl(Semester semester){
        if (semester == null){ throw new RuntimeException("tried to get url for null semester");}

        String seasonString = semester.toString().toLowerCase();
        return "https://giraffe.uvm.edu/~rgweb/batch/final_exams_"+ seasonString +".html";
    }

    public String getDirectoryResultsUrl(String netId){
        return "https://www.uvm.edu/directory/api/query_results.php?name=" + netId;
    }

}
