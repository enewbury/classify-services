package com.classify.crawler.service.uvm;

import com.classify.common.PersistenceService;
import com.classify.dispatcher.RegistrationStatus;
import com.classify.model.business.objects.RegistrationWaitlistEntry;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * Created by Eric Newbury on 6/8/17.
 */
@Component
public class UVMRegistrationEngine {
    private Log log = LogFactory.getLog(UVMRegistrationEngine.class);

    private final PersistenceService persistenceService;
    private final JmsTemplate jmsTemplate;

    @Autowired
    public UVMRegistrationEngine(PersistenceService persistenceService, JmsTemplate jmsTemplate) {
        this.persistenceService = persistenceService;
        this.jmsTemplate = jmsTemplate;
    }

//    @JmsListener(destination = "UVMEnroll")
//    public void register(Integer waitlistEntryId){
//        RegistrationWaitlistEntry entry = persistenceService.getWaitlistEntry(waitlistEntryId);
//        log.info("Registering user: " + entry.getUserId() + " for section: " + entry.getSectionId());
//        entry.setAttemptTime(LocalDateTime.now());
//        entry.setStatus(RegistrationStatus.REGISTERED);
//        persistenceService.updateWaitlist(entry);
//        jmsTemplate.convertAndSend("RegistrationAttempt", entry.getId());
//    }
}
