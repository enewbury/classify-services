package com.classify.crawler.service.uvm;

import com.classify.common.PersistenceService;
import com.classify.crawler.service.CrawlService;
import com.classify.crawler.service.uvm.helper.UvmDataLoader;
import com.classify.dispatcher.DispatchService;
import com.classify.exceptions.BrokenIntegrationException;
import com.classify.model.EnrollmentChanges;
import com.classify.model.SectionUpdates;
import com.classify.model.Semester;
import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.Term;
import com.classify.model.business.objects.University;
import com.classify.model.business.objects.User;
import com.classify.model.business.objects.enums.Standing;
import com.classify.model.uvm.*;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;

@Service
public class UvmCrawlService implements CrawlService{
    private Log log = LogFactory.getLog(UvmCrawlService.class);

    private University university;
    private final UvmDataWalker dataWalker;
    private final PersistenceService persistenceService;
    private final UvmCrawlPersistenceService crawlPersistenceService;
    private final UvmDataLoader dataLoader;
    private final DispatchService dispatchService;

    @Autowired
    public UvmCrawlService(UvmCrawlPersistenceService crawlPersistenceService, UvmDataWalker dataWalker, UvmDataLoader dataLoader, DispatchService dispatchService, PersistenceService persistenceService) {
        this.crawlPersistenceService = crawlPersistenceService;
        this.dataWalker = dataWalker;
        this.dataLoader = dataLoader;
        this.dispatchService = dispatchService;
        this.persistenceService = persistenceService;

    }


    @Override
    public void getHighPriorityData(University university) {
        for(Semester semester : university.getAvailableSemestersList()) {
            crawlSeasons(semester, university);
        }
    }

    private void crawlSeasons(Semester semester, University university){

        this.university = university;

        try {
            //check CSV for changes
            boolean needsUpdate = remoteDataUpdated(semester);

            if (needsUpdate) {
                //update the system
                EnrollmentChanges changeSet = loadSemesterAndData(semester);

                //queue notifications
                dispatchService.handleSectionOpenings(new SectionUpdates(changeSet));
            }

        } catch(BrokenIntegrationException e) {
            //generate cause
            dispatchService.handleBrokenIntegration(e);

        } catch (Exception e) {
            log.error("There was an error getting "+ semester +" data for UVM",e);
        }

    }

    private boolean remoteDataUpdated(Semester semester) throws BrokenIntegrationException, MalformedURLException {
        return remoteDataUpdated(semester, 0);
    }
    private boolean remoteDataUpdated(Semester semester, int retryCount) throws BrokenIntegrationException, MalformedURLException {

            //makes HEAD request to test for updates.  (updates local lastUpdated variables if so)
            int csvResponseCode = dataLoader.getCsvResponseCode(semester);

            //data was updated if status 200
            if (csvResponseCode == 200){
                log.info("UVM "+ semester +" primary data has refreshed");
                return true;
            }
            //no updated if status 304 not modified
            else if (csvResponseCode == 304){
                return false;
            }
            //other status codes indicate error.
            else {
                //ran out of retries
                if(retryCount >= 2) {
                    log.error("Csv page returned a failing status code of: " + csvResponseCode+" after 2 tries");
                    throw new BrokenIntegrationException("UVM"+ semester.name()+" CSV", "Csv page returned a failing status code of: "+csvResponseCode+" when trying to get UVM data.");
                }

                //wait a bit
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    log.warn("Thread sleep interrupted while waiting to make a retry on to get response code for UVM data");
                }

                //retry
                return remoteDataUpdated(semester, retryCount+1);

            }
    }

    private EnrollmentChanges loadSemesterAndData(Semester semester) throws Exception{

        //get year for this url
        int year = dataLoader.getYearForSeason(semester);
        Term term = persistenceService.getTerm(university.getId(), semester, year);

        //No data for this term yet. Load and crawl for term data
        if (term == null){
            term = persistenceService.insertTerm(university.getId(), semester, year);
            updateCalendarDates(Collections.singletonList(term));
        }

        term.setUniversity(university);

        //begin updating term
        log.info("+++++ Updating course info for " + semester + " " + year);
        EnrollmentChanges enrollmentChanges = loadAndUpdateData(term);
        log.info("+++++ Finished updating course info for  " + semester + " " + year);
        return enrollmentChanges;
    }

    private EnrollmentChanges loadAndUpdateData(Term term) throws Exception {
        //get csvData
        List<Course> csvRecords = dataLoader.loadAndMapCsv(term);

        //get db data
        List<Course> dbRecords = dataLoader.loadAndMapDbRecords(term);

        //walk (add update delete data, return updates)
        return dataWalker.walk(csvRecords, dbRecords, university, term);
    }


    @Override
    public void getDailyData(University university) {

        this.university = university;

        //get last three terms for university
        List<Term> terms = crawlPersistenceService.getLastThreeSemestersForUniversity(university);

        //update the course descriptions for those terms
        updateCourseDescriptions(terms);

        //update the registration dates for available term
        updateRegistrationDates();

        //update academic calendar for latest terms
        updateCalendarDates(terms);

    }

    private void updateCourseDescriptions(List<Term> terms){
        //get list of all subjects for school
        List<String> subjects = crawlPersistenceService.getAllSubjectsForSchool(university);

        for(Term term: terms){

            try {
                //load map subject:{courseNum: description}
                Map<String, Map<String, String>> descriptions = dataLoader.loadCourseDescriptionMap(subjects, term);

                //get all courses with versions from this term
                List<Course> courses = crawlPersistenceService.getCoursesWithVersion(term);

                List<Course> updatedCourses = new ArrayList<>();
                for (Course course : courses) {
                    //lookup description in map
                    String remoteDescription = getDescriptionFromMap(course, descriptions);

                    if (!Objects.equals(remoteDescription, course.getDescription())) {
                        course.setDescription(remoteDescription);
                        updatedCourses.add(course);
                    }
                }

                crawlPersistenceService.updateCourseDescriptions(updatedCourses);
            }
            catch (BrokenIntegrationException e){
                log.error("Problem with course description integration", e);
                dispatchService.handleBrokenIntegration(e);
            }
        }
    }

    private void updateRegistrationDates(){
        try{
            //get and parse the page
            List<TermRegistrationDatesDTO> termDatesList = dataLoader.loadTermRegistrationDates();

            for(TermRegistrationDatesDTO termDates: termDatesList) {
                log.info("Updating registration dates for term " + termDates.getSemester().toString() + " " + termDates.getYear());

                //load the semester it references
                Term term = persistenceService.getTerm(University.UVM, termDates.getSemester(), termDates.getYear());
                if (term == null) {
                    //doesn't exist yet, create it.
                    term = persistenceService.insertTerm(University.UVM, termDates.getSemester(), termDates.getYear());
                }

                //update the dates
                Term finalTerm = term;
                termDates.getRegistrationTimes().forEach(registrationTime -> registrationTime.setTermId(finalTerm.getId()));

                crawlPersistenceService.updateTermRegistrationDates(termDates.getRegistrationTimes());
            }

        } catch(BrokenIntegrationException e) {
            log.error("Problem with registration date integration", e);
            dispatchService.handleBrokenIntegration(e);
        } catch (Exception e) {
            log.error("There was an error getting registration date data for UVM",e);
        }

    }

    private void updateCalendarDates(List<Term> terms){
        for(Term term: terms) {
            try {
                log.info("Updating important dates for "+ term.getSemester() + " " + term.getYear());
                TermCalendarDatesDTO termCalendarDates = dataLoader.loadTermCalendarDates(term);

                //update the term
                term.setFirstDayOfClasses(termCalendarDates.getFirstDay());
                term.setLastDayOfClasses(termCalendarDates.getLastDay());
                term.setWithdrawDeadline(termCalendarDates.getWithdraw());

                //update them with dates
                crawlPersistenceService.updateTermCalendarDates(term);
                if(termCalendarDates.getAddDrop() != null) {
                    crawlPersistenceService.updateOpenRegistrationEndDate(term, termCalendarDates.getAddDrop());
                }

            } catch(BrokenIntegrationException e) {
                log.error("Problem with calendar date integration", e);
                dispatchService.handleBrokenIntegration(e);
            } catch (Exception e) {
                log.error("There was an error getting calendar dates data for UVM",e);
            }
        }
    }

    private String getDescriptionFromMap(Course course, Map<String, Map<String, String>> descriptions) {
        String description = null;

        Map<String, String> subjectEntries = descriptions.get(course.getSubject().toUpperCase());
        if(subjectEntries!=null){
            description = subjectEntries.get(course.getCourseNumber());
        }

        return description;
    }

    @Override
    public User crawlUserDetails(User user, String queryName){
        try {
            queryName = queryName.replace(".", "+");
            JsonNode json = dataLoader.loadUserDirectoryDetails(queryName);
            JsonNode userData = json.get("data");

            if (userData.isArray() && userData.size() > 0 && userData.get(0).hasNonNull("givenname")) {
                userData = userData.get(0);

                user.setUniversityUsername(userData.get("uid").get("0").asText());
                user.setFirstName(userData.get("givenname").get("0").asText());
                user.setLastName(userData.get("uvmedusurname").get("0").asText());
                user.setEmail(userData.get("edupersonprincipalname").get("0").asText());
                String standing = userData.get("ou").get("0").asText();
                standing = standing.toUpperCase();
                if ("FIRST YEAR".equals(standing)) standing = "FRESHMAN";
                try {
                    user.setCurrentStanding(Standing.valueOf(standing));
                } catch (IllegalArgumentException ignored){}

            }
            return user;

        } catch(IOException e){
            log.error("Error crawling for user details.", e);
            return user;
        }

    }
}
