package com.classify.crawler.service.uvm.helper;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class UvmBuildingNameFactory {
    private static final Map<String, String> buildingNames;
    static
    {
        buildingNames = new HashMap<String, String>(100);
        buildingNames.put("ML SCI","Marsh Life Science");
        buildingNames.put("GIVN E","Given East");
        buildingNames.put("L/L CM","Living & Learning Commons");
        buildingNames.put("LAFAYE","Lafayette");
        buildingNames.put("WILLMS","Williams");
        buildingNames.put("FLEMIN","Fleming");
        buildingNames.put("JEFFRD","Jeffords");
        buildingNames.put("L/L-A","Living & Learning - A");
        buildingNames.put("WATERM","Waterman");
        buildingNames.put("L/L-B","Living & Learning - B");
        buildingNames.put("STAFFO","Stafford");
        buildingNames.put("MARSH","Marsh Life Science");
        buildingNames.put("PERKIN","Perkins");
        buildingNames.put("GIVN C","Givin C");
        buildingNames.put("RT THR","Royal Tyler Theatre");
        buildingNames.put("ONCMP","On Campus");
        buildingNames.put("PATGYM","Patrick Gym");
        buildingNames.put("OFFCMP","Off Campus");
        buildingNames.put("OLDMIL","Old Mill");
        buildingNames.put("GIVN","Givin");
        buildingNames.put("SOUTHW","Southwick");;
        buildingNames.put("BLLNGS","Billings");
        buildingNames.put("DELEHA","Delehanty Hall");
        buildingNames.put("FAHC","Fletcher Allen Health Care");
        buildingNames.put("GIVN B","Given B");
        buildingNames.put("GUTRSN","Gutterson");
        buildingNames.put("L/L-D","Living & Learning - D");
    }
    public static String getFullBuildingName(String shortName){
        if(shortName == null){
            return null;
        }

        String fullName = buildingNames.get(shortName);

        if(fullName == null){
            return StringUtils.capitalize(shortName.toLowerCase());
        }
        else{
            return fullName;
        }
    }
}
