package com.classify.crawler.service.uvm.helper;

import com.classify.model.WeekDay;
import com.classify.model.business.objects.enums.SectionType;
import com.classify.model.uvm.DenormalizedCourseData;
import com.google.common.primitives.Ints;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import java.lang.reflect.Field;
import java.sql.Time;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

public class UvmCsvTranslator {
    private Log log = LogFactory.getLog(UvmCsvTranslator.class);
    public  Map<String, Integer> getIndexMap(String[] header) {
        Map<String, Integer> indexes = new HashMap<>();

        for (int i = 0; i < header.length; i++) {
            switch (header[i].trim()) {
                case "Subj":
                    indexes.put("subject", i);
                    continue;
                case "#":
                    indexes.put("courseNumber", i);
                    continue;
                case "Title":
                    indexes.put("courseName", i);
                    continue;
                case "Comp Numb":
                    indexes.put("universitySectionId", i);
                    continue;
                case "Sec":
                    indexes.put("sectionName", i);
                    continue;
                case "Lec Lab":
                    indexes.put("sectionType", i);
                    continue;
                case "Max Enrollment":
                    indexes.put("seats", i);
                    continue;
                case "Current Enrollment":
                    indexes.put("seatsFilled", i);
                    continue;
                case "Start Time":
                    indexes.put("startTime", i);
                    continue;
                case "End Time":
                    indexes.put("endTime", i);
                    continue;
                case "Days":
                    indexes.put("days", i);
                    continue;
                case "Credits":
                    indexes.put("credits", i);
                    continue;
                case "Bldg":
                    indexes.put("building", i);
                    continue;
                case "Room":
                    indexes.put("room", i);
                    continue;
                case "Instructor":
                    indexes.put("instructor", i);
                    continue;
                case "NetId":
                    indexes.put("universityUsername", i);
                    continue;
                case "Email":
                    indexes.put("email", i);
            }
        }
        return indexes;
    }

    public DenormalizedCourseData mapCsvToDenormalizedDataRecord(String[] row, Map<String, Integer> i) throws IllegalAccessException{
        DenormalizedCourseData denormalizedCourseData = new DenormalizedCourseData();

        //get array of all fields in data object
        Field[] fields = denormalizedCourseData.getClass().getFields();
        for (Field field: fields){
            String fieldName = field.getName();
            //loop through fields and load in /parse data from csv in.
            switch(fieldName){
                case "creditsMin":
                    Integer j = i.get("credits");
                    //split on "to" get first half
                    if (j == null) continue;
                    String creditsMin = row[j].split("\\s*(to|or)\\s*")[0];
                    field.set(denormalizedCourseData,Double.parseDouble(creditsMin));
                    break;
                case "creditsMax":
                    Integer k = i.get("credits");
                    //split on "to" get first half
                    if (k == null) continue;
                    String[] creditsArray = row[k].split("\\s*(to|or)\\s*");
                    if(creditsArray.length > 1) {
                        field.set(denormalizedCourseData, Double.parseDouble(creditsArray[1]));
                    }
                    break;
                case "firstName":
                    Integer l = i.get("instructor");
                    //check for no instructor
                    //parse name after comma, remove Single digit with period
                    String[] nameParts = row[l].split(",\\s+");

                    if(nameParts[0].matches("Staff.*"))continue;

                    if(nameParts.length > 1) {
                        String[] firstNameParts = nameParts[1].split("\\s+");
                        int firstNameIndex = 0;
                        if(firstNameParts[0].matches("[A-Z]?\\.")){firstNameIndex = 1;}
                        field.set(denormalizedCourseData, firstNameParts[firstNameIndex]);
                    }
                    break;
                case "lastName":
                    //parse name after comma, remove Single digit with period
                    String[] nameParts2 = row[i.get("instructor")].replaceAll("\\s[a-zA-Z]\\.?\\s*$", "").split(",\\s+");
                    if(nameParts2[0].matches("Staff.*"))continue;

                    if(nameParts2.length > 1) {
                        field.set(denormalizedCourseData, nameParts2[0]);
                    }
                    break;

                case "days":
                    //remove whitespace and split
                    String[] dayLetters = row[i.get(fieldName)].replaceAll("\\s+", "").split("");
                    if(dayLetters[0].equals("")) continue;

                    if (dayLetters.length > 0){
                        int[] dayNums = new int[dayLetters.length];
                        //convert to number
                        for(int n=0;n<dayLetters.length;n++){
                            dayNums[n] = WeekDay.getEnum(dayLetters[n]).getId();
                        }

                        String days = Ints.join(",", dayNums);
                        field.set(denormalizedCourseData, days);
                    }
                    break;
                case "startTime":
                    //blank or TBA to null
                    String startTime = row[i.get(fieldName)].trim();
                    if (startTime.equals("") || startTime.equals("TBA")) continue;

                    field.set(denormalizedCourseData, LocalTime.parse(startTime+":00"));
                    break;
                case "endTime":
                    //blank or TBA to null
                    String endTime = row[i.get(fieldName)].trim();
                    if (endTime.equals("") || endTime.equals("TBA")) continue;

                    field.set(denormalizedCourseData, LocalTime.parse(endTime+":00"));//blank or TBA to null
                    break;
                case "sectionType":
                    //map to enum
                    String sectionType = row[i.get(fieldName)].trim();
                    if (sectionType.equals("")) continue;
                    SectionType secType;
                    if(sectionType.equals("LEC")){
                        secType=SectionType.LECTURE;
                    } else if (sectionType.equals("LAB")){
                        secType = SectionType.LAB;
                    } else if (sectionType.equals("ONL")){
                        secType = SectionType.ONLINE;
                    } else {
                        secType = SectionType.OTHER;
                    }
                    field.set(denormalizedCourseData, secType);
                    break;
                case "building":
                    if(i.get(fieldName) !=null && !row[i.get(fieldName)].trim().equals("")) {
                        String buildingName = UvmBuildingNameFactory.getFullBuildingName(row[i.get(fieldName)].trim());
                        field.set(denormalizedCourseData, buildingName);
                    }
                    break;
                case "universityUsername":
                    if(row[i.get("universityUsername")].equals("staff")) continue;
                    //no break so that it can pass on to default
                default:
                    //check if this field is in our map (i.e. ids will no be)
                    if ( i.get(fieldName) == null || row[i.get(fieldName)].trim().equals("")){ continue; }

                    //load directly in, parsing to correct type
                    Class type = field.getType();
                    if (type.equals(Integer.class)){
                        //parse to Int
                        Integer value = Integer.parseInt( row[i.get(fieldName)].trim() );
                        //set field
                        field.set(denormalizedCourseData,value);
                    } else {
                        String value = row[i.get(fieldName)].trim();
                        //set field
                        field.set(denormalizedCourseData,value);
                    }

            }
        }
        return denormalizedCourseData;
    }



}
