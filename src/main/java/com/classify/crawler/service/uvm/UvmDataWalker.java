package com.classify.crawler.service.uvm;

import com.classify.crawler.helper.UpdateChecker;
import com.classify.crawler.helper.WalkActionConsultant;
import com.classify.model.business.objects.*;
import com.classify.model.uvm.CheckStatus;
import com.classify.model.EnrollmentChanges;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.classify.model.uvm.CheckStatus.*;

@Component
public class UvmDataWalker {
    private Log log = LogFactory.getLog(UvmDataWalker.class);

    private UvmCrawlPersistenceService persistenceService;
    private UpdateChecker updateChecker;
    private WalkActionConsultant consultant;

    @Autowired
    public UvmDataWalker(UvmCrawlPersistenceService persistenceService,
                         UpdateChecker updateChecker,
                         WalkActionConsultant consultant){
        this.persistenceService = persistenceService;
        this.updateChecker = updateChecker;
        this.consultant = consultant;
    }

    public EnrollmentChanges walk(List<Course> remoteCourses,
                                        List<Course> dbCourses,
                                        University university,
                                        Term term) {

        //create new batching object
        EnrollmentChanges batch = new EnrollmentChanges(term);

        //mark changes to database
        updateCourses(remoteCourses, dbCourses, batch, term.getTermCode());

        //make all the changes in the batch object
        persistChanges(university, term, batch);

        //return summary of changes
        return batch;
    }


    private EnrollmentChanges updateCourses(List<Course> remoteList, List<Course> dbList, EnrollmentChanges batch, String termCode){
        Iterator<Course> remoteCourses = remoteList.iterator();
        Iterator<Course> dbCourses = dbList.iterator();

        Course curRemoteCourse = (remoteCourses.hasNext()) ? remoteCourses.next() : null;
        Course curDbCourse = (dbCourses.hasNext()) ? dbCourses.next() : null;

        while(curRemoteCourse!=null || curDbCourse!=null) {
            //evaluate differences, determine action to take
            CheckStatus status = consultant.determineCourseAction(curRemoteCourse, curDbCourse);

            if (status.equals(ADD)) {
                addCourse(curRemoteCourse, batch, termCode);
                curRemoteCourse = (remoteCourses.hasNext()) ? remoteCourses.next() : null;
            } else if (status.equals(UPDATE)) {
                updateCourse(curRemoteCourse, curDbCourse, batch, termCode);
                curRemoteCourse = (remoteCourses.hasNext()) ? remoteCourses.next() : null;
                curDbCourse = (dbCourses.hasNext()) ? dbCourses.next() : null;
            } else if (status.equals(DELETE)) {
                deleteCourse(curDbCourse, batch);
                curDbCourse = (dbCourses.hasNext()) ? dbCourses.next() : null;
            } else { log.error("Check status for courses returned an unknown value of: " + status); }

        }

        return batch;
    }

    private void updateSections(List<Section> remoteList, List<Section> dbList, EnrollmentChanges batch, String termCode){
        Iterator<Section> remoteSections = remoteList.iterator();
        Iterator<Section> dbSections = dbList.iterator();

        Section curRemoteSection = (remoteSections.hasNext()) ? remoteSections.next() : null;
        Section curDbSection = (dbSections.hasNext()) ? dbSections.next() : null;


        while(curRemoteSection!=null || curDbSection!=null){
            CheckStatus status = consultant.determineSectionAction(curRemoteSection, curDbSection);

            if (status.equals(ADD)) {
                addSection(curRemoteSection, batch, termCode);
                curRemoteSection = (remoteSections.hasNext()) ? remoteSections.next() : null;
            } else if (status.equals(UPDATE)) {
                updateSection(curRemoteSection, curDbSection, batch, termCode);
                curRemoteSection = (remoteSections.hasNext()) ? remoteSections.next() : null;
                curDbSection = (dbSections.hasNext()) ? dbSections.next() : null;
            } else if (status.equals(DELETE)) {
                deleteSection(curDbSection, batch);
                curDbSection = (dbSections.hasNext()) ? dbSections.next() : null;
            } else { log.error("Check status for sections returned an unknown value of: " + status); }

        }
    }



    private void updateTimes(List<TimeSlot> remoteList, List<TimeSlot> dbList, EnrollmentChanges batch, String termCode){
        Iterator<TimeSlot> remoteTimes = remoteList.iterator();
        Iterator<TimeSlot> dbTimes = dbList.iterator();

        TimeSlot curRemoteTime = (remoteTimes.hasNext()) ? remoteTimes.next() : null;
        TimeSlot curDbTime = (dbTimes.hasNext()) ? dbTimes.next() : null;

        while (curRemoteTime!=null || curDbTime!=null) {
            CheckStatus status = consultant.determineTimeAction(curRemoteTime, curDbTime);

            if (status.equals(ADD)) {
                addTime(curRemoteTime, batch, termCode);
                curRemoteTime = (remoteTimes.hasNext()) ? remoteTimes.next() : null;
            } else if (status.equals(UPDATE)) {
                updateTime(curRemoteTime, curDbTime, batch);
                curRemoteTime = (remoteTimes.hasNext()) ? remoteTimes.next() : null;
                curDbTime = (dbTimes.hasNext()) ? dbTimes.next() : null;
            } else if (status.equals(DELETE)) {
                deleteTime(curDbTime, batch);
                curDbTime = (dbTimes.hasNext()) ? dbTimes.next() : null;
            } else {
                log.error("Check status for times returned an unknown value of: " + status);
            }
        }

    }

    private void updateCourse(Course remoteCourse, Course dbCourse, EnrollmentChanges batch, String termCode){

        //make updates to dbRow
        Course updatedRecord = updateChecker.updateCourse(remoteCourse, dbCourse);

        //if something updated, mark course for update
        if (updatedRecord != null) {
            log.info("updating course: " + remoteCourse.getSubject() + remoteCourse.getCourseNumber()+" with id: "+dbCourse.getId());
            updatedRecord.setTermCode(termCode);
            batch.getCourses().getUpdates().add(updatedRecord);
        }

        //update all the sections for this course
        updateSections(remoteCourse.getSections(), dbCourse.getSections(), batch, termCode);
    }

    private void updateSection(Section remoteSection, Section dbSection, EnrollmentChanges batch, String termCode){
        //update instructor first in case changes made during section update
        updateInstructor(remoteSection.getInstructor(), dbSection.getInstructor(), batch, termCode);
        //make updates to dbRow
        Section updatedSection = updateChecker.updateSection(remoteSection, dbSection);

        //if something updated, mark section for update
        if (updatedSection != null){
            log.info("  updating section: " + dbSection.getSectionName() + " " +
                    "for " + dbSection.getCourse().getSubject() + dbSection.getCourse().getCourseNumber()+" with id: "+dbSection.getId());
            batch.getSections().getUpdates().add(updatedSection);
            batch.getPreviousSections().put(dbSection.getId(), dbSection);
        }

        //update all the times for this course
        updateTimes(remoteSection.getTimeSlots(), dbSection.getTimeSlots(), batch, termCode);
    }

    private void updateTime(TimeSlot remoteTime, TimeSlot dbTime, EnrollmentChanges batch){

        TimeSlot updatedTime = updateChecker.updateTime(remoteTime, dbTime);

        if(updatedTime != null){
            log.info("      updating time: " + dbTime.getDays() + " " +
                    "for " + dbTime.getSection().getCourse().getSubject() + dbTime.getSection().getCourse().getCourseNumber() + " " + dbTime.getSection().getSectionName()+" with id: "+dbTime.getId());
            batch.getTimeSlots().getUpdates().add(updatedTime);
        }
    }

    private void updateInstructor(Instructor remoteInst, Instructor dbInst, EnrollmentChanges batch, String termCode){
        if(remoteInst == null && dbInst == null) return;

        CheckStatus status = consultant.determineInstructorAction(remoteInst, dbInst);

        if( status.equals(ADD)){
            addInstructor(remoteInst, batch, termCode);
        }
        else if (status.equals(UPDATE)){
            //don't overwrite a newer update to instructor
            if(dbInst.getSyncedTerm() == null || Integer.parseInt(termCode) >= dbInst.getSyncedTerm()) {
                Instructor updatedInstructor = updateChecker.updateInstructor(remoteInst, dbInst);

                if (updatedInstructor != null) {
                    log.info("updating Instructor " + dbInst.getFirstName() + " " + dbInst.getLastName() + " with id: " + dbInst.getId());
                    batch.getInstructors().getUpdates().add(updatedInstructor);
                }
            }
        }
    }

    private void addCourse(Course remoteCourse, EnrollmentChanges batch, String termCode){

        log.info("adding course: "+remoteCourse.getSubject()+remoteCourse.getCourseNumber() + " for " + termCode);
        remoteCourse.setTermCode(termCode);
        //mark course for insertion
        batch.getCourses().getInserts().add(remoteCourse);
        //add subcategory
        addSections(remoteCourse.getSections(), batch, termCode);
    }

    private void addSections(List<Section> sections, EnrollmentChanges batch, String termCode){
        sections.forEach(s -> addSection(s, batch, termCode));
    }

    private void addSection(Section remoteSection, EnrollmentChanges batch, String termCode){
        log.info("  adding section: " + remoteSection.getSectionName() + " " +
                "for " + termCode + " " + remoteSection.getCourse().getSubject() + remoteSection.getCourse().getCourseNumber());
        //mark section for insertion
        batch.getSections().getInserts().add(remoteSection);
        if(remoteSection.getInstructor() != null){
            addInstructor(remoteSection.getInstructor(), batch, termCode);
        }
        //add subcategory
        addTimes(remoteSection.getTimeSlots(), batch, termCode);
    }

    private void addTimes(List<TimeSlot> timeSlots, EnrollmentChanges batch, String termCode) {
        timeSlots.forEach(t -> addTime(t, batch, termCode));
    }

    private void addTime(TimeSlot remoteTime, EnrollmentChanges batch, String termCode){
        log.info("      adding time: " + remoteTime.getDays() + " " +
                "for " + termCode + " " + remoteTime.getSection().getCourse().getSubject() + remoteTime.getSection().getCourse().getCourseNumber() + " " + remoteTime.getSection().getSectionName());
        batch.getTimeSlots().getInserts().add(remoteTime);
    }

    private void addInstructor(Instructor remoteInst, EnrollmentChanges batch, String termCode){
        log.info("adding Instructor "+remoteInst.getFirstName()+" "+remoteInst.getLastName());
        remoteInst.setSyncedTerm(Integer.parseInt(termCode));
        batch.getInstructors().getInserts().add(remoteInst);
    }

    private void deleteCourse(Course dbCourse, EnrollmentChanges batch){
        log.info("deleting course sections for: " + dbCourse.getTermCode() + " " + dbCourse.getSubject() + dbCourse.getCourseNumber());
        //simply delete sections for course
        deleteSections(dbCourse.getSections(), batch);

    }

    private void deleteSections(List<Section> dbSections, EnrollmentChanges batch){
        dbSections.forEach(s -> deleteSection(s, batch));
    }

    private void deleteSection(Section dbSection, EnrollmentChanges batch){
        log.info("  deleting section: " + dbSection.getSectionName() + " for " + dbSection.getCourse().getTermCode() + " " + dbSection.getCourse().getSubject() + dbSection.getCourse().getCourseNumber());
        //mark section for deletion
        batch.getSections().getDeletes().add(dbSection);
        deleteTimes(dbSection.getTimeSlots(), batch);
    }


    private void deleteTimes(List<TimeSlot> timeSlots, EnrollmentChanges batch){
        timeSlots.forEach(t -> deleteTime(t, batch));
    }

    private void deleteTime(TimeSlot dbTime, EnrollmentChanges batch){
        log.info("      deleting time: " + dbTime.getDays() + " "+
        "for " + dbTime.getSection().getCourse().getTermCode() + " " + dbTime.getSection().getCourse().getSubject() + dbTime.getSection().getCourse().getCourseNumber() + " " + dbTime.getSection().getSectionName());
        batch.getTimeSlots().getDeletes().add(dbTime);

    }

    private void persistChanges(University university, Term term, EnrollmentChanges batch){
        //deletes first avoids duplicate keys
        persistDeletes(batch);

        persistBatchInsertCoursesAndInstructors(university, term, batch);
        persistSectionInserts(university, term, batch);
        persistTimeSlotInserts(term, batch);

        persistBatchUpdates(university, batch);

    }

    private void persistDeletes(EnrollmentChanges batch) {
        if(batch.getTimeSlots().getDeletes().size() > 0) {
            persistenceService.batchDeleteTimes(batch.getTimeSlots().getDeletes());
        }
        if(batch.getSections().getDeletes().size() > 0) {
            persistenceService.batchCancelSections(batch.getSections().getDeletes());
        }
    }

    private void persistBatchInsertCoursesAndInstructors(University university, Term term, EnrollmentChanges batch) {
        //insert courses and instructors
        if(batch.getCourses().getInserts().size() > 0) {
            persistenceService.batchInsertCourses(batch.getCourses().getInserts());
            Map<String, Integer> courseIds = persistenceService.getNeededCourseIds(batch.getCourses().getInserts(), university.getId(), Course.class);
            persistenceService.batchInsertCourseVersions(batch.getCourses().getInserts(), term, courseIds);
        }
        if(batch.getInstructors().getInserts().size() > 0 ) {
            //get instructors not already in database
            List<Instructor> instructors = persistenceService.getInstructorsNeedingInsert(batch.getInstructors().getInserts());
            persistenceService.batchInsertInstructors(instructors);
        }
    }

    private void persistSectionInserts(University university, Term term, EnrollmentChanges batch) {
        if(batch.getSections().getInserts().size() > 0) {
            //get course ids and instructor ids for inserting sections
            Map<String, Integer> courseIds = persistenceService.getNeededCourseIds(batch.getSections().getInserts(), university.getId(), Section.class);
            Map<String, Integer> instructorIds = persistenceService.getNeededInstructorIds(batch.getSections().getInserts(), university.getId());

            //insert sections
            persistenceService.batchInsertSections(batch.getSections().getInserts(), courseIds, instructorIds, term.getId());
        }
    }

    private void persistTimeSlotInserts(Term term, EnrollmentChanges batch) {
        if(batch.getTimeSlots().getInserts().size() > 0) {
            //get section ids for inserting times
            Map<Integer, Integer> sectionIds = persistenceService.getNeededSectionIds(batch.getTimeSlots().getInserts(), term.getId());

            //insert times
            persistenceService.batchInsertTimes(batch.getTimeSlots().getInserts(), sectionIds);
        }
    }

    private void persistBatchUpdates(University university, EnrollmentChanges batch) {
        //UPDATES
        if(batch.getCourses().getUpdates().size() > 0) {
            persistenceService.batchUpdateCourses(batch.getCourses().getUpdates());
        }
        if(batch.getSections().getUpdates().size() > 0) {
            Map<String, Integer> instructorIds = persistenceService.getNeededInstructorIds(batch.getSections().getUpdates(), university.getId());
            persistenceService.batchUpdateSections(batch.getSections().getUpdates(), instructorIds);
        }
        if(batch.getTimeSlots().getUpdates().size() > 0) {
            persistenceService.batchUpdateTimes(batch.getTimeSlots().getUpdates());
        }
        if(batch.getInstructors().getUpdates().size() > 0) {
            persistenceService.batchUpdateInstructors(batch.getInstructors().getUpdates());
        }
    }

}
