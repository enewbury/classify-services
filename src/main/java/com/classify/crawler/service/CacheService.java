package com.classify.crawler.service;

import com.classify.model.Semester;

import java.time.LocalDateTime;

/**
 * Created by Eric Newbury on 6/13/17.
 */
public interface CacheService {
    Long getUVMEnrollmentUpdated(Semester semester);

    void setUVMEnrollmentUpdated(Semester semester, Long time);

    void clearUVMEnrollmentUpdatedCache();

    void setBrokenIntegration(String location, LocalDateTime notified);

    LocalDateTime getBrokenIntegration(String location);

    LocalDateTime getExecutingJob(String jobName);

    void setExecutingJob(String jobName, LocalDateTime startTime);
}
