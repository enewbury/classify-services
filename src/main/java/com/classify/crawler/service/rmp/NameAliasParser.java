package com.classify.crawler.service.rmp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class NameAliasParser {
    private static final Log log = LogFactory.getLog(NameAliasParser.class);
    private static final String aliasFilePath = "name_alias.txt";

    public static Map<String, List<String>> getAliasMap() {
        Map<String, List<String>> aliasMap = new HashMap<>();
        BufferedReader input = null;

        try {
            InputStream is = NameAliasParser.class.getResourceAsStream(aliasFilePath);
            input = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while ((line = input.readLine()) != null) {
                //System.out.println("line = " + line);
                StringTokenizer st = new StringTokenizer(line, ",");
                String key = st.nextToken();
                List<String> values = new ArrayList<>();
                while (st.hasMoreElements()) {
                    values.add(st.nextToken());
                }
                aliasMap.put(key, values);
            }


        } catch (IOException ex) {
            log.error("Couldn't connect to alias file", ex);
        } finally {
            try {
                if(input != null) {
                    input.close();
                }
            } catch (IOException ex) {
               log.error("Couldn't close alias file", ex);
            }
        }

        return aliasMap;
    }
}
