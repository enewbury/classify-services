package com.classify.crawler.service.rmp;

import com.classify.common.PersistenceService;
import com.classify.crawler.helper.JsoupHelper;
import com.classify.crawler.helper.UrlFactory;
import com.classify.common.MailService;
import com.classify.dispatcher.DispatchService;
import com.classify.exceptions.BrokenIntegrationException;
import com.classify.model.business.objects.University;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jooq.tables.records.JInstructorRecord;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class RateMyProfessorsService {
    private static final Log log = LogFactory.getLog(RateMyProfessorsService.class);

    private UrlFactory urlFactory = new UrlFactory();
    private ObjectMapper mapper = new ObjectMapper();

    private final PersistenceService persistenceService;
    private final DispatchService dispatchService;

    @Autowired
    public RateMyProfessorsService(DispatchService dispatchService, PersistenceService persistenceService) {
        this.dispatchService = dispatchService;
        this.persistenceService = persistenceService;
    }

    public void updateRMPData(University university) {
        Map<String,List<String>> nameAliasMap = NameAliasParser.getAliasMap();

        Integer rmpSchoolId;
        //already have the id?
        if(university.getRmpId() != null){
            rmpSchoolId = university.getRmpId();
        }
        //load the id from RMP
        else{
            rmpSchoolId = loadSchoolIdFromRMP(university);
        }

        //if this school is on RMP
        if(rmpSchoolId != null) {
            //get all the instructors to update
            List<JInstructorRecord> instructors = persistenceService.getInstructorsForUniversity(university.getId());

            for (JInstructorRecord instructor : instructors) {
                try {
                    updateInstructorRMPData(instructor, rmpSchoolId, nameAliasMap);
                } catch (BrokenIntegrationException e) {
                    //send notification
                    dispatchService.handleBrokenIntegration(e);
                }
                //randomize time between requests to avoid loading their system.
                waitRandomTimeBetween1And10Seconds();
            }
        }
        //else log that we couldn't crawl for this school
        else{
            log.warn("Couldn't find a rateMyProfessor schoolId for " + university.getAcronym());
        }
    }

    private Integer loadSchoolIdFromRMP(University university) {
        try {
            Integer rmpSchoolId = null;

            String sURL = urlFactory.getRMPSchoolIdApi(university);
            JsonNode root = mapper.readTree(new URL(sURL));
            Iterator<JsonNode> docNodes = root.path("response").path("docs").elements();
            if(docNodes.hasNext()){
                rmpSchoolId = docNodes.next().path("pk_id").asInt();
            }

            if(rmpSchoolId != null) {
                //persist api
                persistenceService.updateRMPIdForUniversity(university.getId(), rmpSchoolId);
            }

            return rmpSchoolId;

        } catch (Exception e) {
            log.error("Could not connect to rmp schoolId API", e);
            return null;
        }
    }

    private void updateInstructorRMPData(JInstructorRecord instructor, int rmpSchoolId, Map<String, List<String>> nameAliasMap) throws BrokenIntegrationException {
        Integer rmpInstructorId = getInstructorRMPId(instructor.getFirstName(), instructor, rmpSchoolId);

        //lookup all aliases.
        if(rmpInstructorId == null){
            if(nameAliasMap.get(instructor.getFirstName().toLowerCase()) != null){
                for (String alias: nameAliasMap.get(instructor.getFirstName().toLowerCase())){
                    rmpInstructorId = getInstructorRMPId(alias, instructor, rmpSchoolId);
                    if(rmpInstructorId != null) break;
                }
            }
        }

        if(rmpInstructorId != null) {
            crawlInstructorPageAndUpdateData(instructor, rmpInstructorId);
        }
    }

    private Integer getInstructorRMPId(String firstNameAlias, JInstructorRecord instructor, int rmpSchoolId) {
        if(instructor.getRmpId() != null){
            //get instructor rmp id from database
            return instructor.getRmpId();
        }
        else{
            //get rmp instructor id from api
            Integer instructorRMPId =  loadInstructorIdFromRMP(firstNameAlias, instructor, rmpSchoolId);
            if( instructorRMPId !=null ){
                instructor.setRmpId(instructorRMPId);
                return instructorRMPId;
            }
            else{
                return null;
            }
        }
    }

    private Integer loadInstructorIdFromRMP(String firstNameAlias, JInstructorRecord instructor, int rmpSchoolId) {
        try {
            Integer rmpInstructorId = null;

            String sURL = urlFactory.getRMPInstructorIdApi(firstNameAlias, instructor.getLastName(), rmpSchoolId);
            JsonNode root = mapper.readTree(new URL(sURL));
            Iterator<JsonNode> docNodes = root.path("response").path("docs").elements();
            if(docNodes.hasNext()){
                rmpInstructorId = docNodes.next().path("pk_id").asInt();
            }

            return rmpInstructorId;

        } catch (Exception e) {
            log.error("Could not connect to rmp instructorId API", e);
            return null;
        }
    }

    private void crawlInstructorPageAndUpdateData(JInstructorRecord instructor, int rmpInstructorId) throws BrokenIntegrationException {


        try {
            //connect with Jsoup
            String profileUrl = UrlFactory.getInstructorProfileUrl(rmpInstructorId);
            Connection.Response response = Jsoup.connect(profileUrl).userAgent(JsoupHelper.getRandomUserAgent()).execute();

            if (response.statusCode() == 200) {
                Document doc = response.parse();
                //getRating
                Elements elsMatchingRating = doc.select("#mainContent > .right-panel > .rating-breakdown > .left-breakdown .breakdown-wrapper > .breakdown-header:first-child .grade");
                //get Blurb
                Elements elsMatchingBlurb = doc.select("#mainContent > .right-panel > .rating-filter tr:nth-of-type(2) .commentsParagraph");

                if(elsMatchingRating.size() ==1 && elsMatchingBlurb.size() ==1){
                    //set id
                    if (instructor.getRmpId() == null) {
                        instructor.setRmpId(rmpInstructorId);
                    }
                    //set rating
                    instructor.setRmpRating(Double.parseDouble(elsMatchingRating.first().text().trim()));
                    //set blurb
                    instructor.setRmpLatestComment(elsMatchingBlurb.first().text().trim());
                    //persist it
                    persistenceService.updateInstructorRMPData(instructor);
                }
                else if (profHasNoRatings(doc.select(".headline"))){
                    //no op
                }
                else{
                    //couldn't find correct divs, changed page?
                    throw new BrokenIntegrationException("RMP instructor profile","RateMyProfessors may have changed their page structure. Triggered while looking for prof with rmp id: "+rmpInstructorId);
                }
            }
            else{
                //couldn't connect to profile, url changed?
                throw new BrokenIntegrationException("RMP instructor profile", "RateMyProfessor profile didn't return 200.  May have changed urls structure.");
            }

        }
        catch (IOException e){
            log.warn("Couldn't connect to professor profile", e);
        }
    }

    private boolean profHasNoRatings(Elements headline) {
        return (headline.size()!=0 && headline.text().contains("Be the first to rate Professor"));
    }


    private void waitRandomTimeBetween1And10Seconds(){
        int waitTime = RandomUtils.nextInt(10000-1000)+1000;
        try {
            Thread.sleep(waitTime);
        } catch (InterruptedException e) {
            log.error("Couldn't sleep thread while crawling RMPData",e);
        }
    }

}
