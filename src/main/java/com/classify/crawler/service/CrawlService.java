package com.classify.crawler.service;

import com.classify.model.Semester;
import com.classify.model.business.objects.University;
import com.classify.model.business.objects.User;


public interface CrawlService{

    void getHighPriorityData(University university);

    void getDailyData(University university);

    User crawlUserDetails(User user, String universityUsername);
}
