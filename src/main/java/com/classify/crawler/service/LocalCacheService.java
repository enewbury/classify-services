package com.classify.crawler.service;

import com.classify.model.Semester;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Component
public class LocalCacheService implements CacheService {
    private Map<String, Object> cache = new HashMap<>();

    @Override
    public Long getUVMEnrollmentUpdated(Semester semester){
        return (Long) cache.get("UVM-" + semester.toString() + "-enrollment-updated");
    }

    @Override
    public void setUVMEnrollmentUpdated(Semester semester, Long time){
        cache.put("UVM-" + semester.toString() + "-enrollment-updated", time);
    }

    @Override
    public void clearUVMEnrollmentUpdatedCache(){
        for(Semester semester: Semester.values()) {
            cache.remove("UVM-" + semester.toString() + "-enrollment-updated");
        }
    }

    @Override
    public void setBrokenIntegration(String location, LocalDateTime notified){
        cache.put("Broken-Integration-"+location, notified);
    }

    @Override
    public LocalDateTime getBrokenIntegration(String location){
        return (LocalDateTime) cache.get("Broken-Integration-"+location);
    }

    @Override
    public LocalDateTime getExecutingJob(String jobName) {
        return (LocalDateTime) cache.get(jobName);
    }

    @Override
    public void setExecutingJob(String jobName, LocalDateTime startTime) {
        cache.put(jobName, startTime);
    }
}
