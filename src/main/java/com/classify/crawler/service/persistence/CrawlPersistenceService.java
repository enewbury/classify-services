package com.classify.crawler.service.persistence;

import com.classify.model.business.objects.*;
import com.classify.model.business.objects.enums.Standing;
import com.classify.model.business.objects.mappers.JooqDataMapper;
import com.google.common.collect.Lists;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.jooq.Batch;
import org.jooq.BatchBindStep;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.exception.DataAccessException;
import org.jooq.tables.*;
import org.jooq.tables.records.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.jooq.Tables.*;
import static org.jooq.impl.DSL.concat;

@Service
public class CrawlPersistenceService{
    private Log log = LogFactory.getLog(CrawlPersistenceService.class);

    private JooqDataMapper dataMapper = new JooqDataMapper();

    protected DSLContext db;

    @Autowired
    public CrawlPersistenceService(@SuppressWarnings("SpringJavaAutowiringInspection") DSLContext db) {
        this.db = db;
    }


    public void batchInsertCourses(List<Course> courses) {
        log.info("adding new courses");

        BatchBindStep courseBatch = db.batch(db
                .insertInto(COURSE, COURSE.SUBJECT, COURSE.COURSE_NUMBER, COURSE.UNIVERSITY_ID)
                .values((String) null, (String) null, (Integer) null)
                .onDuplicateKeyIgnore());



        for(Course course: courses){
            if(course.getUniversityId() == null){
                throw new NullPointerException("Warning tried to insert course that had no university id set. Make sure an id is assigned when loading in data");
            }
            courseBatch.bind(course.getSubject(), course.getCourseNumber(), course.getUniversityId());
        }

        //try twice
        try {
            courseBatch.execute();
        } catch(DataAccessException e){
            courseBatch.execute();
        }
    }

    public void batchInsertCourseVersions(List<Course> courses, Term term, Map<String, Integer> courseIds){
        BatchBindStep versionBatch = db.batch(db
                .insertInto(COURSE_VERSION, COURSE_VERSION.COURSE_ID, COURSE_VERSION.NAME, COURSE_VERSION.CREDITS_MIN, COURSE_VERSION.CREDITS_MAX, COURSE_VERSION.TERM_CODE)
                .values((Integer) null, (String) null, (Double) null, (Double) null, (String) null)
                .onDuplicateKeyIgnore());

        for(Course course: courses){
            Integer courseId = courseIds.get(course.getSubject() + course.getCourseNumber());
            versionBatch.bind(courseId, course.getName(), course.getCreditsMin(), course.getCreditsMax(), term.getTermCode());
        }

        //try twice
        try {
            versionBatch.execute();
        } catch(DataAccessException e){
            versionBatch.execute();
        }
    }

    public void batchInsertInstructors(List<Instructor> instructors) {
        log.info("adding new instructors if not exists");
        BatchBindStep batch = db.batch(db
                .insertInto(INSTRUCTOR, INSTRUCTOR.FIRST_NAME, INSTRUCTOR.LAST_NAME, INSTRUCTOR.UNIVERSITY_USERNAME, INSTRUCTOR.EMAIL, INSTRUCTOR.UNIVERSITY_ID, INSTRUCTOR.SYNCED_TERM)
                .values((String) null, (String) null, (String) null, (String) null, (Integer) null, (Integer) null)
                .onDuplicateKeyIgnore());

        for(Instructor inst: instructors){
            if(inst.getUniversityId() == null){
                throw new NullPointerException("Warning tried to insert instructor that had no university id set. Make sure an id is assigned when loading in data");
            }
            batch.bind(inst.getFirstName(), inst.getLastName(), inst.getUniversityUsername(), inst.getEmail(), inst.getUniversityId(), inst.getSyncedTerm());
        }

        //try twice
        try {
            batch.execute();
        } catch(DataAccessException e){
            batch.execute();
        }
    }

    public void batchInsertSections(List<Section> sections, Map<String, Integer> insertedCourseIds, Map<String, Integer> insertedInstructorIds, int semesterId) {
        log.info("adding new sections");
        List<JSectionRecord> jSections = Lists.newArrayList();

        for(Section section: sections){
            JSectionRecord jSection = db.newRecord(JSection.SECTION);
            jSection = dataMapper.mapSection(section, jSection);

            //get course Id from map
            Integer courseId = insertedCourseIds.get(section.getCourse().getSubject() + section.getCourse().getCourseNumber());
            if (courseId == null){
                log.error("Found no Id in last inserted map for course: "+ section.getCourse().getSubject()+section.getCourse().getCourseNumber());
                log.error("There were "+insertedCourseIds.size() + " results and the key looks like + '" + insertedCourseIds.keySet().stream().findAny().get());
                continue;
            }

            jSection.setCourseId(courseId);
            jSection.setTermId(semesterId);

            //get instructor Id from map (not every section has an instructor)
            Integer instructorId = null;
            if(section.getInstructor() !=null ){
                String username = section.getInstructor().getUniversityUsername();
                instructorId = insertedInstructorIds.get(username);
            }

            jSection.setInstructorId(instructorId);

            jSections.add(jSection);
        }

        Batch batch = db.batchInsert(jSections);

        //try twice
        try {
            batch.execute();
        } catch(DataAccessException e){
            batch.execute();
        }
    }

    public void batchInsertTimes(List<TimeSlot> times, Map<Integer, Integer> insertedSectionIds) {
        log.info("adding new times");

        List<JTimeSlotRecord> jTimes = Lists.newArrayList();

        for(TimeSlot time : times){
            JTimeSlotRecord jTime = db.newRecord(JTimeSlot.TIME_SLOT);
            jTime = dataMapper.mapTimeSlot(time, jTime);

            //get sectionId from map
            Integer sectionId = insertedSectionIds.get(time.getSection().getUniversitySectionId());
            if (sectionId == null){
                log.error("Found no Id in last inserted map for section: "+
                        time.getSection().getCourse().getSubject()+ time.getSection().getCourse().getCourseNumber()+ time.getSection().getSectionName());
                log.error("There were "+insertedSectionIds.size() + " results and the key looks like + '" + insertedSectionIds.keySet().stream().findAny().get());
                continue;
            }

            jTime.setSectionId(sectionId);

            jTimes.add(jTime);
        }

        Batch batch = db.batchInsert(jTimes);
        //try twice
        try {
            batch.execute();
        } catch(DataAccessException e){
            batch.execute();
        }
    }

    public Map<String, Integer> getNeededCourseIds(List<? extends Object> inserts, int universityId, Class clazz) {
        log.info("looking up last inserted courseIds");

        if(inserts.size() == 0){return new HashMap<>();}

        //get concatenation of subject and course_num and universityId i.e. ASCI0121 for all inserts
        List<String> identifiers = inserts.stream()
                .map(insert -> getMapKeyFromSubjectAndNum(insert, clazz))
                .distinct()
                .collect(Collectors.toList());

        JCourse c = COURSE.as("c");
        Field<String> cat = concat(c.SUBJECT, c.COURSE_NUMBER).as("subNum");
        return db
                .select(concat(c.SUBJECT, c.COURSE_NUMBER).as("subNum"), c.ID)
                .from(c).where(concat(c.SUBJECT, c.COURSE_NUMBER).in(identifiers)
                        .and(c.UNIVERSITY_ID.eq(universityId)))
                .fetchMap(cat,c.ID);
    }

    private String getMapKeyFromSubjectAndNum(Object insert, Class clazz){
        if(clazz.equals(Section.class)){
            Section sec = (Section) insert;
            return sec.getCourse().getSubject() + sec.getCourse().getCourseNumber();
        } else if(clazz.equals(Course.class)){
            Course c = (Course) insert;
            return c.getSubject() + c.getCourseNumber();
        } else {
            log.error("asked for incompatible key map type");
            return "";
        }

    }

    public Map<String, Integer> getNeededInstructorIds(List<Section> inserts, int universityId) {
        log.info("looking up instructor ids for sections about to be inserted/updated");
        if(inserts.size() == 0){return new HashMap<>();}

        List<String> identifiers = inserts.stream()
                .filter(s -> s.getInstructor() != null)
                .map(s -> s.getInstructor().getUniversityUsername())
                .distinct()
                .collect(Collectors.toList());

        JInstructor i = INSTRUCTOR.as("i");

        return db
                .select(i.ID, i.UNIVERSITY_USERNAME).from(i)
                .where(i.UNIVERSITY_USERNAME.in(identifiers).and(i.UNIVERSITY_ID.eq(universityId)))
                .fetchMap(i.UNIVERSITY_USERNAME,i.ID);
    }


    public Map<Integer, Integer> getNeededSectionIds(List<TimeSlot> inserts, int semesterId) {
        log.info("looking up last inserted section Ids");

        List<Integer> identifiers = inserts.stream()
                .map(t -> t.getSection().getUniversitySectionId())
                .distinct()
                .collect(Collectors.toList());

        JSection s = SECTION.as("s");

        return db
                .select(s.ID,s.UNIVERSITY_SECTION_ID).from(s)
                .where(s.UNIVERSITY_SECTION_ID.in(identifiers).and(s.TERM_ID.eq(semesterId).and(s.CANCELLED.notEqual(true))))
                .fetchMap(s.UNIVERSITY_SECTION_ID, s.ID);
    }


    public void batchUpdateCourses(List<Course> updates) {
        log.info("persisting course updates");
        List<JCourseRecord> jCourses = new ArrayList<>();
        List<JCourseVersionRecord> jCourseVersions = new ArrayList<>();

        for(Course u: updates){
            JCourseRecord jCourse = db.newRecord(COURSE);
            jCourse = dataMapper.mapCoursePrimaryData(u, jCourse);

            JCourseVersionRecord jCourseVersion = db.newRecord(COURSE_VERSION);
            jCourseVersion = dataMapper.mapCourseVersionPrimaryData(u, jCourseVersion);
            jCourses.add(jCourse);
            jCourseVersions.add(jCourseVersion);
        }

        db.batchUpdate(jCourses).execute();
        db.batchUpdate(jCourseVersions).execute();
    }

    public void batchUpdateSections(List<Section> updates, Map<String, Integer> instructorIds) {
        log.info("persisting section updates");
        List<JSectionRecord> jSections = new ArrayList<>();

        for(Section u: updates){
            JSectionRecord jSection = db.newRecord(SECTION);
            jSection = dataMapper.mapSection(u, jSection);
            //always reset the instructor id.  If has an instructor, and it is found in the map, set it.
            if(u.getInstructor() != null && instructorIds.get(u.getInstructor().getUniversityUsername()) != null){
                jSection.setInstructorId(instructorIds.get(u.getInstructor().getUniversityUsername()));
            }
            jSections.add(jSection);
        }

        db.batchUpdate(jSections).execute();

        insertSectionMetrics(updates);
    }

    private void insertSectionMetrics(List<Section> updates) {
        List<JSectionMetricsRecord> metrics = new ArrayList<>();

        updates.stream().filter(update -> update.getSeats() != null && update.getSeatsFilled() != null).forEach(update -> {
            JSectionMetricsRecord metric = db.newRecord(SECTION_METRICS);
            metric.setSectionId(update.getId());
            metric.setSeats(update.getSeats());
            metric.setSeatsFilled(update.getSeatsFilled());
            metric.setTimeStamp(LocalDateTime.now());
            metrics.add(metric);
        });

        Batch batch = db.batchInsert(metrics);

        //try twice
        try {
            batch.execute();
        } catch(DataAccessException e){
            batch.execute();
        }
    }

    public void batchUpdateTimes(List<TimeSlot> updates) {
        log.info("persisting time updates");

        List<JTimeSlotRecord> jTimes = new ArrayList<>();

        for(TimeSlot u: updates){
            JTimeSlotRecord jTime = db.newRecord(TIME_SLOT);
            jTime = dataMapper.mapTimeSlot(u, jTime);
            jTimes.add(jTime);

        }
        db.batchUpdate(jTimes).execute();
    }

    public void batchUpdateInstructors(List<Instructor> updates) {
        log.info("persisting instructor updates");
        List<JInstructorRecord> jProfs = new ArrayList<>();

        for(Instructor u: updates){
            JInstructorRecord jProf = db.newRecord(INSTRUCTOR);
            jProf = dataMapper.mapInstructorPrimaryData(u, jProf);

            jProfs.add(jProf);
        }

        db.batchUpdate(jProfs).execute();
    }

    public void batchCancelSections(List<Section> deletes) {
        log.info("persisting section cancellations");

        List<Integer> sectionIds = deletes.stream().map(Section::getId).collect(Collectors.toList());
        db.update(SECTION).set(SECTION.CANCELLED, true).where(SECTION.ID.in(sectionIds)).execute();
    }

    public void batchDeleteTimes(List<TimeSlot> deletes) {
        log.info("persisting time deletes");

        List<JTimeSlotRecord> jTimes = Lists.newArrayList();

        for(TimeSlot time: deletes){
            JTimeSlotRecord jTime = db.newRecord(TIME_SLOT);
            jTime = dataMapper.mapTimeSlot(time, jTime);

            jTimes.add(jTime);
        }

        db.batchDelete(jTimes).execute();
    }

    public List<Instructor> getInstructorsNeedingInsert(List<Instructor> instructors) {
        if(instructors.get(0).getUniversityId() == null){
            throw new NullPointerException("Warning tried to select instructor with no university id set. Make sure an id is assigned when loading in data");
        }
        int lastIndex = 0;
        List<Instructor> newInstructors = Lists.newArrayList();

        while(lastIndex < instructors.size()) {
            List<Instructor> currentInstructorSection = instructors.subList(lastIndex, Math.min(lastIndex + 2000, instructors.size() - 1));
            lastIndex = lastIndex+2000;
            List<String> usernames = currentInstructorSection.stream()
                    .map(Instructor::getUniversityUsername)
                    .collect(Collectors.toList());

            JInstructor i = JInstructor.INSTRUCTOR.as("i");
            List<Instructor> returnedInstructors = db
                    .selectFrom(i)
                    .where(
                            i.UNIVERSITY_USERNAME.in(usernames)
                            .and(i.UNIVERSITY_ID.eq(instructors.get(0).getUniversityId())))
                    .fetchInto(Instructor.class);

            List<Instructor> filteredPart = instructors.stream()
                    .filter(inst -> !returnedInstructors.contains(inst))
                    .collect(Collectors.toList());

            newInstructors.addAll(filteredPart);
        }

        return newInstructors;
    }



    public List<JCourseRecord> getAllCoursesForUniversity(University university){
        return db.selectFrom(COURSE).where(COURSE.UNIVERSITY_ID.eq(university.getId())).fetch();
    }

    public List<Term> getLastThreeSemestersForUniversity(University university) {
        return db.selectFrom(TERM).where(TERM.UNIVERSITY_ID.eq(university.getId()))
                .orderBy(TERM.TERM_CODE.desc())
                .limit(3).fetchInto(Term.class);
    }

    public List<Course> getCoursesWithVersion(Term term) {
        JCourse c = COURSE.as("c");
        JCourseVersion v = COURSE_VERSION.as("v");

        return db.select(c.SUBJECT,c.COURSE_NUMBER, v.ID.as("version_id"),v.DESCRIPTION).from(c)
                .join(v).on(v.COURSE_ID.eq(c.ID))
                .where(
                        v.TERM_CODE.eq(term.getTermCode()).and(
                        c.UNIVERSITY_ID.eq(term.getUniversityId())
                )).fetchInto(Course.class);
    }

    public void updateCourseDescriptions(List<Course> updatedCourses) {

        List<JCourseVersionRecord> versionRecords = new ArrayList<>();
        for(Course course: updatedCourses){
            JCourseVersionRecord versionRecord = db.newRecord(JCourseVersion.COURSE_VERSION);
            versionRecord.setId(course.getVersionId());
            versionRecord.setDescription(course.getDescription());
            versionRecords.add(versionRecord);
        }
        db.batchUpdate(versionRecords).execute();
    }

    public List<String> getAllSubjectsForSchool(University university) {
        return db.select(COURSE.SUBJECT).from(COURSE)
                .where(COURSE.UNIVERSITY_ID.eq(university.getId()))
                .groupBy(COURSE.SUBJECT).fetch(COURSE.SUBJECT);
    }

    public void updateTermRegistrationDates(final List<RegistrationTime> registrationTimes) {

        registrationTimes.forEach(time -> {
            JRegistrationTimeRecord record = db.selectFrom(REGISTRATION_TIME)
                    .where(REGISTRATION_TIME.STANDING.eq(time.getStanding().toString()))
                    .and(REGISTRATION_TIME.TERM_ID.eq(time.getTermId())).fetchOne();
            if (record == null){
                record = db.newRecord(REGISTRATION_TIME);
                record.setStanding(time.getStanding().toString()).setTermId(time.getTermId());
            }
            if (time.getStart() != null && !time.getStart().equals(record.getStart())){
                record.setStart(time.getStart());
            }
            if (time.getEnd() != null && !time.getEnd().equals(record.getEnd())){
                record.setEnd(time.getEnd());
            }
            record.store();
        });

    }

    public void updateTermCalendarDates(Term term) {
        db.update(TERM)
            .set(TERM.FIRST_DAY_OF_CLASSES, term.getFirstDayOfClasses())
            .set(TERM.LAST_DAY_OF_CLASSES, term.getLastDayOfClasses())
            .set(TERM.WITHDRAW_DEADLINE, term.getWithdrawDeadline())
            .where(TERM.ID.eq(term.getId())).execute();
    }

    public void updateOpenRegistrationEndDate(Term term, LocalDate addDropDate) {
        JRegistrationTimeRecord record = db.selectFrom(REGISTRATION_TIME).where(REGISTRATION_TIME.STANDING.eq(Standing.ALL.toString()))
                .and(REGISTRATION_TIME.TERM_ID.eq(term.getId())).fetchOne();

        if(record != null){
            record.setEnd(addDropDate.atTime(23,59,59));
            record.update();
        } else {
            log.warn("Tried to update the add drop date for open registartion, but it did not exist yet. TermID: " + term.getId());
        }
    }
}
