package com.classify.crawler.helper;

import com.classify.model.Semester;
import org.jooq.tools.StringUtils;

public class TermConverter {

    public int getYear(String code){

        if (code == null || code.length() < 6) throw new IllegalArgumentException("Term "+code+" is invalid. Cannot get year.");
        return Integer.parseInt(code.substring(0, 4));

    }
    public Semester getSemester(String code){

        if (code == null || code.length() < 6){return null;}
        String seasonCode = code.substring(4);

        switch (seasonCode){
            case "01":
                return Semester.SPRING;
            case "06":
                return Semester.SUMMER;
            case "09":
                return Semester.FALL;
            default:
                return null;
        }
    }

    public String getSemesterCode(Semester semester){
        if (semester == null){ return null; }

        switch(semester){
            case SPRING:
                return "01";
            case SUMMER:
                return "06";
            case FALL:
                return "09";
            default:
                return null;
        }
    }

    public String getTermCode(Semester semester, String year){
        if (semester == null || StringUtils.isEmpty(year)){ return null; }
        return year.trim() + getSemesterCode(semester);
    }

}
