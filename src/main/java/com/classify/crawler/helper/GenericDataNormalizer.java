package com.classify.crawler.helper;

import com.classify.model.business.objects.mappers.GenericDataMapper;
import com.classify.model.uvm.DenormalizedCourseData;
import org.apache.commons.lang3.StringUtils;

public class GenericDataNormalizer extends DataNormalizer<DenormalizedCourseData> {

    public GenericDataNormalizer(){
        setDataMapper(new GenericDataMapper());
    }

    @Override
    protected boolean isSameCourse(DenormalizedCourseData cur, DenormalizedCourseData last) {
        return (
                last != null &&
                cur.subject.equals(last.subject) && cur.courseNumber.equals(last.courseNumber)
        );
    }

    @Override
    protected boolean isSameSection(DenormalizedCourseData cur, DenormalizedCourseData last) {
        return (
                last != null &&
                isSameCourse(cur, last) &&
                cur.universitySectionId.equals(last.universitySectionId)
        );
    }

    @Override
    protected boolean isDoubleBooked(DenormalizedCourseData cur, DenormalizedCourseData last) {

            return (
                    isSameSection(cur, last) &&
                    (StringUtils.contains(cur.days, last.days) || StringUtils.contains(last.days, cur.days)) && //days overlap
                    (cur.startTime.isBefore(last.endTime) && cur.endTime.isAfter(last.startTime)) //times overlap
            );

    }

    @Override
    protected boolean validTimeSlot(DenormalizedCourseData record) {
        boolean somethingIsSet = false;
        if(
            record!=null &&
            (record.days !=null ||
            record.startTime !=null ||
            record.endTime !=null)

        ){
            somethingIsSet=true;
        }

        if(somethingIsSet && (
            record.days == null ||
            record.startTime == null ||
            record.endTime == null
        )){
            return false;
        }
        return true;
    }


}
