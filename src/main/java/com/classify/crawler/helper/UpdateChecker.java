package com.classify.crawler.helper;

import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.Instructor;
import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.TimeSlot;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@Component
public class UpdateChecker {
    private Log log = LogFactory.getLog(UpdateChecker.class);


    public Course updateCourse(@NotNull Course remoteCourse, @NotNull Course dbCourse) {
        if(remoteCourse == null || dbCourse == null){
            throw new NullPointerException("Call to update course was given a null course");
        }

        boolean changed = false;

        if (!Objects.equals(remoteCourse.getName(), dbCourse.getName())) {dbCourse.setName(remoteCourse.getName()); changed = true;}
        if (!Objects.equals(remoteCourse.getSubject(), dbCourse.getSubject())) {dbCourse.setSubject(remoteCourse.getSubject()); changed = true; }
        if (!Objects.equals(remoteCourse.getCourseNumber(), dbCourse.getCourseNumber())) {dbCourse.setCourseNumber(remoteCourse.getCourseNumber()); changed = true; }
        if (!Objects.equals(remoteCourse.getCreditsMin(), dbCourse.getCreditsMin())) {dbCourse.setCreditsMin(remoteCourse.getCreditsMin()); changed = true;}
        if (!Objects.equals(remoteCourse.getCreditsMax(), dbCourse.getCreditsMax())) {dbCourse.setCreditsMax(remoteCourse.getCreditsMax()); changed = true;}

        if (changed){
            return dbCourse;
        }
        return null;
    }

    public Section updateSection(@NotNull Section remoteSection, @NotNull Section dbSection) {
        if(remoteSection == null || dbSection == null){
            throw new NullPointerException("Call to update section was given a null section");
        }

        boolean changed = false;

        Section updatedSection = new Section(dbSection);

        if(!Objects.equals(remoteSection.getUniversitySectionId(), dbSection.getUniversitySectionId())){ updatedSection.setUniversitySectionId(remoteSection.getUniversitySectionId()); changed = true; }
        if(!Objects.equals(remoteSection.getSectionName(), dbSection.getSectionName())){ updatedSection.setSectionName(remoteSection.getSectionName()); changed = true; }
        if(!Objects.equals(remoteSection.getType(), dbSection.getType())){ updatedSection.setType(remoteSection.getType()); changed = true; }
        if(!Objects.equals(remoteSection.getSeats(), dbSection.getSeats())){ updatedSection.setSeats(remoteSection.getSeats()); changed = true; }
        if(!Objects.equals(remoteSection.getSeatsFilled(), dbSection.getSeatsFilled())){ updatedSection.setSeatsFilled(remoteSection.getSeatsFilled()); changed = true; }

        //update assigned instructor
        if(!Objects.equals(remoteSection.getInstructor(), dbSection.getInstructor())){updatedSection.setInstructorId(null); updatedSection.setInstructor(remoteSection.getInstructor()); changed=true;}
        //null out id if null instructor
        if(remoteSection.getInstructor() == null){updatedSection.setInstructorId(null);}

        if(changed) return updatedSection;
        return null;
    }

    public Instructor updateInstructor(@NotNull Instructor remoteInst, @NotNull Instructor dbInst) {
        if(remoteInst == null || dbInst == null){
            throw new NullPointerException("Call to updateInstructor was given a null instructor");
        }

        if (dbInst.getId() == null) return null;
        boolean changed = false;

        if(!Objects.equals(remoteInst.getFirstName(), dbInst.getFirstName())){dbInst.setFirstName(remoteInst.getFirstName()); changed = true; }
        if(!Objects.equals(remoteInst.getLastName(), dbInst.getLastName())){dbInst.setLastName(remoteInst.getLastName()); changed=true; }
        if(!Objects.equals(remoteInst.getUniversityUsername(), dbInst.getUniversityUsername())) {dbInst.setUniversityUsername(remoteInst.getUniversityUsername()); changed = true; }
        if(!Objects.equals(remoteInst.getEmail(), dbInst.getEmail())){dbInst.setEmail(remoteInst.getEmail()); changed=true;}

        if(changed) return dbInst;
        return null;
    }

    public TimeSlot updateTime(@NotNull TimeSlot remoteTime, @NotNull TimeSlot dbTime) {
        if(remoteTime == null || dbTime == null){
            throw new NullPointerException("Call to updateTime was given a null time");
        }

        boolean changed = false;

        if(dbTime.getId()==null){ return null; }

        if(!Objects.equals(remoteTime.getStartTime(), dbTime.getStartTime())){dbTime.setStartTime(remoteTime.getStartTime()); changed=true; }
        if(!Objects.equals(remoteTime.getEndTime(), dbTime.getEndTime())){dbTime.setEndTime(remoteTime.getEndTime()); changed=true; }
        if(!Objects.equals(remoteTime.getDays(), dbTime.getDays())) { dbTime.setDays(remoteTime.getDays()); dbTime.setDaysList(remoteTime.getDaysList()); changed = true; }
        if(!Objects.equals(remoteTime.getBuilding(), dbTime.getBuilding())){dbTime.setBuilding(remoteTime.getBuilding()); changed=true; }
        if(!Objects.equals(remoteTime.getRoom(), dbTime.getRoom())){dbTime.setRoom(remoteTime.getRoom()); changed=true; }
        if(!Objects.equals(remoteTime.getNonWeekly(), dbTime.getNonWeekly())) {dbTime.setNonWeekly(remoteTime.getNonWeekly()); changed = true; }

        if(changed) return dbTime;
        return null;
    }

}
