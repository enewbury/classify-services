package com.classify.crawler.helper;

import com.classify.model.business.objects.mappers.JooqDataMapper;
import org.jooq.Record;

import static org.jooq.Tables.COURSE;
import static org.jooq.Tables.SECTION;

public class JooqDataNormalizer extends DataNormalizer<Record> {

    public JooqDataNormalizer(){
        setDataMapper(new JooqDataMapper());
    }
    @Override
    protected boolean isSameCourse(Record cur, Record last){
        return (
                last != null &&
                cur.getValue(COURSE.getName()+"_"+COURSE.ID.getName()).equals(last.getValue(COURSE.getName()+"_"+COURSE.ID.getName()))
        );
    }

    @Override
    protected boolean isSameSection(Record cur, Record last){
        return (
                last != null &&
                isSameCourse(cur, last) &&
                cur.getValue(SECTION.getName()+"_"+SECTION.ID.getName()).equals(last.getValue(SECTION.getName()+"_"+SECTION.ID.getName()))
        );
    }

    @Override
    protected boolean isDoubleBooked(Record cur, Record last){
        return false;
    }

    @Override
    protected boolean validTimeSlot(Record record) { return true; }
}
