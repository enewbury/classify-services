package com.classify.crawler.helper;

import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.Term;
import com.classify.model.business.objects.TimeSlot;
import com.classify.model.business.objects.mappers.DataMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;


public abstract class DataNormalizer<R> {
    private static final Log log = LogFactory.getLog(DataNormalizer.class);

    private int counter;
    private List<R> records;
    private DataMapper<R> dataMapper;

    protected void setDataMapper(DataMapper<R> dataMapper){ this.dataMapper = dataMapper; }

    public List<Course> normalizeResultsToCourse(List<R> recordSet, Term term){
        //not thread safe, make sure each is "newed" or prototype
        this.records = recordSet;
        counter = 0;

        List<Course> courses = new ArrayList<>();

        Course currentCourse = null;
        Section currentSection = null;
        TimeSlot currentTimeSlot = null;

        while(counter < records.size()){
            R curRecord = getCurrent();
            R lastRecord = getLast();

            if(!isSameCourse(curRecord, lastRecord)){
                //different course, add it
                currentCourse = dataMapper.mapCourse(curRecord);
                currentCourse.setUniversityId(term.getUniversityId());
                courses.add(currentCourse);

            }

            if(!isSameSection(curRecord, lastRecord) && currentCourse!=null){
                //different section, add it
                currentSection = dataMapper.mapSection(curRecord);
                currentSection.setCourse(currentCourse);
                currentSection.setCourseId(currentCourse.getId());
                if(currentSection.getInstructor() !=null){
                    currentSection.getInstructor().setUniversityId(term.getUniversityId());
                }
                currentCourse.getSections().add(currentSection);

            }
            currentTimeSlot = mapTimeSlot(currentSection, currentTimeSlot, curRecord, lastRecord);

            counter++;
        }

        return courses;
    }

    public List<Section> normalizeResultsToSection(List<R> recordSet, Term term){
        //not thread safe, make sure each is "newed" or prototype
        this.records = recordSet;
        counter = 0;

        List<Section> sections = new ArrayList<>();

        Section currentSection = null;
        TimeSlot currentTimeSlot = null;

        while(counter < records.size()){
            R curRecord = getCurrent();
            R lastRecord = getLast();

            if(!isSameSection(curRecord, lastRecord)){
                //different section, add it
                currentSection = dataMapper.mapSection(curRecord);
                Course course = dataMapper.mapCourse(curRecord);
                course.setUniversityId(term.getUniversityId());
                currentSection.setCourse(course);
                currentSection.setCourseId(course.getId());
                if(currentSection.getInstructor() !=null){
                    currentSection.getInstructor().setUniversityId(term.getUniversityId());
                }
                sections.add(currentSection);
            }

            //add any valid, non double booked timeslot
            currentTimeSlot = mapTimeSlot(currentSection, currentTimeSlot, curRecord, lastRecord);

            counter++;
        }

        return sections;
    }

    private TimeSlot mapTimeSlot(Section currentSection, TimeSlot currentTimeSlot, R curRecord, R lastRecord) {
        //add any valid, non double booked timeslot
        if(validTimeSlot(curRecord) && !isDoubleBooked(curRecord, lastRecord) && currentSection !=null) {
            currentTimeSlot = dataMapper.mapTimeSlot(curRecord);
            if(currentTimeSlot != null) {
                currentTimeSlot.setSection(currentSection);
                currentTimeSlot.setSectionId(currentSection.getId());
                currentSection.getTimeSlots().add(currentTimeSlot);
            }
        }
        //valid, but is double booked
        else if(validTimeSlot(curRecord)){
            if (currentTimeSlot != null) {
                currentTimeSlot.setNonWeekly(true);
            }
        } else {
            log.warn("Found incomplete time slot. no op");
        }
        return currentTimeSlot;
    }

    protected abstract boolean isSameCourse(R cur, R last);

    protected abstract boolean isSameSection(R cur, R last);

    protected abstract boolean isDoubleBooked(R cur, R last);

    protected abstract boolean validTimeSlot(R record);

    private R getCurrent(){
        if(counter >= 0 && counter < records.size()){
            return records.get(counter);
        }
        else{
            return null;
        }
    }
    private R getLast(){
        if(counter > 0 && counter <= records.size()){
            return records.get(counter -1);
        }
        else{
            return null;
        }
    }



}
