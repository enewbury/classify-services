package com.classify.crawler.helper;

import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.Instructor;
import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.TimeSlot;
import com.classify.model.uvm.CheckStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import static com.classify.model.uvm.CheckStatus.*;

@Service
public class WalkActionConsultant {
    private static final Log log = LogFactory.getLog(WalkActionConsultant.class);

    public CheckStatus determineCourseAction(Course remoteCourse, Course dbCourse){
        //ASSUMPTION: At least one argument is not null
        if(remoteCourse == null && dbCourse == null){
            throw new NullPointerException("method call 'determineCourseAction' was given two null courses.");
        }

        //if end of db list, add this remote record
        if( dbCourse == null ){ return ADD; }

        //extra stuff on database. Remove it
        if (remoteCourse == null){ return DELETE; }

        //UPDATE
        if(remoteCourse.equals(dbCourse)){ return UPDATE; }
        else{
            //ADD
            if(remoteCourse.compareTo(dbCourse) < 0){ return ADD; }
            //DELETE
            else{ return DELETE; }
        }
    }

    public CheckStatus determineSectionAction(Section remoteSection, Section dbSection) {
        //ASSUMPTION: At least one argument is not null
        if(remoteSection == null && dbSection == null){
            throw new NullPointerException("method call 'determineSectionAction' was given two null sections.");
        }

        //if dbRow has no more sections add this csv section
        if(dbSection == null) return ADD;

        //extra stuff on database. Remove it
        if (remoteSection == null) return DELETE;

        //UPDATE
        if(remoteSection.equals(dbSection)) { return UPDATE; }
        else {
            //either this csv section is new, or one has been removed
            //ADD
            if(remoteSection.compareTo(dbSection) < 0) {return ADD;}
            //DELETE
            else {return DELETE;}
        }
    }

    public CheckStatus determineInstructorAction(Instructor remoteInst, Instructor dbInst) {
        //ASSUMPTION: At least one argument is not null
        if(remoteInst == null && dbInst == null){
            throw new NullPointerException("method call 'determineInstructorAction' was given two null instructors.");
        }

        //if end of db list
        if( dbInst == null) return ADD;

        //if end of remoteList
        if (remoteInst == null) return CheckStatus.DELETE;

        if (remoteInst.equals(dbInst)) return CheckStatus.UPDATE;

        return CheckStatus.ADD;
    }

    public CheckStatus determineTimeAction(TimeSlot remoteTime, TimeSlot dbTime) {
        //ASSUMPTION: At least one argument is not null
        if(remoteTime == null && dbTime == null){
            throw new NullPointerException("method call 'determineTimeAction' was given two null times.");
        }

        //no more db Times, add remote one
        if( dbTime == null) return ADD;

        //no more remote times, get rid of extra db one
        if (remoteTime == null) return DELETE;

        //UPDATE
        if(remoteTime.equals(dbTime)){ return UPDATE; }
        else{
            //either this remote section is new, or one has been removed
            //ADD
            if(remoteTime.compareTo(dbTime) < 0){ return ADD; }
            //DELETE remoteTime after, so remove extra db entry
            else{ return DELETE; }
        }
    }
}
