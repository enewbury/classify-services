package com.classify.crawler.helper;

import com.classify.model.business.objects.University;

public class UrlFactory {

    public String getRMPSchoolIdApi(University university){
        return "http://search.mtvnservices.com/typeahead/suggest/?q="+
                university.getName().toLowerCase().replaceAll(" ","+") +"&fq=content_type_s%3ASCHOOL&siteName=rmp";
    }

    public String getRMPInstructorIdApi(String firstName, String lastName, Integer rmpSchoolId){
        return "http://search.mtvnservices.com/typeahead/suggest/?q="+
                firstName.replaceAll(" ", "+") + "+" + lastName.replaceAll(" ", "+") +
                "+AND+schoolid_s%3A"+ rmpSchoolId +"&siteName=rmp";
    }

    public static String getInstructorProfileUrl(int rmpInstructorId) {
        return "http://www.ratemyprofessors.com/ShowRatings.jsp?tid="+rmpInstructorId;
    }
}
