package com.classify.crawler.rest;

import com.classify.common.PersistenceService;
import com.classify.crawler.service.CacheService;
import com.classify.crawler.service.CrawlService;
import com.classify.crawler.service.CrawlServiceFactory;
import com.classify.crawler.service.rmp.RateMyProfessorsService;
import com.classify.crawler.service.uvm.UvmCrawlService;
import com.classify.dispatcher.DispatchService;
import com.classify.model.business.objects.University;
import com.classify.model.business.objects.enums.Standing;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("manage/crawler")
public class CrawlRestService {

    private final CrawlServiceFactory crawlServiceFactory;
    private final RateMyProfessorsService rateMyProfessorsService;
    private final PersistenceService persistenceService;
    private final DispatchService dispatchService;
    private final CacheService cacheService;

    @Autowired
    public CrawlRestService(RateMyProfessorsService rateMyProfessorsService, CrawlServiceFactory crawlServiceFactory, PersistenceService persistenceService, DispatchService dispatchService, CacheService cacheService) {
        this.rateMyProfessorsService = rateMyProfessorsService;
        this.crawlServiceFactory = crawlServiceFactory;
        this.persistenceService = persistenceService;
        this.dispatchService = dispatchService;
        this.cacheService = cacheService;
    }

    @RequestMapping("/{universityId}/priority/high")
    public void getHighPriorityData(@PathVariable("universityId") String universityId){
        University university = persistenceService.getUniversityById(Integer.parseInt(universityId));
        CrawlService crawlService = crawlServiceFactory.getCrawlServiceForUniversity(university);
        crawlService.getHighPriorityData(university);
    }

    @RequestMapping("/{universityId}/priority/low")
    public void updateDailyData(@PathVariable("universityId") Integer universityId){
        University university = new University();
        university.setId(universityId);
        CrawlService crawlService = crawlServiceFactory.getCrawlServiceForUniversity(university);
        crawlService.getDailyData(university);
    }

    @RequestMapping("/resetUvmLastUpdated")
    public void resetUvmLastUpdated(){
        University university = new University();
        university.setId(University.UVM);

        UvmCrawlService crawlService = (UvmCrawlService) crawlServiceFactory.getCrawlServiceForUniversity(university);
        cacheService.clearUVMEnrollmentUpdatedCache();
    }

    @RequestMapping("/updateRMPData/{universityId}")
    public void getRMPData(@PathVariable("universityId") Integer universityId){
        University university = persistenceService.getUniversityById(universityId);

        rateMyProfessorsService.updateRMPData(university);
    }


}
