package com.classify.crawler.scheduled;

import com.classify.common.PersistenceService;
import com.classify.crawler.service.CacheService;
import com.classify.crawler.service.CrawlService;
import com.classify.crawler.service.CrawlServiceFactory;
import com.classify.crawler.service.rmp.RateMyProfessorsService;
import com.classify.model.business.objects.University;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class CrawlScheduler {
    private Log log = LogFactory.getLog(CrawlScheduler.class);

    private final PersistenceService persistenceService;
    private final CrawlServiceFactory crawlServiceFactory;
    private final RateMyProfessorsService rateMyProfessorsService;
    private final TaskExecutor taskExecutor;
    private final CacheService cacheService;

    @Autowired
    public CrawlScheduler(PersistenceService persistenceService, CrawlServiceFactory crawlServiceFactory, RateMyProfessorsService rateMyProfessorsService, TaskExecutor taskExecutor, CacheService cacheService) {
        this.persistenceService = persistenceService;
        this.crawlServiceFactory = crawlServiceFactory;
        this.rateMyProfessorsService = rateMyProfessorsService;
        this.taskExecutor = taskExecutor;
        this.cacheService = cacheService;
    }

    public void crawl(){
        persistenceService.getAllUniversities().forEach(university -> {
            //noinspection InfiniteLoopStatement
            while (true) {
                LocalDateTime inProgress = cacheService.getExecutingJob(university.getName() + "-monitor");
                if (inProgress == null || inProgress.isBefore(LocalDateTime.now().minusMinutes(5))) {
                    //not multi-instance safe
                    cacheService.setExecutingJob(university.getName() + "-monitor", LocalDateTime.now());
                    taskExecutor.execute(() -> runMonitoringOnUniversity(university));
                }

                try {
                    Thread.sleep(1000 * 10);
                } catch (InterruptedException e) {
                    log.error("Thread sleep interrupted while crawling", e);
                }
            }
        });
    }

    private void runMonitoringOnUniversity(University university){
        //get this university's crawler
        CrawlService crawlService = crawlServiceFactory.getCrawlServiceForUniversity(university);

        crawlService.getHighPriorityData(university);
    }

    //run at 1:02AM
    @Scheduled(cron = "0 2 1 * * *")
    public void updateRMPData(){
        //get list of universities.
        persistenceService.getAllUniversities().forEach(university -> {

            //not multi-instance safe
            LocalDateTime inProgress = cacheService.getExecutingJob(university.getName() + "-RMP");
            if (inProgress == null || inProgress.isBefore(LocalDateTime.now().minusMinutes(5))) {
                cacheService.setExecutingJob(university.getName() + "-RMP", LocalDateTime.now());
                taskExecutor.execute(() -> rateMyProfessorsService.updateRMPData(university));
            }

        });
    }

    //run at 4AM
    @Scheduled(cron = "0 0 4 * * *")
    public void getDailyData() throws InterruptedException {
        persistenceService.getAllUniversities().forEach(university -> {

            //not multi-instance safe
            LocalDateTime inProgress = cacheService.getExecutingJob(university.getName() + "-daily");
            if (inProgress == null || inProgress.isBefore(LocalDateTime.now().minusMinutes(5))) {
                cacheService.setExecutingJob(university.getName() + "-daily", LocalDateTime.now());
                getDailyDataForUniversity(university);
            }

        });
    }

    private void getDailyDataForUniversity(University university){
        CrawlService crawlService = crawlServiceFactory.getCrawlServiceForUniversity(university);

        taskExecutor.execute(() -> {
            log.info("Crawling " + university.toString() + " for daily data");
            crawlService.getDailyData(university);
        });
    }
}
