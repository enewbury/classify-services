package com.classify.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by Eric Newbury on 8/12/17.
 */
@ConfigurationProperties("com.classify")
public class ClassifyProperties {
    private String braintreeMerchantId = "";
    private String braintreePrivateKey = "";
    private String braintreePublicKey = "";
    private String domain = "http://localhost:3000";
    private String resourceDomain = "http://localhost:8080";
    private Boolean mockEmail = false;

    public String getBraintreeMerchantId() {
        return braintreeMerchantId;
    }

    public void setBraintreeMerchantId(String braintreeMerchantId) {
        this.braintreeMerchantId = braintreeMerchantId;
    }

    public String getBraintreePrivateKey() {
        return braintreePrivateKey;
    }

    public void setBraintreePrivateKey(String braintreePrivateKey) {
        this.braintreePrivateKey = braintreePrivateKey;
    }

    public String getBraintreePublicKey() {
        return braintreePublicKey;
    }

    public void setBraintreePublicKey(String braintreePublicKey) {
        this.braintreePublicKey = braintreePublicKey;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getResourceDomain() {
        return resourceDomain;
    }

    public void setResourceDomain(String resourceDomain) {
        this.resourceDomain = resourceDomain;
    }

    public Boolean getMockEmail() {
        return mockEmail;
    }

    public void setMockEmail(Boolean mockEmail) {
        this.mockEmail = mockEmail;
    }
}
