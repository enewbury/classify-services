package com.classify.config.auth;

import com.classify.exceptions.ErrorType;
import com.classify.exceptions.ExceptionLevel;
import com.classify.model.rest.AuthenticationErrorRestResponse;
import com.classify.model.rest.ErrorRestResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.entity.ContentType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class RestAuthFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) throws IOException, ServletException {
        response.setContentType(ContentType.APPLICATION_JSON.toString());
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        PrintWriter writer = response.getWriter();
        mapper.writeValue(writer, new AuthenticationErrorRestResponse(exception.getMessage(), exception.getClass().getSimpleName()));
        writer.flush();
    }
}