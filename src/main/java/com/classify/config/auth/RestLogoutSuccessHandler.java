package com.classify.config.auth;

import com.classify.model.rest.EmptyRestResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.entity.ContentType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Eric Newbury on 8/10/17.
 */
@Component
public class RestLogoutSuccessHandler implements LogoutSuccessHandler {
    private ObjectMapper mapper = new ObjectMapper();
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setContentType(ContentType.APPLICATION_JSON.toString());
        response.setStatus(HttpServletResponse.SC_ACCEPTED);

        PrintWriter writer = response.getWriter();
        mapper.writeValue(writer, new EmptyRestResponse());
        writer.flush();
    }
}
