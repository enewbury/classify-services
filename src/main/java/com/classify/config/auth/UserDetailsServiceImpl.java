package com.classify.config.auth;

import com.classify.api.service.persistence.UserPersistenceService;
import com.classify.model.business.objects.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Eric Newbury on 8/9/17.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserPersistenceService userPersistenceService;

    @Autowired
    public UserDetailsServiceImpl(UserPersistenceService userPersistenceService) {
        this.userPersistenceService = userPersistenceService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userPersistenceService.findByEmail(username);

        if(user == null){
            throw new UsernameNotFoundException("Could not find user: " + username);
        }

        return new CustomUserDetails(user);
    }
}
