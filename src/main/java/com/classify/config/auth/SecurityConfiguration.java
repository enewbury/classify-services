package com.classify.config.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsUtils;

/**
 * Created by Eric Newbury on 8/9/17.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserDetailsServiceImpl userDetailsService;
    private final RestAuthenticationEntryPoint restAuthenticationEntryPoint;
    private final RestAuthSuccessHandler restAuthSuccessHandler;
    private final RestAuthFailureHandler restAuthFailureHandler;
    private final RestLogoutSuccessHandler restLogoutSuccessHandler;

    @Value("${security.require-ssl:false}")
    private boolean requireSsl;

    @Autowired
    public SecurityConfiguration(RestAuthenticationEntryPoint restAuthenticationEntryPoint, RestAuthSuccessHandler restAuthSuccessHandler, RestAuthFailureHandler restAuthFailureHandler, UserDetailsServiceImpl userDetailsService, RestLogoutSuccessHandler restLogoutSuccessHandler) {
        this.restAuthenticationEntryPoint = restAuthenticationEntryPoint;
        this.restAuthSuccessHandler = restAuthSuccessHandler;
        this.restAuthFailureHandler = restAuthFailureHandler;
        this.userDetailsService = userDetailsService;
        this.restLogoutSuccessHandler = restLogoutSuccessHandler;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http = (this.requireSsl) ? http.requiresChannel().mvcMatchers("/_ah/health").requiresInsecure().anyRequest().requiresSecure().and() : http;
        http
            .cors().and()

            .csrf()
                .disable()
            .exceptionHandling()
                .authenticationEntryPoint(restAuthenticationEntryPoint).and()
            .authorizeRequests()
                .requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
                .mvcMatchers(HttpMethod.POST,"/api/v1/user", "/api/v1/user/token").permitAll()
                .mvcMatchers(HttpMethod.PATCH, "/api/v1/user/token").permitAll()
                .mvcMatchers(HttpMethod.OPTIONS, "/api/v1/user/token").permitAll()
                .mvcMatchers(HttpMethod.POST,"/api/v1/session").permitAll()
                .mvcMatchers(HttpMethod.GET, "/api/v1/payments/braintreeToken").permitAll()
                .mvcMatchers(HttpMethod.POST, "/api/v1/payments/donation").permitAll()
                .mvcMatchers("/api/**").authenticated()
                .mvcMatchers("/**").permitAll().and()
            .formLogin().permitAll()
                .usernameParameter("username")
                .passwordParameter("password")
                .loginProcessingUrl("/api/v1/session")
                .successHandler(restAuthSuccessHandler)
                .failureHandler(restAuthFailureHandler).and()
            .logout().permitAll()
                .logoutRequestMatcher(new AntPathRequestMatcher("/api/v1/session", HttpMethod.DELETE.name()))
                .logoutSuccessHandler(restLogoutSuccessHandler).and()
            .rememberMe();
        // @formatter:on
    }
}
