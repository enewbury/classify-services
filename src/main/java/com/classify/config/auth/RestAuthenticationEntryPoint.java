package com.classify.config.auth;

import com.classify.exceptions.ErrorType;
import com.classify.exceptions.ExceptionLevel;
import com.classify.model.rest.ErrorRestResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.entity.ContentType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Eric Newbury on 8/9/17.
 */
@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletResponse.setContentType(ContentType.APPLICATION_JSON.toString());
        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        PrintWriter writer = httpServletResponse.getWriter();
        mapper.writeValue(writer, new ErrorRestResponse(ExceptionLevel.WARN, ErrorType.NOT_LOGGED_IN, e.getMessage(), e.getMessage()));
    }
}
