package com.classify.dispatcher;

import com.classify.common.MailService;
import com.classify.crawler.service.CacheService;
import com.classify.crawler.service.persistence.CrawlPersistenceService;
import com.classify.exceptions.BrokenIntegrationException;
import com.classify.model.SectionUpdates;
import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.User;
import com.classify.model.business.objects.WatchlistEntry;
import com.classify.model.EnrollmentChanges;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DispatchService {

    private Log log = LogFactory.getLog(CrawlPersistenceService.class);

    private final DispatchPersistenceService dispatchPersistenceService;
    private final JmsTemplate jmsTemplate;
    private final MailService mailService;
    private final CacheService cacheService;

    @Autowired
    public DispatchService(DispatchPersistenceService dispatchPersistenceService, JmsTemplate jmsTemplate, MailService mailService, CacheService cacheService) {
        this.dispatchPersistenceService = dispatchPersistenceService;
        this.jmsTemplate = jmsTemplate;
        this.mailService = mailService;
        this.cacheService = cacheService;
    }

    public void handleBrokenIntegration(BrokenIntegrationException brokenIntegration){
        LocalDateTime notified = cacheService.getBrokenIntegration(brokenIntegration.getLocation());

        if(shouldSendBrokenIntegrationNotification(notified)) {
            mailService.sendBrokenIntegrationNotification(brokenIntegration);
            cacheService.setBrokenIntegration(brokenIntegration.getLocation(), LocalDateTime.now());
        }
    }

    private boolean shouldSendBrokenIntegrationNotification(LocalDateTime notified){
        return notified == null || notified.isBefore(LocalDateTime.now().minusDays(1));
    }

    public void handleSectionOpenings(SectionUpdates sectionUpdates){
        log.info("Grouping changes by user");

        //filter to list of newly opened sections
        Map<Integer, Section> newlyOpenSections = sectionUpdates.getUpdatedSections().stream()
                .filter(s -> {
                    Section previous = sectionUpdates.getPreviousValues().get(s.getId());
                    return (
                            //is open now
                            (s.getSeats() - s.getSeatsFilled()) > 0 &&
                            //was closed before
                            (previous.getSeats() - previous.getSeatsFilled() <= 0)
                    );
                }).collect(Collectors.toMap(Section::getId, s -> s));

        //filter to list of newly closed sections
        Map<Integer, Section> newlyClosedSections = sectionUpdates.getUpdatedSections().stream()
                .filter(s -> {
                    Section previous = sectionUpdates.getPreviousValues().get(s.getId());

                    return (
                            //filled up
                            (s.getSeats() - s.getSeatsFilled()) <= 0 &&
                            //wasn't filled up before
                            (previous.getSeats() - previous.getSeatsFilled() > 0)
                    );

                }).collect(Collectors.toMap(Section::getId, s -> s));

        //get any watchlist entries for open sections
        List<WatchlistEntry> entries = dispatchPersistenceService.getWatchListEntriesForOpenings(
                sectionUpdates.getTerm().getId(),
                Stream.concat(
                        newlyOpenSections.keySet().stream(),
                        newlyClosedSections.keySet().stream()
                ).collect(Collectors.toList())
        );

        Map<Integer, UserSectionChanges> sectionsPerUser = mapUsersToChanges(newlyOpenSections, newlyClosedSections, entries);

        for(Integer userId: sectionsPerUser.keySet()){
            UserSectionChanges changes = sectionsPerUser.get(userId);
            User user = dispatchPersistenceService.getUser(userId);
            mailService.sendEnrollmentChangeNotification(user, changes.openings, changes.closings);
        }

    }

    private Map<Integer, UserSectionChanges> mapUsersToChanges(Map<Integer, Section> newlyOpenSections, Map<Integer, Section> newlyClosedSections, List<WatchlistEntry> entries) {
        Map<Integer, UserSectionChanges> sectionsPerUser = new HashMap<>(entries.size());

        for(WatchlistEntry entry: entries){
            //initialize any new user
            if(sectionsPerUser.get(entry.getUserId()) == null) {
                sectionsPerUser.put(entry.getUserId(), new UserSectionChanges());
            }

            //add as opening if it is there
            if(newlyOpenSections.get(entry.getSectionId()) != null) {
                sectionsPerUser.get(entry.getUserId()).openings.add(newlyOpenSections.get(entry.getSectionId()));
            }

            //add as closing if it is ther
            if(newlyClosedSections.get(entry.getSectionId()) !=null) {
                sectionsPerUser.get(entry.getUserId()).closings.add(newlyClosedSections.get(entry.getSectionId()));
            }
        }

        return sectionsPerUser;
    }

    private static class UserSectionChanges {
        public List<Section> openings = new ArrayList<>();
        public List<Section> closings = new ArrayList<>();
    }

}
