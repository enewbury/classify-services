package com.classify.dispatcher;

public enum RegistrationStatus {
    QUEUED, PROCESSING, REGISTERED, FAILED
}
