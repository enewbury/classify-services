package com.classify.dispatcher;

import com.classify.model.business.objects.User;
import com.classify.model.business.objects.WatchlistEntry;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.jooq.Tables.USER;
import static org.jooq.Tables.WATCHLIST_ENTRY;

/**
 * Created by Eric Newbury on 8/21/17.
 */
@Service
public class DispatchPersistenceService {

    private final DSLContext db;

    @Autowired
    public DispatchPersistenceService(DSLContext db) {
        this.db = db;
    }

    List<WatchlistEntry> getWatchListEntriesForOpenings(Integer termId, List<Integer> sectionIds){
        return db.selectFrom(WATCHLIST_ENTRY)
                .where(WATCHLIST_ENTRY.SECTION_ID.in(sectionIds))
                .and(WATCHLIST_ENTRY.TERM_ID.eq(termId))
                .fetchInto(WatchlistEntry.class);
    }

    User getUser(Integer userId) {
        return db.selectFrom(USER).where(USER.ID.eq(userId)).fetchOneInto(User.class);
    }
}
