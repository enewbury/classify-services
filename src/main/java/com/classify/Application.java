package com.classify;


import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Environment;
import com.classify.config.ClassifyProperties;
import com.classify.crawler.scheduled.CrawlScheduler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.jms.support.destination.DynamicDestinationResolver;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.jms.ConnectionFactory;
import javax.jms.Session;

@Configuration
@EnableAutoConfiguration
@ComponentScan
@EnableScheduling
@EnableJms
@EnableConfigurationProperties(ClassifyProperties.class)
public class Application {

    @Value("${com.classify.braintree-merchant-id}")
    private String braintreeMerchantId;

    @Value("${com.classify.braintree-public-key}")
    private String braintreePublicKey;

    @Value("${com.classify.braintree-private-key}")
    private String braintreePrivateKey;

    public static void main(String[] args) {
        //allow connections to ssl urls without verifying
        System.setProperty("jsse.enableSNIExtension", "false");
        ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);

        //Start crawling
        CrawlScheduler scheduler = ctx.getBean(CrawlScheduler.class);
        scheduler.crawl();
    }

    @Profile("aws")
    @Bean
    public ConnectionFactory awsConnectionFactory(){
        return new SQSConnectionFactory(
                new ProviderConfiguration(),
                AmazonSQSClientBuilder.defaultClient()
        );
    }

    @Profile("aws")
    @Bean
    public AmazonSimpleEmailService simpleEmailService(){
        return AmazonSimpleEmailServiceClientBuilder.defaultClient();
    }

    @Bean
    public JmsListenerContainerFactory<?> jmsListenerContainerFactory(ConnectionFactory connectionFactory,
                                                                      DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        factory.setDestinationResolver(new DynamicDestinationResolver());
        factory.setSessionTransacted(false);
        factory.setConcurrency("1-5");
        factory.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
        return factory;
    }

    @Bean // Serialize message content to json using TextMessage
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("http://localhost:3000", "https://classifyregistration.com")
                        .allowedMethods("GET","POST","PUT","PATCH","DELETE", "OPTIONS");
            }
        };
    }

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(10);
        return executor;
    }

    @Profile("dev")
    @Bean
    public BraintreeGateway braintreeDevGateway() {
        return new BraintreeGateway(
                Environment.SANDBOX,
                braintreeMerchantId,
                braintreePublicKey,
                braintreePrivateKey
        );
    }

    @Profile("aws")
    @Bean
    public BraintreeGateway braintreeAwsGateway() {
        return new BraintreeGateway(
                Environment.PRODUCTION,
                braintreeMerchantId,
                braintreePublicKey,
                braintreePrivateKey
        );
    }
}
