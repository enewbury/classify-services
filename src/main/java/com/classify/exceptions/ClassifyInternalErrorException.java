package com.classify.exceptions;

/**
 * Created by Eric Newbury on 8/14/17.
 */
public class ClassifyInternalErrorException extends Exception{
    private ErrorType error;
    private ExceptionLevel exceptionLevel;

    public ClassifyInternalErrorException(ErrorType error, ExceptionLevel exceptionLevel, String message, Throwable cause) {
        super(message, cause);
        this.error = error;
        this.exceptionLevel = exceptionLevel;
    }

    public ErrorType getError() {
        return error;
    }

    public void setError(ErrorType error) {
        this.error = error;
    }

    public ExceptionLevel getExceptionLevel() {
        return exceptionLevel;
    }

    public void setExceptionLevel(ExceptionLevel exceptionLevel) {
        this.exceptionLevel = exceptionLevel;
    }
}
