package com.classify.exceptions;

public class BrokenIntegrationException extends Exception {
    String location;

    public BrokenIntegrationException(String location) {
        super();
        this.location=location;
    }

    public BrokenIntegrationException(String location, String message) {
        super(message);
        this.location=location;
    }

    public BrokenIntegrationException(String location, Throwable cause) {
        super(cause);
        this.location=location;
    }

    public BrokenIntegrationException( String location, String message, Throwable cause){
        super(message, cause);
        this.location=location;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
