package com.classify.exceptions;

/**
 * Created by Eric Newbury on 8/2/17.
 */
public enum ExceptionLevel {INFO, WARN, ERROR}
