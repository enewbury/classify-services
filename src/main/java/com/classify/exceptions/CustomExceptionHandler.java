package com.classify.exceptions;

import com.classify.model.rest.ErrorRestResponse;
import com.classify.model.rest.ValidationErrorRestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;

/**
 * Created by Eric Newbury on 8/2/17.
 */
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
    private Log log = LogFactory.getLog(CustomExceptionHandler.class);

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ErrorRestResponse(ExceptionLevel.ERROR, ErrorType.NOT_FOUND, "The server couldn't find the droids you are looking for.", ex.getMessage())
        );
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ArrayList<ValidationErrorRestResponse.Violation> violations = new ArrayList<>();
        for (FieldError er : exception.getBindingResult().getFieldErrors()) {
            violations.add(new ValidationErrorRestResponse.Violation(er.getField(), er.getDefaultMessage()));
        }

        String firstError = (violations.size() > 0) ? violations.get(0).getViolation() : "Unknown validation error";

        ValidationErrorRestResponse restResponse = new ValidationErrorRestResponse(firstError, violations);

        return ResponseEntity.badRequest().body(restResponse);

    }

    @ExceptionHandler(value = ClassifyClientErrorException.class)
    public final ResponseEntity<ErrorRestResponse> handleClientError(ClassifyClientErrorException e){
        return ResponseEntity.status(e.getStatus()).body(
                new ErrorRestResponse(
                        ExceptionLevel.WARN,
                        e.getError(),
                        e.getMessage(),
                        e.getMessage()
                )
        );
    }

    @ExceptionHandler(value = ClassifyInternalErrorException.class)
    public final ResponseEntity<ErrorRestResponse> handleClientError(ClassifyInternalErrorException e){
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                new ErrorRestResponse(
                        e.getExceptionLevel(),
                        e.getError(),
                        e.getMessage(),
                        e.getCause().getMessage()
                )
        );
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public final ResponseEntity<ErrorRestResponse> handleAccessDeniedException(AccessDeniedException e){
        ErrorRestResponse restResponse = new ErrorRestResponse(ExceptionLevel.WARN, ErrorType.INSUFICIENT_AUTHORIZATION, "You don't have permission to mess with that! You sneaky bugger you...", e.getClass().getSimpleName());
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(restResponse);
    }

    @ExceptionHandler(value = {Exception.class, RuntimeException.class, Throwable.class})
    public final ResponseEntity<ErrorRestResponse> handleException(Throwable e) {
        log.error("Exception in rest call", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                new ErrorRestResponse(
                        ExceptionLevel.ERROR,
                        ErrorType.UNKNOWN,
                        "Eegats! The server experienced an error!",
                        e.getMessage()
                )
        );
    }


}