package com.classify.exceptions;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by Eric Newbury on 8/14/17.
 */
public class ClassifyClientErrorException extends Exception{
    private ErrorType error;
    private int status;

    public ClassifyClientErrorException(ErrorType error, int status, String message) {
        super(message);
        this.error = error;
        this.status = status;
    }

    public ErrorType getError() {
        return error;
    }

    public void setError(ErrorType error) {
        this.error = error;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
