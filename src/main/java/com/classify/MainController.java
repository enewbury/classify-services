package com.classify;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Eric Newbury on 9/24/17.
 */
@Controller
public class MainController extends AbstractErrorController {
    private static final String PATH = "/error";

    @Autowired
    public MainController(ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }

    @RequestMapping(value = PATH, produces = "text/html")
    public String error(HttpServletResponse response) {
        response.setStatus(HttpStatus.OK.value());
        return "index.html";
    }

    @RequestMapping(value = PATH)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
        Map<String, Object> body = this.getErrorAttributes(request, false);
        HttpStatus status = this.getStatus(request);
        return ResponseEntity.status(status).body(body);
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
