package com.classify.crawler.service.uvm;

import com.classify.crawler.helper.UpdateChecker;
import com.classify.crawler.helper.WalkActionConsultant;
import com.classify.model.business.objects.*;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class UvmDataWalkerTest {

    private UvmDataWalker dataWalker;
    private UvmCrawlPersistenceService persistenceService;
    private University university;

    @Before
    public void setup(){
        persistenceService = mock(UvmCrawlPersistenceService.class);
        dataWalker = new UvmDataWalker(persistenceService, new UpdateChecker(), new WalkActionConsultant());
        University university = new University();
        university.setId(1);
        this.university = university;
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldAddWhenDbIsEmpty(){
        Course course = new Course();
        course.setSubject("BCOR");
        course.setCourseNumber("101");
        course.setUniversityId(1);
        course.setCreditsMin(3d);

        Section section = new Section();
        section.setUniversitySectionId(123);
        section.setCourse(course);

        TimeSlot time = new TimeSlot();
        time.setStartTime(LocalTime.parse("10:30:00"));
        time.setEndTime(LocalTime.parse("11:30:00"));
        time.setDays("2,4");
        time.setSection(section);

        Instructor instructor = new Instructor();
        instructor.setUniversityUsername("enewbury");
        instructor.setEmail("enewbury@uvm.edu");

        section.setTimeSlots(Lists.newArrayList(time));
        section.setInstructor(instructor);
        course.setSections(Lists.newArrayList(section));

        Term term = new Term();
        term.setTermCode("201501");
        term.setId(1);

        dataWalker.walk(
                Lists.newArrayList(course),
                new ArrayList<>(),
                university,
                term);


        //course
        ArgumentCaptor<List> courseInsertsArgument = ArgumentCaptor.forClass(List.class);
        verify(persistenceService).batchInsertCourses(courseInsertsArgument.capture());
        List insertedCourses = courseInsertsArgument.getValue();
        assertEquals(1, insertedCourses.size());

        //section
        ArgumentCaptor<List> sectionInsertsArgument = ArgumentCaptor.forClass(List.class);
        verify(persistenceService).batchInsertCourses(sectionInsertsArgument.capture());
        List insertedSections = sectionInsertsArgument.getValue();
        assertEquals(1, insertedSections.size());

        //time slot
        ArgumentCaptor<List> timeInsertArgument = ArgumentCaptor.forClass(List.class);
        verify(persistenceService).batchInsertCourses(timeInsertArgument.capture());
        List insertedTimes = timeInsertArgument.getValue();
        assertEquals(1, insertedTimes.size());

        //instructor
        ArgumentCaptor<List> instructorInsertArgument = ArgumentCaptor.forClass(List.class);
        verify(persistenceService).batchInsertCourses(instructorInsertArgument.capture());
        List insertedInsts = instructorInsertArgument.getValue();
        assertEquals(1, insertedInsts.size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldAddCourseWhenRemoteComesBefore(){
        Course remoteCourse = new Course();
        remoteCourse.setSubject("CALS");
        remoteCourse.setCourseNumber("101");

        Course dbCourse = new Course();
        dbCourse.setSubject("CS");
        dbCourse.setCourseNumber("101");
        dbCourse.setTermCode("201501");

        Term term = new Term();
        term.setTermCode("201501");
        term.setId(1);

        dataWalker.walk(
                Lists.newArrayList(remoteCourse),
                Lists.newArrayList(dbCourse),
                university,
                term);

        ArgumentCaptor<List> courseInsertsArgument = ArgumentCaptor.forClass(List.class);
        verify(persistenceService).batchInsertCourses(courseInsertsArgument.capture());
        List<Course> insertedCourses = courseInsertsArgument.getValue();
        assertEquals(1, insertedCourses.size());
        assertEquals("CALS", insertedCourses.get(0).getSubject());
        assertEquals("101", insertedCourses.get(0).getCourseNumber());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldAddSectionWhenRemoteComesBefore(){
        Course remoteCourse = new Course(); remoteCourse.setSubject("Cals"); remoteCourse.setCourseNumber("1");
        Section remoteSection = new Section();
        remoteSection.setCourse(remoteCourse);
        remoteCourse.setSections(Lists.newArrayList(remoteSection));
        remoteSection.setUniversitySectionId(2);
        remoteSection.setSectionName("B");

        Course dbCourse = new Course(); dbCourse.setSubject("Cals"); dbCourse.setCourseNumber("1"); dbCourse.setTermCode("201501");
        Section dbSection = new Section();
        dbSection.setCourse(dbCourse);
        dbCourse.setSections(Lists.newArrayList(dbSection));
        dbSection.setUniversitySectionId(35);
        dbSection.setSectionName("C");

        Term term = new Term();
        term.setTermCode("201501");
        term.setId(1);
        dataWalker.walk(
                Lists.newArrayList(remoteCourse),
                Lists.newArrayList(dbCourse),
                university,
                term);

        ArgumentCaptor<List> sectionInsertsArgument = ArgumentCaptor.forClass(List.class);
        verify(persistenceService).batchInsertSections(sectionInsertsArgument.capture(), anyMap(), anyMap(), anyInt());
        List<Section> insertedSections = sectionInsertsArgument.getValue();
        assertEquals(1, insertedSections.size());
        assertEquals(new Integer(2), insertedSections.get(0).getUniversitySectionId());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldAddTimeWhenRemoteComesBefore(){
        Course remoteCourse = new Course(); remoteCourse.setSubject("Cals"); remoteCourse.setCourseNumber("1");
        Section remoteSection = new Section();
        remoteSection.setCourse(remoteCourse);
        remoteCourse.setSections(Lists.newArrayList(remoteSection));
        remoteSection.setUniversitySectionId(2);
        remoteSection.setSectionName("B");
        TimeSlot remoteTime = new TimeSlot();
        remoteTime.setSection(remoteSection);
        remoteTime.setStartTime(LocalTime.parse("01:20:00"));
        remoteTime.setEndTime(LocalTime.parse("02:20:00"));
        remoteTime.setDays("2,4");
        remoteSection.setTimeSlots(Lists.newArrayList(remoteTime));

        Course dbCourse = new Course(); dbCourse.setSubject("Cals"); dbCourse.setCourseNumber("1"); dbCourse.setTermCode("201501");
        Section dbSection = new Section();
        dbSection.setCourse(dbCourse);
        dbCourse.setSections(Lists.newArrayList(dbSection));
        dbSection.setUniversitySectionId(35);
        dbSection.setSectionName("C");
        TimeSlot dbTime = new TimeSlot();
        dbTime.setSection(dbSection);
        dbTime.setStartTime(LocalTime.parse("01:40:00")); //later start time
        dbTime.setEndTime(LocalTime.parse("02:20:00"));
        dbTime.setDays("2,4");
        dbSection.setTimeSlots(Lists.newArrayList(dbTime));

        Term term = new Term();
        term.setTermCode("201501");
        term.setId(1);

        dataWalker.walk(
                Lists.newArrayList(remoteCourse),
                Lists.newArrayList(dbCourse),
                university,
                term);

        ArgumentCaptor<List> timeInsertsArgument = ArgumentCaptor.forClass(List.class);
        verify(persistenceService).batchInsertTimes(timeInsertsArgument.capture(), anyMap());
        List<TimeSlot> insertedTimes = timeInsertsArgument.getValue();
        assertEquals(1, insertedTimes.size());
        assertEquals(LocalTime.parse("01:20:00"), insertedTimes.get(0).getStartTime());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldAddInstructorIfNoMatchFound(){
        Course remoteCourse = new Course(); remoteCourse.setSubject("Cals"); remoteCourse.setCourseNumber("1");
        Section remoteSection = new Section();
        remoteSection.setCourse(remoteCourse);
        remoteCourse.setSections(Lists.newArrayList(remoteSection));
        remoteSection.setUniversitySectionId(2);
        remoteSection.setSectionName("B");
        Instructor inst = new Instructor();
        inst.setUniversityUsername("username");
        inst.setFirstName("First");
        inst.setLastName("Last");
        inst.setEmail("email@email.com");
        remoteSection.setInstructor(inst);

        Term term = new Term();
        term.setTermCode("201501");
        term.setId(1);

        dataWalker.walk(
                Lists.newArrayList(remoteCourse),
                Lists.newArrayList(),
                university,
                term);

        ArgumentCaptor<List> instructorInsertArgument = ArgumentCaptor.forClass(List.class);
        verify(persistenceService).batchInsertCourses(instructorInsertArgument.capture());
        List<Course> insertedInstructors = instructorInsertArgument.getValue();
        assertEquals(1, insertedInstructors.size());
    }


    @Test
    @SuppressWarnings("unchecked")
    public void shouldAddNewInstructorWhenDbIsNullButRemoteHasInstructor(){
        Instructor oldInstructor = null;
        Instructor newInstructor = new Instructor();
        newInstructor.setUniversityUsername("ericn");

        Course remoteCourse = new Course();
        remoteCourse.setSubject("CALS"); remoteCourse.setCourseNumber("145");
        Section remoteSection = new Section();
        remoteSection.setSectionName("A");
        remoteSection.setUniversitySectionId(1);
        remoteSection.setInstructor(newInstructor);
        remoteSection.setCourse(remoteCourse);
        remoteCourse.setSections(Lists.newArrayList(remoteSection));

        Course dbCourse = new Course();
        dbCourse.setSubject("CALS");
        dbCourse.setCourseNumber("145");
        dbCourse.setTermCode("201501");
        Section dbSection = new Section();
        dbSection.setSectionName("A");
        dbSection.setUniversitySectionId(1);
        dbSection.setInstructor(oldInstructor);
        dbSection.setCourse(dbCourse);
        dbCourse.setSections(Lists.newArrayList(dbSection));

        Term term = new Term();
        term.setTermCode("201501");
        term.setId(1);

        ArgumentCaptor<List> instructorInsertArgument = ArgumentCaptor.forClass(List.class);
        dataWalker.walk(
                Lists.newArrayList(remoteCourse),
                Lists.newArrayList(dbCourse),
                university,
                term);

        //verify that new instructor was in fact inserted

        verify(persistenceService).getInstructorsNeedingInsert(instructorInsertArgument.capture());
        assertEquals(1, instructorInsertArgument.getValue().size());
    }

}
