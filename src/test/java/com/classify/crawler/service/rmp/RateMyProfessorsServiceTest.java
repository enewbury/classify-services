package com.classify.crawler.service.rmp;

import com.classify.common.MailService;
import com.classify.common.PersistenceService;
import com.classify.dispatcher.DispatchService;
import com.classify.model.business.objects.University;
import com.google.common.collect.Lists;
import org.jooq.tables.records.JInstructorRecord;
import org.jooq.types.UInteger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class RateMyProfessorsServiceTest {

    private PersistenceService persistenceService;
    private RateMyProfessorsService rateMyProfessorsService;
    private University university;

    @Before
    public void setup(){
        University uvm = new University();
        uvm.setId(1);
        uvm.setAcronym("UVM");
        uvm.setName("University of Vermont");
        university = uvm;

        JInstructorRecord instructor = new JInstructorRecord();
        instructor.setFirstName("Bob");
        instructor.setLastName("Erickson");

        persistenceService = mock(PersistenceService.class);
        when(persistenceService.getInstructorsForUniversity(anyInt())).thenReturn(Lists.newArrayList(instructor));

        rateMyProfessorsService = new RateMyProfessorsService(mock(DispatchService.class), persistenceService);

    }

    //Integration Test
//    @Test
    public void shouldGetSchoolIdFromApi(){
        rateMyProfessorsService.updateRMPData(university);

        ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);
        verify(persistenceService).updateRMPIdForUniversity(eq(1), argumentCaptor.capture());

        System.out.println(argumentCaptor.getValue());
        assertEquals(new Integer(1320), argumentCaptor.getValue());
    }

    //Integration Test
//    @Test
    public void shouldNotFindUniversity(){
        university.setName("University of Gibberish");
        rateMyProfessorsService.updateRMPData(university);

        verify(persistenceService, never()).updateRMPIdForUniversity(anyInt(), anyInt());
    }

    //IntegrationTest
//    @Test
    public void shouldGetInstructorIdFromApi(){
        university.setName("University of Vermont");
        rateMyProfessorsService.updateRMPData(university);

        ArgumentCaptor<JInstructorRecord> argumentCaptor = ArgumentCaptor.forClass(JInstructorRecord.class);
        verify(persistenceService).updateInstructorRMPData(argumentCaptor.capture());

        assertEquals(new Integer(70306), argumentCaptor.getValue().getRmpId());
    }

    //IntegrationTest
//    @Test
    public void shouldFindNoInstructorId(){
        JInstructorRecord instructor = new JInstructorRecord();
        instructor.setFirstName("George");
        instructor.setLastName("Cloony");

        when(persistenceService.getInstructorsForUniversity(anyInt())).thenReturn(Lists.newArrayList(instructor));

        rateMyProfessorsService.updateRMPData(university);
        verify(persistenceService, never()).updateInstructorRMPData(any());
    }

    //IntegrationTest
//    @Test
    public void shouldGetCorrectRatingAndBlurbForErickson(){
        JInstructorRecord instructor = new JInstructorRecord();
        instructor.setId(70306);
        instructor.setFirstName("Bob");
        instructor.setLastName("Erickson");
        university.setRmpId(1320);

        when(persistenceService.getInstructorsForUniversity(anyInt())).thenReturn(Lists.newArrayList(instructor));

        rateMyProfessorsService.updateRMPData(university);
        ArgumentCaptor<JInstructorRecord> argCaptor = ArgumentCaptor.forClass(JInstructorRecord.class);
        verify(persistenceService).updateInstructorRMPData(argCaptor.capture());

        assertEquals(new Double(3.1), argCaptor.getValue().getRmpRating());
        assertEquals("Incredibly annoying, rude, and awkward. Avoid at all costs.",argCaptor.getValue().getRmpLatestComment());
    }

}
