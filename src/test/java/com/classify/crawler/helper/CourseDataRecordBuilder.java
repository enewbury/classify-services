package com.classify.crawler.helper;

import com.classify.model.business.objects.enums.SectionType;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.tables.*;
import org.jooq.tools.jdbc.MockConnection;
import org.jooq.tools.jdbc.MockDataProvider;

import java.time.LocalTime;
import java.util.Iterator;

import static org.jooq.Tables.*;

/**
 * Created by Eric Newbury on 8/1/17.
 */
public class CourseDataRecordBuilder {

    private MockDataProvider provider = new MockJooqDataProvider();
    private MockConnection connection = new MockConnection(provider);

    // Pass the mock connection to a jOOQ DSLContext:
    private DSLContext db = DSL.using(connection, SQLDialect.MYSQL);

    private Integer courseId;
    private Integer courseVersionId;
    private String courseVersionName;
    private String subject;
    private String courseNumber;
    private Integer universityId;
    private String termCode;
    private Integer sectionId;
    private Integer universitySectionId;
    private String sectionName;
    private SectionType sectionType;
    private Integer seats;
    private Integer seatsFilled;
    private Integer instructorId;
    private String firstName;
    private String lastName;
    private String universityUsername;
    private String email;
    private Integer syncedTerm;
    private Integer timeSlotId;
    private LocalTime startTime;
    private LocalTime endTime;
    private String days;
    private String building;
    private String room;
    private boolean nonWeekly;

    public CourseDataRecordBuilder setCourseId(int courseId) {
        this.courseId = courseId;
        return this;
    }

    public CourseDataRecordBuilder setCourseVersionId(int courseVersionId) {
        this.courseVersionId = courseVersionId;
        return this;
    }

    public CourseDataRecordBuilder setCourseVersionName(String courseVersionName) {
        this.courseVersionName = courseVersionName;
        return this;
    }

    public CourseDataRecordBuilder setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public CourseDataRecordBuilder setUniversityId(Integer id){
        this.universityId = id;
        return this;
    }

    public CourseDataRecordBuilder setCourseNumber(String courseNumber) {
        this.courseNumber = courseNumber;
        return this;
    }

    public CourseDataRecordBuilder setTermCode(String termCode) {
        this.termCode = termCode;
        return this;
    }

    public CourseDataRecordBuilder setSectionId(int sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public CourseDataRecordBuilder setUniversitySectionId(int universitySectionId) {
        this.universitySectionId = universitySectionId;
        return this;
    }

    public CourseDataRecordBuilder setSectionName(String sectionName) {
        this.sectionName = sectionName;
        return this;
    }

    public CourseDataRecordBuilder setSectionType(SectionType sectionType) {
        this.sectionType = sectionType;
        return this;
    }

    public CourseDataRecordBuilder setSeats(int seats) {
        this.seats = seats;
        return this;
    }

    public CourseDataRecordBuilder setSeatsFilled(int seatsFilled) {
        this.seatsFilled = seatsFilled;
        return this;
    }

    public CourseDataRecordBuilder setInstructorId(int instructorId) {
        this.instructorId = instructorId;
        return this;
    }

    public CourseDataRecordBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public CourseDataRecordBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public CourseDataRecordBuilder setUniversityUsername(String universityUsername) {
        this.universityUsername = universityUsername;
        return this;
    }

    public CourseDataRecordBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public CourseDataRecordBuilder setSyncedTerm(int syncedTerm) {
        this.syncedTerm = syncedTerm;
        return this;
    }

    public CourseDataRecordBuilder setTimeSlotId(Integer timeSlotId) {
        this.timeSlotId = timeSlotId;
        return this;
    }

    public CourseDataRecordBuilder setStartTime(LocalTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public CourseDataRecordBuilder setEndTime(LocalTime endTime) {
        this.endTime = endTime;
        return this;
    }

    public CourseDataRecordBuilder setDays(String days) {
        this.days = days;
        return this;
    }

    public CourseDataRecordBuilder setBuilding(String building) {
        this.building = building;
        return this;
    }

    public CourseDataRecordBuilder setRoom(String room) {
        this.room = room;
        return this;
    }

    public CourseDataRecordBuilder setNonWeekly(Boolean nonWeekly) {
        this.nonWeekly = nonWeekly;
        return this;
    }

    public Record build(){
        JCourse c = COURSE.as("c");
        JCourseVersion v = COURSE_VERSION.as("v");
        JSection s = SECTION.as("s");
        JTimeSlot t = TIME_SLOT.as("t");
        JInstructor i = INSTRUCTOR.as("i");

        Record r1 = db.newRecord(c.ID.as(COURSE.getName() + "_" + COURSE.ID.getName()), c.SUBJECT, c.COURSE_NUMBER, c.UNIVERSITY_ID,
                v.ID.as(COURSE_VERSION.getName() + "_" + COURSE_VERSION.ID.getName()), v.NAME, v.CREDITS_MIN, v.CREDITS_MAX, v.DESCRIPTION, v.TERM_CODE,
                s.ID.as(SECTION.getName() + "_" + SECTION.ID.getName()), s.SECTION_NAME, s.UNIVERSITY_SECTION_ID,
                s.TYPE, s.SEATS, s.SEATS_FILLED, s.TERM_ID,
                i.ID.as(INSTRUCTOR.getName() + "_" + INSTRUCTOR.ID.getName()), i.FIRST_NAME, i.LAST_NAME, i.UNIVERSITY_USERNAME, i.EMAIL, i.SYNCED_TERM, i.RMP_ID, i.RMP_RATING, i.RMP_LATEST_COMMENT,
                t.ID.as(TIME_SLOT.getName() + "_" + TIME_SLOT.ID.getName()), t.START_TIME, t.END_TIME, t.BUILDING, t.ROOM, t.DAYS, t.NON_WEEKLY);

        r1.set(c.ID.as(COURSE.getName() + "_" + COURSE.ID.getName()), courseId);
        r1.set(c.SUBJECT, subject);
        r1.set(c.COURSE_NUMBER, courseNumber);
        r1.set(c.UNIVERSITY_ID, universityId);
        r1.set(v.ID.as(COURSE_VERSION.getName() + "_" + COURSE_VERSION.ID.getName()), courseVersionId);
        r1.set(v.NAME, courseVersionName);
        r1.set(v.TERM_CODE, termCode);
        r1.set(s.ID.as(SECTION.getName() + "_" + SECTION.ID.getName()),sectionId);
        r1.set(s.UNIVERSITY_SECTION_ID,universitySectionId);
        r1.set(s.SECTION_NAME,sectionName);
        r1.set(s.TYPE, sectionType.toString());
        r1.set(s.SEATS,seats);
        r1.set(s.SEATS_FILLED,seatsFilled);
        r1.set(i.ID.as(INSTRUCTOR.getName() + "_" + INSTRUCTOR.ID.getName()),instructorId);
        r1.set(i.FIRST_NAME,firstName);
        r1.set(i.LAST_NAME,lastName);
        r1.set(i.UNIVERSITY_USERNAME,universityUsername);
        r1.set(i.EMAIL, email);
        r1.set(i.SYNCED_TERM, syncedTerm);
        r1.set(t.ID.as(TIME_SLOT.getName() + "_" + TIME_SLOT.ID.getName()),timeSlotId);
        r1.set(t.START_TIME, startTime);
        r1.set(t.END_TIME,endTime);
        r1.set(t.DAYS,days);
        r1.set(t.BUILDING,building);
        r1.set(t.ROOM,room);
        r1.set(t.NON_WEEKLY,nonWeekly);

        return r1;
    }
}
