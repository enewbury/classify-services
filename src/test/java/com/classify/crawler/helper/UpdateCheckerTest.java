package com.classify.crawler.helper;

import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.Instructor;
import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.TimeSlot;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UpdateCheckerTest {
    private UpdateChecker updateChecker;

    @Before
    public void setup(){
        updateChecker = new UpdateChecker();
    }

    //Course
    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionIfGivenANullArgument(){
        updateChecker.updateCourse(null, new Course());
    }

    @Test
    public void shouldReturnNullIfNoChange(){
        Course updated = updateChecker.updateCourse(new Course(), new Course());
        assertNull(updated);
        Course withData = new Course();
        withData.setId(1);
        withData.setSubject("ASCI");
        updated = updateChecker.updateCourse(withData, withData);
        assertNull(updated);
    }

    @Test
    public void shouldReturnCourseWithIdAndUpdatedField(){
        Course remoteCourse = new Course();
        remoteCourse.setSubject("ASCI");
        remoteCourse.setCreditsMin(3d);
        Course dbCourse = new Course();
        dbCourse.setId(1);
        dbCourse.setSubject("ASCI");
        dbCourse.setCreditsMin(2d);
        Course updated = updateChecker.updateCourse(remoteCourse, dbCourse);

        assertEquals(new Integer(1), updated.getId());
        assertEquals(new Double(3), updated.getCreditsMin());
        assertNull(updated.getName());
        //and more
    }

    //section
    @Test(expected = NullPointerException.class)
    public void shouldThrowNullExceptionIfASectionIsNull(){
        updateChecker.updateSection(new Section(), null);
    }

    @Test
    public void shouldReturnNullIfNoSectionChange(){
        Section updated = updateChecker.updateSection(new Section(), new Section());
        assertNull(updated);
        Section withData = new Section();
        withData.setId(1);
        withData.setSeatsFilled(4);
        updated = updateChecker.updateSection(withData, withData);
        assertNull(updated);
    }

    @Test
    public void shouldReturnSectionWithIdAndUpdatedFields(){
        Section remoteSection = new Section();
        remoteSection.setSeats(100);
        remoteSection.setSeatsFilled(23);
        Section dbSection = new Section();
        dbSection.setId(1);
        dbSection.setSeats(100);
        dbSection.setSeatsFilled(22);
        Section updated = updateChecker.updateSection(remoteSection, dbSection);

        assertEquals(new Integer(1), updated.getId());
        assertEquals(new Integer(23), updated.getSeatsFilled());
        assertNull(updated.getInstructorId());
        assertEquals(new Integer(100), updated.getSeats());
        assertNull(updated.getType());
    }

    @Test
    public void shouldReturnWithDifferentInstructor(){
        Section remoteSection = new Section();
        remoteSection.setSeats(100);
        remoteSection.setSeatsFilled(23);
        Instructor newInstructor = new Instructor("Eric", "Newbury", "enewbury", "enewbury@uvm.edu", 1);
        remoteSection.setInstructor(newInstructor);
        Section dbSection = new Section(remoteSection);
        dbSection.setInstructor(new Instructor("Dude","Man","dude","dude@uvm.edu", 1));
        Section updated = updateChecker.updateSection(remoteSection, dbSection);

        assertEquals(newInstructor, updated.getInstructor());
    }

    //time slot
    @Test(expected = NullPointerException.class)
    public void shouldThrowNullExceptionIfATimeIsNull(){
        updateChecker.updateTime(new TimeSlot(), null);
    }

    @Test
    public void shouldReturnNullIfNoTimeSlotChange(){
        TimeSlot updated = updateChecker.updateTime(new TimeSlot(), new TimeSlot());
        assertNull(updated);
        TimeSlot withData = new TimeSlot();
        withData.setId(1);
        withData.setStartTime(LocalTime.parse("10:20:00"));
        updated = updateChecker.updateTime(withData, withData);
        assertNull(updated);
    }

    @Test
    public void shouldReturnUpdatedTime(){
        TimeSlot remoteTime = new TimeSlot();
        remoteTime.setStartTime(LocalTime.parse("11:20:00"));
        remoteTime.setEndTime(LocalTime.parse("12:20:00"));
        remoteTime.setDays("1,3,5");

        TimeSlot dbTime = new TimeSlot();
        dbTime.setId(1);
        dbTime.setStartTime(LocalTime.parse("11:20:00"));
        dbTime.setEndTime(LocalTime.parse("13:20:00"));
        dbTime.setDays("2,4");
        TimeSlot updated = updateChecker.updateTime(remoteTime, dbTime);

        assertEquals(new Integer(1), updated.getId());
        assertEquals(LocalTime.parse("11:20:00"), updated.getStartTime());
        assertEquals(LocalTime.parse("12:20:00"), updated.getEndTime());
        assertEquals("1,3,5", updated.getDays());
    }

    //instructor
    @Test(expected = NullPointerException.class)
    public void shouldThrowNullExceptionIfAnInstructorIsNull(){
        updateChecker.updateInstructor(new Instructor(), null);
    }

    @Test
    public void shouldReturnNullIfNoInstructorChange(){
        Instructor updated = updateChecker.updateInstructor(new Instructor(), new Instructor());
        assertNull(updated);
        Instructor withData = new Instructor();
        withData.setId(1);
        withData.setFirstName("Eric");
        updated = updateChecker.updateInstructor(withData, withData);
        assertNull(updated);
    }

    @Test
    public void shouldReturnInstructorWithIdAndUpdatedFields(){
        Instructor remoteInst = new Instructor();
        remoteInst.setFirstName("Eric");
        remoteInst.setLastName("Newbury");
        remoteInst.setEmail("enewbz@uvm.edu");

        Instructor dbInst = new Instructor();
        dbInst.setId(1);
        //no first name currently setUVMEnrollmentUpdate
        dbInst.setLastName("Newburg");
        dbInst.setEmail("enewbug@uvm.edu");
        Instructor updated = updateChecker.updateInstructor(remoteInst, dbInst);

        assertEquals(new Integer(1), updated.getId());
        assertEquals("Eric",updated.getFirstName());
        assertEquals("Newbury", updated.getLastName());
        assertEquals("enewbz@uvm.edu", updated.getEmail());
        assertNull(updated.getRmpRating());
    }
}
