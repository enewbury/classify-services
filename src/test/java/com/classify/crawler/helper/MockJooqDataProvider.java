package com.classify.crawler.helper;

import org.jooq.tools.jdbc.MockDataProvider;
import org.jooq.tools.jdbc.MockExecuteContext;
import org.jooq.tools.jdbc.MockResult;

import java.sql.SQLException;

/**
 * Created by Eric Newbury on 8/10/17.
 */
public class MockJooqDataProvider implements MockDataProvider {
    @Override
    public MockResult[] execute(MockExecuteContext mockExecuteContext) throws SQLException {
        return new MockResult[0];
    }
}
