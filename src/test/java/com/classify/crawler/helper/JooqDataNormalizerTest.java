package com.classify.crawler.helper;


import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.Term;
import com.classify.model.business.objects.University;
import com.classify.model.business.objects.enums.SectionType;
import org.jooq.Record;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JooqDataNormalizerTest {

    private JooqDataNormalizer dataNormalizer = new JooqDataNormalizer();
    List<Course> courseList;
    List<Record> testRecords = new ArrayList<>();
    Term term = new Term();

    @Before
    public void setup(){

        CourseDataRecordBuilder recordBuilder = new CourseDataRecordBuilder();
        
        Record r1 = recordBuilder.setCourseId(1).setCourseVersionId(1).setCourseVersionName("Course 1").setSubject("ASTR").setCourseNumber("023").setTermCode("201501")
                .setSectionId(1).setUniversitySectionId(1).setSectionName("A").setSectionType(SectionType.LECTURE).setSeats(45).setSeatsFilled(14)
                .setInstructorId(1).setFirstName("Eric").setLastName("Newbz").setUniversityUsername("enewbz").setEmail("enewbz@uvm.edu").setSyncedTerm(201501)
                .setTimeSlotId(1).setStartTime(LocalTime.parse("11:30:00")).setEndTime(LocalTime.parse("12:30:00")).setDays("1,3,5").setBuilding("Terril").setRoom("L104").setNonWeekly(false)
                .build();

        //same course different section
        Record r2 = recordBuilder.setSectionId(2).setUniversitySectionId(2).setSectionName("B").setTimeSlotId(2).build();

        //same section different time
        Record r3 = recordBuilder.setTimeSlotId(3).build();

        //different section
        Record r4 = recordBuilder.setSectionId(3).setUniversitySectionId(3).setSectionName("C").setTimeSlotId(4).setDays("2,4").build();

        //different course w/ no timeslot
        Record r5 = new CourseDataRecordBuilder().setCourseId(2).setCourseVersionId(2).setCourseVersionName("Course 2").setSubject("ASTR").setCourseNumber("024").setTermCode("201501")
                .setSectionId(5).setUniversitySectionId(5).setSectionName("C").setSectionType(SectionType.LECTURE).setSeats(45).setSeatsFilled(14)
                .setInstructorId(1).setFirstName("Eric").setLastName("Newbz").setUniversityUsername("enewbz").setEmail("enewbz@uvm.edu").setSyncedTerm(201501)
                .build();

        testRecords.add(r1);
        testRecords.add(r2);
        testRecords.add(r3);
        testRecords.add(r4);
        testRecords.add(r5);

        term.setUniversityId(University.UVM);
        term.setSemester("Fall");
        term.setTermCode("201509");

        courseList = dataNormalizer.normalizeResultsToCourse(testRecords, term);
    }

    @Test
    public void shouldLoadCorrectNumberOfItems(){
        //two courses
        assertEquals(2,courseList.size());

        //first course has 3 sections
        assertEquals(3,courseList.get(0).getSections().size());

        //first course first section has one time
        assertEquals(1,courseList.get(0).getSections().get(0).getTimeSlots().size());

        //first course, second section has two times
        assertEquals(2, courseList.get(0).getSections().get(1).getTimeSlots().size());

        //second course has one section and section has no times
        assertEquals(1, courseList.get(1).getSections().size());
        assertEquals(0, courseList.get(1).getSections().get(0).getTimeSlots().size());
    }

    @Test
    public void shouldBeInCorrectOrder(){

        //courses in correct order
        assertEquals(Integer.valueOf(1), courseList.get(0).getId());
        assertEquals(Integer.valueOf(2), courseList.get(1).getId());

        //sections in correct order
        assertEquals(Integer.valueOf(1), courseList.get(0).getSections().get(0).getId());
        assertEquals(Integer.valueOf(2), courseList.get(0).getSections().get(1).getId());
        assertEquals(Integer.valueOf(3), courseList.get(0).getSections().get(2).getId());

        //times in correct order
        assertEquals(Integer.valueOf(2), courseList.get(0).getSections().get(1).getTimeSlots().get(0).getId());
        assertEquals(Integer.valueOf(3), courseList.get(0).getSections().get(1).getTimeSlots().get(1).getId());
    }
    
    @Test
    public void timeSlotsShouldHaveParentSection(){
        assertNotNull(courseList.get(0).getSections().get(1).getTimeSlots().get(0).getSection());
    }
    
    @Test
    public void sectionShouldHaveParentCourse(){
        assertNotNull(courseList.get(0).getSections().get(2).getCourse());
    }
    
    @Test
    public void shouldNotHaveAnyNullLists(){
        assertNotNull(courseList.get(1).getSections().get(0).getTimeSlots());
        assertEquals(0, courseList.get(1).getSections().get(0).getTimeSlots().size());
    }

    @Test
    public void shouldLoadCorrectRecordsWhenMappingBySection(){
        List<Section> sectionList = dataNormalizer.normalizeResultsToSection(testRecords, term);
        assertEquals(4, sectionList.size());
    }
}