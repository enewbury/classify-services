package com.classify.crawler.helper;

import com.classify.model.Semester;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TermConverterTest {

    @Test
    public void givenCodeReturnYear(){
        TermConverter termConverter = new TermConverter();
        assertEquals(2015, termConverter.getYear("201501"));
    }

    @Test
    public void givenCodeReturnSemester(){
        TermConverter termConverter = new TermConverter();
        assertEquals(Semester.FALL, termConverter.getSemester("201509"));
        assertEquals(Semester.SPRING, termConverter.getSemester("201501"));
        assertEquals(Semester.SUMMER, termConverter.getSemester("201506"));
    }

    @Test
    public void givenSemesterReturnSemesterCode(){
        TermConverter termConverter = new TermConverter();
        assertEquals("01", termConverter.getSemesterCode(Semester.SPRING));
        assertEquals("06", termConverter.getSemesterCode(Semester.SUMMER));
        assertEquals("09", termConverter.getSemesterCode(Semester.FALL));
    }

    @Test
    public void givenSemesterAndYearReturnCode(){
        TermConverter termConverter = new TermConverter();
        assertEquals("201001", termConverter.getTermCode(Semester.SPRING, "2010"));
        assertEquals("201006", termConverter.getTermCode(Semester.SUMMER, "2010"));
        assertEquals("201009", termConverter.getTermCode(Semester.FALL, "2010"));
    }
}
