package com.classify.crawler.helper;

import com.classify.model.business.objects.Course;

import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.TimeSlot;
import com.classify.model.business.objects.enums.SectionType;
import org.junit.Test;

import java.time.LocalTime;

import static com.classify.model.uvm.CheckStatus.*;
import static org.junit.Assert.assertEquals;

public class WalkActionConsultantTest {

    private WalkActionConsultant consultant = new WalkActionConsultant();


    //Course
    @Test
    public void shouldAddWhenDbCourseIsNull(){
        Course remoteCourse = new Course("","ASCI","021",3d,null,1);
        assertEquals(ADD,consultant.determineCourseAction(remoteCourse, null));
    }

    @Test
    public void shouldDeleteWhenRemoteCourseIsNull(){
        Course dbCourse = new Course("","ASCI","021",3d,null,1); dbCourse.setId(1);
        assertEquals(DELETE, consultant.determineCourseAction(null, dbCourse));
    }

    @Test
    public void shouldAddWhenRemoteSubjectComesBefore(){
        Course remoteCourse = new Course("","ASCI", "021", 3d, null, 1);
        Course dbCourse = new Course("","CALS","024", 3d, null, 1);
        assertEquals(ADD, consultant.determineCourseAction(remoteCourse,dbCourse));
    }

    @Test
    public void shouldAddWhenSubjectsSameAndRemoteNumberComesBefore(){
        Course remoteCourse = new Course("","ASCI", "021", 3d, null, 1);
        Course dbCourse = new Course("","ASCI","024", 3d, null, 1);
        assertEquals(ADD, consultant.determineCourseAction(remoteCourse,dbCourse));
    }

    @Test
    public void shouldDeleteWhenDbSubjectComesBefore(){
        Course remoteCourse = new Course("","CALS", "021", 3d, null, 1);
        Course dbCourse = new Course("","ASCI","024", 3d, null, 1);
        assertEquals(DELETE, consultant.determineCourseAction(remoteCourse,dbCourse));
    }

    @Test
    public void shouldDeleteWhenSubjectsSameAndDbNumberComesBefore(){
        Course remoteCourse = new Course("","CALS", "060", 3d, null, 1);
        Course dbCourse = new Course("","CALS","024", 3d, null, 1);
        assertEquals(DELETE, consultant.determineCourseAction(remoteCourse,dbCourse));
    }

    @Test
    public void shouldUpdateWhenSameSubjectAndCourseNumber(){
        Course remoteCourse = new Course("","ASCI", "021", 3d, null, 1);
        Course dbCourse = new Course("","ASCI","021", 3d, null, 1);
        assertEquals(UPDATE, consultant.determineCourseAction(remoteCourse, dbCourse));
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionIfGivenTwoNullPointers(){
        consultant.determineCourseAction(null, null);
    }

    //Section
    @Test
    public void shouldAddWhenDbSectionIsNull(){
        Section remoteSection = new Section(24322,"B", SectionType.LECTURE,24,10);
        assertEquals(ADD, consultant.determineSectionAction(remoteSection, null));
    }

    @Test
    public void shouldDeleteWhenRemoteSectionIsNull(){
        Section dbSection = new Section(24322,"B", SectionType.LECTURE,24,10);
        assertEquals(DELETE, consultant.determineSectionAction(null, dbSection));
    }

    @Test
    public void shouldUpdateWhenUniversityIdsEqual(){
        Section remoteSection = new Section(24322,"A", SectionType.LECTURE,24,10);
        Section dbSection = new Section(24322,"A", SectionType.LECTURE,24,10);
        assertEquals(UPDATE, consultant.determineSectionAction(remoteSection, dbSection));
    }

    @Test
    public void shouldAddWhenNotEqualAndRemoteSectionComesBefore(){
        Section remoteSection = new Section(24322,"A", SectionType.LECTURE,24,10);
        Section dbSection = new Section(3553,"B", SectionType.LECTURE,24,10);
        assertEquals(ADD, consultant.determineSectionAction(remoteSection, dbSection));
    }

    @Test
    public void shouldDeleteWhenNotEqualAndDbSectionComesBefore(){
        Section remoteSection = new Section(45543,"B", SectionType.LECTURE,24,10);
        Section dbSection = new Section(24322,"A", SectionType.LECTURE,24,10);
        assertEquals(DELETE, consultant.determineSectionAction(remoteSection, dbSection));
    }
    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionIfGivenTwoNullSections(){
        consultant.determineSectionAction(null, null);
    }

    //TIMESLOT

    @Test
    public void shouldAddWhenDbTimeIsNull(){
        TimeSlot remoteTime = new TimeSlot();
        assertEquals(ADD, consultant.determineTimeAction(remoteTime, null));
    }

    @Test
    public void shouldDeleteWhenRemoteTimeIsNull(){
        TimeSlot dbTime = new TimeSlot();
        assertEquals(DELETE, consultant.determineTimeAction(null, dbTime));
    }

    @Test
    public void shouldUpdateWhenAllThreeAreEqual(){
        TimeSlot remoteTime = new TimeSlot(LocalTime.parse("10:00:00"), LocalTime.parse("11:00:00"),"1","","", false);
        TimeSlot dbTime = new TimeSlot(LocalTime.parse("10:00:00"), LocalTime.parse("11:00:00"),"1","","", false);
        assertEquals(UPDATE, consultant.determineTimeAction(remoteTime, dbTime));
    }

    @Test
    public void shouldAddWhenSameTimeAndRemoteDaysComesFirst(){
        TimeSlot remoteTime = new TimeSlot(LocalTime.parse("10:00:00"), LocalTime.parse("11:00:00"),"1,3,5","","", false);
        TimeSlot dbTime = new TimeSlot(LocalTime.parse("10:00:00"), LocalTime.parse("11:00:00"),"2,4","","", false);
        assertEquals(ADD, consultant.determineTimeAction(remoteTime, dbTime));
    }
    @Test
    public void shouldDeleteWhenSameTimeAndDbDaysComesFirst(){
        TimeSlot remoteTime = new TimeSlot(LocalTime.parse("10:00:00"), LocalTime.parse("11:00:00"),"2,4","","", false);
        TimeSlot dbTime = new TimeSlot(LocalTime.parse("10:00:00"), LocalTime.parse("11:00:00"),"1,3,5","","", false);
        assertEquals(DELETE, consultant.determineTimeAction(remoteTime, dbTime));
    }

    @Test
    public void shouldAddWhenRemoteTimeComesFirst(){
        TimeSlot remoteTime = new TimeSlot(LocalTime.parse("10:00:00"), LocalTime.parse("11:00:00"),"1,3,5","","", false);
        TimeSlot dbTime = new TimeSlot(LocalTime.parse("10:30:00"), LocalTime.parse("11:00:00"),"1,3,5","","", false);
        assertEquals(ADD, consultant.determineTimeAction(remoteTime, dbTime));
    }

    @Test
    public void shouldDeleteWhenDbTimeComesFirst(){
        TimeSlot remoteTime = new TimeSlot(LocalTime.parse("10:45:00"), LocalTime.parse("11:00:00"),"1,3,5","","", false);
        TimeSlot dbTime = new TimeSlot(LocalTime.parse("10:30:00"), LocalTime.parse("11:00:00"),"1,3,5","","", false);
        assertEquals(DELETE, consultant.determineTimeAction(remoteTime, dbTime));
    }

    @Test
    public void shouldDeleteIfEverythingMatchesButEndTime(){
        TimeSlot remoteTime = new TimeSlot(LocalTime.parse("10:30:00"), LocalTime.parse("11:45:00"),"1,3,5","","", false);
        TimeSlot dbTime = new TimeSlot(LocalTime.parse("10:30:00"), LocalTime.parse("11:00:00"),"1,3,5","","", false);
        assertEquals(DELETE, consultant.determineTimeAction(remoteTime, dbTime));
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionIfGivenTwoNullTimes(){
        consultant.determineTimeAction(null, null);
    }

}
