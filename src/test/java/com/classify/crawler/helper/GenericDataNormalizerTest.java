package com.classify.crawler.helper;


import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.Term;
import com.classify.model.business.objects.University;
import com.classify.model.business.objects.enums.SectionType;
import com.classify.model.uvm.DenormalizedCourseData;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GenericDataNormalizerTest {

    private GenericDataNormalizer dataNormalizer = new GenericDataNormalizer();
    List<Course> courseList;

    @Before
    public void setup(){
        List<DenormalizedCourseData> testRecords = new ArrayList<>();
        
        DenormalizedCourseData r1 = new DenormalizedCourseData();
        r1.courseName="Course 1"; r1.subject="ASTR"; r1.courseNumber="023"; r1.creditsMin=3d; r1.creditsMax=null;
        r1.universitySectionId=1; r1.sectionName="A"; r1.sectionType= SectionType.LECTURE; r1.seats=45; r1.seatsFilled=14;
        r1.firstName="Eric"; r1.lastName="Newbz"; r1.universityUsername="enewbz"; r1.email="enewbz@uvm.edu";
        r1.startTime= LocalTime.parse("11:30:00"); r1.endTime=LocalTime.parse("12:30:00"); r1.days="1,3,5"; r1.building="TERRIL"; r1.room="L104";

        //same course different section
        DenormalizedCourseData r2 = new DenormalizedCourseData();
        r2.courseName="Course 1"; r2.subject="ASTR"; r2.courseNumber="023";
        r2.universitySectionId=2; r2.sectionName="B"; r2.sectionType= SectionType.LECTURE; r2.seats=45; r2.seatsFilled=14;
        r2.firstName="Eric"; r2.lastName="Newbz"; r2.universityUsername="enewbz"; r2.email="enewbz@uvm.edu";
        r2.startTime=LocalTime.parse("11:30:00"); r2.endTime=LocalTime.parse("12:30:00");  r2.days="1,3,5"; r2.building="TERRIL"; r2.room="L104";

        //same section different time
        DenormalizedCourseData r3 = new DenormalizedCourseData();
        r3.courseName="Course 1"; r3.subject="ASTR"; r3.courseNumber="023";
        r3.universitySectionId=2; r3.sectionName="B"; r3.sectionType= SectionType.LECTURE; r3.seats=45; r3.seatsFilled=14;
        r3.firstName="Eric"; r3.lastName="Newbz"; r3.universityUsername="enewbz"; r3.email="enewbz@uvm.edu";
        r3.startTime=LocalTime.parse("12:30:00"); r3.endTime=LocalTime.parse("13:30:00");  r3.days="4"; r3.building="TERRIL"; r3.room="L104";

        //different section
        DenormalizedCourseData r4 = new DenormalizedCourseData();
        r4.courseName="Course 1"; r4.subject="ASTR"; r4.courseNumber="023";
        r4.universitySectionId=3; r4.sectionName="C"; r4.sectionType= SectionType.LECTURE; r4.seats=45; r4.seatsFilled=14;
        r4.firstName="Eric"; r4.lastName="Newbz"; r4.universityUsername="enewbz"; r4.email="enewbz@uvm.edu";
        r4.startTime=LocalTime.parse("10:30:00"); r4.endTime=LocalTime.parse("12:30:00");  r4.days="2,4"; r4.building="TERRIL"; r4.room="L104";
        
        //different course
        DenormalizedCourseData r5 = new DenormalizedCourseData();
        r5.courseName="Course 1"; r5.subject="ASTR"; r5.courseNumber="140"; r5.creditsMin=1d; r5.creditsMax=3d;
        r5.universitySectionId=4; r5.sectionName="C"; r5.sectionType= SectionType.LECTURE; r5.seats=45; r5.seatsFilled=14;
        r5.firstName="Eric"; r5.lastName="Newbz"; r5.universityUsername="enewbz"; r5.email="enewbz@uvm.edu";
        r5.startTime=LocalTime.parse("10:30:00"); r5.endTime=LocalTime.parse("12:30:00");  r5.days="2,4"; r5.building="TERRIL"; r5.room="L104";
        
        //overlapping time
        DenormalizedCourseData r6 = new DenormalizedCourseData();
        r6.courseName="Course 1"; r6.subject="ASTR"; r6.courseNumber="140";
        r6.universitySectionId=4; r6.sectionName="C"; r6.sectionType= SectionType.LECTURE; r6.seats=45; r6.seatsFilled=14;
        r6.firstName="Eric"; r6.lastName="Newbz"; r6.universityUsername="enewbz"; r6.email="enewbz@uvm.edu";
        r6.startTime=LocalTime.parse("10:40:00"); r6.endTime=LocalTime.parse("12:10:00");  r6.days="4"; r6.building="TERRIL"; r6.room="L104";

        //invalid time
        DenormalizedCourseData r7 = new DenormalizedCourseData();
        r7.courseName="Course 1"; r7.subject="ASTR"; r7.courseNumber="140";
        r7.universitySectionId=4; r7.sectionName="C"; r7.sectionType= SectionType.LECTURE; r7.seats=45; r7.seatsFilled=14;
        r7.firstName="Eric"; r7.lastName="Newbz"; r7.universityUsername="enewbz"; r7.email="enewbz@uvm.edu";
        r7.startTime=LocalTime.parse("14:30:00"); r7.endTime=LocalTime.parse("15:30:00");  r7.days=null; r7.building="TERRIL"; r7.room="L104";

        //invalid time
        DenormalizedCourseData r8 = new DenormalizedCourseData();
        r8.courseName="Course 1"; r8.subject="ASTR"; r8.courseNumber="140";
        r8.universitySectionId=4; r8.sectionName="C"; r8.sectionType= SectionType.LECTURE; r8.seats=45; r8.seatsFilled=14;
        r8.firstName="Eric"; r8.lastName="Newbz"; r8.universityUsername="enewbz"; r8.email="enewbz@uvm.edu";
        r8.startTime=null; r8.endTime=LocalTime.parse("12:30:00");  r8.days="2,4"; r8.building="TERRIL"; r8.room="L104";

        //valid section with no time
        DenormalizedCourseData r9 = new DenormalizedCourseData();
        r9.courseName="Course 1"; r9.subject="ASTR"; r9.courseNumber="140";
        r9.universitySectionId=5; r9.sectionName="D"; r9.sectionType= SectionType.LECTURE; r9.seats=45; r9.seatsFilled=14;
        r9.firstName="Eric"; r9.lastName="Newbz"; r9.universityUsername="enewbz"; r9.email="enewbz@uvm.edu";
        r9.startTime=null; r9.endTime=null;  r9.days=null; r9.building=null; r9.room=null;


        testRecords.add(r1);
        testRecords.add(r2);
        testRecords.add(r3);
        testRecords.add(r4);
        testRecords.add(r5);
        testRecords.add(r6);
        testRecords.add(r7);
        testRecords.add(r8);
        testRecords.add(r9);

        Term term = new Term();
        term.setUniversityId(University.UVM);
        term.setSemester("Fall");
        term.setTermCode("201509");

        courseList = dataNormalizer.normalizeResultsToCourse(testRecords, term);
    }

    @Test
    public void shouldLoadCorrectNumberOfItems(){
        //two courses
        assertEquals(2,courseList.size());

        //first course has 3 sections
        assertEquals(3,courseList.get(0).getSections().size());

        //first course first section has one time
        assertEquals(1,courseList.get(0).getSections().get(0).getTimeSlots().size());

        //first course, second section has two times
        assertEquals(2, courseList.get(0).getSections().get(1).getTimeSlots().size());

        //second course has two sections and section 1 has one time
        assertEquals(2, courseList.get(1).getSections().size());
        assertEquals(1, courseList.get(1).getSections().get(0).getTimeSlots().size());
        //second course 2nd section has no times
        assertEquals(0, courseList.get(1).getSections().get(1).getTimeSlots().size());
    }

    @Test
    public void shouldBeInCorrectOrder(){

        //courses in correct order
        assertEquals("023", courseList.get(0).getCourseNumber());
        assertEquals("140", courseList.get(1).getCourseNumber());

        //sections in correct order
        assertEquals("A", courseList.get(0).getSections().get(0).getSectionName());
        assertEquals("B", courseList.get(0).getSections().get(1).getSectionName());
        assertEquals("C", courseList.get(0).getSections().get(2).getSectionName());
        assertEquals("C", courseList.get(1).getSections().get(0).getSectionName());
        assertEquals("D", courseList.get(1).getSections().get(1).getSectionName());

        //times in correct order
        assertEquals(LocalTime.parse("11:30:00"), courseList.get(0).getSections().get(1).getTimeSlots().get(0).getStartTime());
        assertEquals(LocalTime.parse("12:30:00"), courseList.get(0).getSections().get(1).getTimeSlots().get(1).getStartTime());
    }

    @Test
    public void shouldBeSetToNonWeekly(){
        assertTrue(courseList.get(1).getSections().get(0).getTimeSlots().get(0).getNonWeekly());
    }

    @Test
    public void shouldNotIncludeInvalidItems(){
        boolean nullPointer = false;
        try{
            courseList.get(1).getSections().get(0).getTimeSlots().get(1);
        }
        catch(IndexOutOfBoundsException e){
            nullPointer = true;
        }

        assertTrue(nullPointer);

        nullPointer = false;

        try{
            courseList.get(1).getSections().get(0).getTimeSlots().get(2);
        }
        catch (IndexOutOfBoundsException e){
            nullPointer = true;
        }

        assertTrue(nullPointer);
    }
}