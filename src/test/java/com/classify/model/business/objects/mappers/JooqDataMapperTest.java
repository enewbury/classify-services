package com.classify.model.business.objects.mappers;


import com.classify.crawler.helper.CourseDataRecordBuilder;
import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.TimeSlot;
import com.classify.model.business.objects.enums.SectionType;
import org.jooq.Record;
import org.junit.Test;

import static org.junit.Assert.*;

public class JooqDataMapperTest {

    private JooqDataMapper dataMapper = new JooqDataMapper();


    @Test
    public void shouldHandleNulls() throws NullPointerException{
        Record r1 = new CourseDataRecordBuilder()
        .setCourseId(1).setCourseVersionName("Course 1").setSubject("ASTR").setCourseNumber("023").setCourseVersionId(1)
        .setSectionId(1).setUniversitySectionId(1).setSectionName("A").setSectionType(SectionType.LECTURE).setSeats(45).setSeatsFilled(14)
        .setInstructorId(1).setFirstName("Eric").setLastName("Newbz").setUniversityUsername("enewbz").setEmail("enewbz@uvm.setEdu")
        .setTimeSlotId(1).setStartTime(null).setEndTime(null).setDays(null).setBuilding(null).setRoom(null).setNonWeekly(false).build();

        Course course = dataMapper.mapCourse(r1);
        Section section = dataMapper.mapSection(r1);
        TimeSlot time = dataMapper.mapTimeSlot(r1);

        assertNull(course.getCreditsMax());
        assertNull(time.getStartTime());
        assertNull(time.getEndTime());
        assertNull(time.getDays());
        assertNotNull(time.getDaysList());
        assertEquals(0, time.getDaysList().size());
    }

    @Test
    public void shouldReturnNullTimeIfNoTimeId(){
        Record r1 = new CourseDataRecordBuilder()
       .setCourseId(1).setCourseVersionName("Course 1").setSubject("ASTR").setCourseNumber("023")
        .setSectionId(1).setUniversitySectionId(1).setSectionName("A").setSectionType(SectionType.LECTURE).setSeats(45).setSeatsFilled(14)
        .setInstructorId(1).setFirstName("Eric").setLastName("Newbz").setUniversityUsername("enewbz").setEmail("enewbz@uvm.setEdu")
                .build();

        TimeSlot time = dataMapper.mapTimeSlot(r1);
        assertNull(time);
    }

    @Test
    public void shouldHaveNullInstructorIfIdNull(){
        Record r1 = new CourseDataRecordBuilder()
        .setCourseId(1).setCourseVersionName("Course 1").setSubject("ASTR").setCourseNumber("023")
        .setSectionId(1).setUniversitySectionId(1).setSectionName("A").setSectionType(SectionType.LECTURE).setSeats(45).setSeatsFilled(14).build();

        Section section = dataMapper.mapSection(r1);

        assertNull(section.getInstructorId());
        assertNull(section.getInstructor());
    }
}