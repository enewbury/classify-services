package com.classify.model.business.objects.mappers;


import com.classify.model.business.objects.Course;
import com.classify.model.business.objects.Section;
import com.classify.model.business.objects.TimeSlot;
import com.classify.model.business.objects.enums.SectionType;
import com.classify.model.uvm.DenormalizedCourseData;
import org.junit.Test;

import java.time.LocalTime;

import static org.junit.Assert.*;

public class GenericDataMapperTest {

    private GenericDataMapper dataMapper = new GenericDataMapper();


    @Test
    public void shouldHandleNulls() throws NullPointerException{
        DenormalizedCourseData r1 = new DenormalizedCourseData();
        r1.courseName="Course 1"; r1.subject="ASTR"; r1.courseNumber="023"; r1.creditsMin=3d; r1.creditsMax=null;
        r1.universitySectionId=1; r1.sectionName="A"; r1.sectionType= SectionType.LECTURE; r1.seats=45; r1.seatsFilled=14;
        r1.firstName="Eric"; r1.lastName="Newbz"; r1.universityUsername="enewbz"; r1.email="enewbz@uvm.edu";
        r1.startTime= LocalTime.parse("10:30:00"); r1.endTime=LocalTime.parse("11:30:00"); r1.days=""; r1.building=null; r1.room=null;

        Course course = dataMapper.mapCourse(r1);
        Section section = dataMapper.mapSection(r1);
        TimeSlot time = dataMapper.mapTimeSlot(r1);

        assertNull(course.getCreditsMax());
        assertNull(time.getDays());
        assertNotNull(time.getDaysList());
        assertEquals(0, time.getDaysList().size());
    }

    @Test
    public void shouldHaveNoTimeslotForNullDays(){
        DenormalizedCourseData r1 = new DenormalizedCourseData();
        r1.courseName="Course 1"; r1.subject="ASTR"; r1.courseNumber="023"; r1.creditsMin=3d; r1.creditsMax=null;
        r1.universitySectionId=1; r1.sectionName="A"; r1.sectionType= SectionType.LECTURE; r1.seats=45; r1.seatsFilled=14;
        r1.firstName="Eric"; r1.lastName="Newbz"; r1.universityUsername="enewbz"; r1.email="enewbz@uvm.edu";
        r1.startTime=null; r1.endTime=null; r1.days=null; r1.building=null; r1.room=null;

        Section section = dataMapper.mapSection(r1);
        TimeSlot time = dataMapper.mapTimeSlot(r1);

        assertNull(time);
        assertEquals(0,section.getTimeSlots().size());

    }
}